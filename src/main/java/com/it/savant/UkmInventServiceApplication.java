package com.it.savant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UkmInventServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(UkmInventServiceApplication.class, args);
    }
}