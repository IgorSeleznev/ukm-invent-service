package com.it.savant;

import com.it.savant.ukm.invent.service.domain.service.database.DataInitializeService;
import com.it.savant.ukm.invent.service.domain.service.database.DatabaseCreateManager;
import com.it.savant.ukm.invent.service.domain.service.database.DatabaseSelectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class DatabaseInitializeApplicationRunner implements ApplicationRunner {

    private final DatabaseCreateManager createManager;
    private final DatabaseSelectService selectService;
    private final DataInitializeService initializeService;

    @Autowired
    public DatabaseInitializeApplicationRunner(final DatabaseCreateManager createManager,
                                               final DatabaseSelectService selectService,
                                               final DataInitializeService initializeService) {
        this.createManager = createManager;
        this.selectService = selectService;
        this.initializeService = initializeService;
    }

    @Override
    public void run(final ApplicationArguments args) {
        createManager.createIfNotExists();
        selectService.select();
        initializeService.initialize();
    }
}
