package com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.map.validator;

import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class IntegrationSmSpecMapValidator {

    public boolean isValid(final Map<String, String> map) {
        return map.containsKey("DOCID")
                && map.containsKey("DOCTYPE")
                && map.containsKey("SPECITEM")
                && map.containsKey("ARTICLE")
                && map.containsKey("DISPLAYITEM")
                && map.containsKey("ITEMPRICE")
                && map.containsKey("ITEMPRICECUR")
                && map.containsKey("QUANTITY")
                && map.containsKey("TOTALPRICE")
                && map.containsKey("TOTALPRICECUR")
                && map.size() == 10;
    }
}
