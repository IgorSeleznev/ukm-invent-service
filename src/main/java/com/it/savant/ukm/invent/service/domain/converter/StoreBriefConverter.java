package com.it.savant.ukm.invent.service.domain.converter;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.entity.StoreBriefEntity;
import com.it.savant.ukm.invent.service.domain.model.StoreBrief;
import org.springframework.stereotype.Component;

@Component
public class StoreBriefConverter implements Converter<StoreBriefEntity, StoreBrief> {

    @Override
    public StoreBrief convert(StoreBriefEntity source) {
        return new StoreBrief()
                .setId(source.getId())
                .setName(source.getName());
    }
}
