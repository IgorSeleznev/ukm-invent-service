package com.it.savant.ukm.invent.service.domain.service.parameter;

import com.it.savant.ukm.invent.service.domain.model.parameter.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ParameterUpdateService {

    private final ParameterProvider parameterProvider;

    @Autowired
    public ParameterUpdateService(final ParameterProvider parameterProvider) {
        this.parameterProvider = parameterProvider;
    }

    public void update(final Parameter parameter) {
        parameterProvider.write(parameter.getName(), parameter.getValue());
    }
}
