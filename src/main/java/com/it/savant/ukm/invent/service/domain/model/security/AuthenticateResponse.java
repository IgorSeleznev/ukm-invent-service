package com.it.savant.ukm.invent.service.domain.model.security;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class AuthenticateResponse {

    public static AuthenticateResponse failed() {
        return new AuthenticateResponse();
    }

    private String token;

    public boolean isFailed() {
        return token == null;
    }
}
