package com.it.savant.ukm.invent.service.domain.service.inventory;

import com.it.savant.ukm.invent.service.da.client.inventory.InventoryManuallyExistByOrdernoNotConfirmedClient;
import com.it.savant.ukm.invent.service.domain.model.Inventory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InventoryManuallyMakeService {

    private final InventoryManuallyExistByOrdernoNotConfirmedClient existClient;
    private final InventoryManuallyActualizeService manuallyActualizeService;
    private final InventoryManuallyCloneService cloneService;

    @Autowired
    public InventoryManuallyMakeService(
            final InventoryManuallyExistByOrdernoNotConfirmedClient existClient,
            final InventoryManuallyActualizeService manuallyActualizeService,
            final InventoryManuallyCloneService cloneService
    ) {
        this.existClient = existClient;
        this.manuallyActualizeService = manuallyActualizeService;
        this.cloneService = cloneService;
    }

    public void make(final Inventory importedInventory) {
        if (!existClient.existNotConfirmed(importedInventory.getOrderNo(), importedInventory.getLocation())) {
            cloneService.clone(importedInventory.getOrderNo(), importedInventory.getLocation());
        } else {
            manuallyActualizeService.actualize(importedInventory);
        }
    }
}
