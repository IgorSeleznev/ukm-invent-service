package com.it.savant.ukm.invent.service.common.exception;

public class UkmInventServiceBusinessException extends RuntimeException {

    public UkmInventServiceBusinessException(final String message) {
        super(message);
    }

    public UkmInventServiceBusinessException(final Throwable throwable) {
        super(throwable);
    }
}
