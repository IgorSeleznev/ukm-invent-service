package com.it.savant.ukm.invent.service.web.converter.security;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.security.AdminAuthenticateRequest;
import com.it.savant.ukm.invent.service.web.json.AdminAuthenticateRequestJson;
import org.springframework.stereotype.Component;

@Component
public class AdminAuthenticateRequestConverter implements Converter<AdminAuthenticateRequestJson, AdminAuthenticateRequest> {

    @Override
    public AdminAuthenticateRequest convert(final AdminAuthenticateRequestJson source) {
        return new AdminAuthenticateRequest()
                .setLogin(source.getLogin())
                .setPassword(source.getPassword());
    }
}
