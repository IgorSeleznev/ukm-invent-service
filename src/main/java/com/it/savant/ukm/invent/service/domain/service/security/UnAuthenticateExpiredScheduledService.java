package com.it.savant.ukm.invent.service.domain.service.security;

import com.it.savant.ukm.invent.service.domain.service.scheduled.ScheduledService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TriggerContext;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class UnAuthenticateExpiredScheduledService implements ScheduledService {

    private final UnAuthenticateExpiredService unAuthenticateService;
    private final UnAuthenticateNextExecutionTimeService nextExecutionTimeService;

    @Autowired
    public UnAuthenticateExpiredScheduledService(
            final UnAuthenticateExpiredService unAuthenticateService,
            final UnAuthenticateNextExecutionTimeService nextExecutionTimeService
    ) {
        this.unAuthenticateService = unAuthenticateService;
        this.nextExecutionTimeService = nextExecutionTimeService;
    }

    @Override
    public void execute() {
        unAuthenticateService.unAuthenticateExpired();
    }

    @Override
    public Date nextExecutionTime(final TriggerContext triggerContext) {
        return nextExecutionTimeService.nextExecutionTime(triggerContext);
    }
}
