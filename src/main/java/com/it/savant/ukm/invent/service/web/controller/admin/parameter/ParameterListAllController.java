package com.it.savant.ukm.invent.service.web.controller.admin.parameter;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.PageableSortableRequest;
import com.it.savant.ukm.invent.service.domain.model.parameter.Parameter;
import com.it.savant.ukm.invent.service.domain.service.parameter.ParameterListAllPageableSortableService;
import com.it.savant.ukm.invent.service.web.json.PageableSortableRequestJson;
import com.it.savant.ukm.invent.service.web.json.ParameterJson;
import com.it.savant.ukm.invent.service.web.json.ResultPageJson;
import com.it.savant.ukm.invent.service.web.security.Authorized;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static com.it.savant.ukm.invent.service.web.json.ResultPageJson.resultFrom;

@Slf4j
@RestController
public class ParameterListAllController {

    private final ParameterListAllPageableSortableService service;
    private final Converter<Parameter, ParameterJson> parameterJsonConverter;
    private final Converter<PageableSortableRequestJson, PageableSortableRequest> requestConverter;

    @Autowired
    public ParameterListAllController(final ParameterListAllPageableSortableService service,
                                      final Converter<Parameter, ParameterJson> parameterJsonConverter,
                                      final Converter<PageableSortableRequestJson, PageableSortableRequest> requestConverter) {
        this.service = service;
        this.parameterJsonConverter = parameterJsonConverter;
        this.requestConverter = requestConverter;
    }

    @Authorized(administratorOnly = true)
    @PostMapping("/admin/parameter/list-all")
    public ResultPageJson<ParameterJson> listAll(@RequestBody final PageableSortableRequestJson request) {
        log.info("Получен запрос на получение списка параметров");
        return resultFrom(
                service.listAll(
                        requestConverter.convert(request)
                ),
                parameterJsonConverter
        );
    }
}
