package com.it.savant.ukm.invent.service.domain.service.license;

import com.it.savant.ukm.invent.service.da.client.license.LicenseActivityDateTimeClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class LicenseActivityUpdateService {

    private final LicenseActivityDateTimeClient client;

    @Autowired
    public LicenseActivityUpdateService(final LicenseActivityDateTimeClient client) {
        this.client = client;
    }

    public void update(final String clientId, final LocalDateTime activityDateTime) {
        client.update(clientId, activityDateTime);
    }
}
