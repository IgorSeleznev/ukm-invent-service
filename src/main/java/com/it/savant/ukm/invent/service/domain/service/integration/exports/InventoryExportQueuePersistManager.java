package com.it.savant.ukm.invent.service.domain.service.integration.exports;

import com.it.savant.ukm.invent.service.da.client.export.queue.ExportQueueInsertClient;
import com.it.savant.ukm.invent.service.da.client.export.queue.ExportQueueListClient;
import com.it.savant.ukm.invent.service.da.client.export.queue.ExportQueueMarkExportedClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class InventoryExportQueuePersistManager {

    private final ExportQueueInsertClient insertClient;
    private final ExportQueueMarkExportedClient markExportedClient;
    private final ExportQueueListClient listClient;

    @Autowired
    public InventoryExportQueuePersistManager(final ExportQueueInsertClient insertClient,
                                              final ExportQueueMarkExportedClient markExportedClient,
                                              final ExportQueueListClient listClient) {
        this.insertClient = insertClient;
        this.markExportedClient = markExportedClient;
        this.listClient = listClient;
    }

    public void replace(final String docId) {
        insertClient.replace(docId);
    }

    public void markPolled(final String docId) {
        markExportedClient.markPolled(docId);
    }

    public List<String> list() {
        return listClient.listAll();
    }
}
