package com.it.savant.ukm.invent.service.domain.service.parameter;

import com.it.savant.ukm.invent.service.da.client.initialize.TableIsEmptyClient;
import com.it.savant.ukm.invent.service.domain.model.parameter.Parameter;
import com.it.savant.ukm.invent.service.domain.model.parameter.ParameterChooseItem;
import com.it.savant.ukm.invent.service.domain.service.scheduled.SchedulingModeType;
import com.it.savant.ukm.invent.service.domain.service.integration.IntegrationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.google.common.collect.Lists.newArrayList;
import static com.it.savant.ukm.invent.service.domain.constants.ParameterConstants.*;
import static com.it.savant.ukm.invent.service.domain.model.parameter.ParameterType.*;

@Service
public class ParameterTableInitializeService {

    private final TableIsEmptyClient emptyClient;
    private final ParameterProvider parameterProvider;

    @Autowired
    public ParameterTableInitializeService(final TableIsEmptyClient emptyClient,
                                           final ParameterProvider parameterProvider) {
        this.emptyClient = emptyClient;
        this.parameterProvider = parameterProvider;
    }

    @Transactional
    public void initialize() {
        if (!emptyClient.isEmpty("parameter")) {
            return;
        }
        parameterProvider.insert(
                new Parameter()
                        .setName(DOCID_SEQUENCE)
                        .setValue("0")
                        .setTitle("Номер следующего акта инвентаризации для экспорта в Супермаг+")
                        .setType(NUMBER)
        );
        parameterProvider.insert(
                new Parameter()
                        .setName(DOCID_PREFIX_PLACEHOLDER)
                        .setValue("M%s")
                        .setTitle("Шаблон для формирования кода акта инвентаризации")
        );
        parameterProvider.insert(
                new Parameter()
                        .setName(IMPORT_FAILED_TRY_TIMEOUT)
                        .setValue(IMPORT_FAILED_TRY_TIMEOUT_DEFAULT_VALUE)
                        .setTitle("Таймаут повтора импорта ИЗ Супермаг+ в случае ошибки")
                        .setType(NUMBER)
        );
        parameterProvider.insert(
                new Parameter()
                        .setName(EXPORT_FAILED_TRY_TIMEOUT)
                        .setValue(EXPORT_FAILED_TRY_TIMEOUT_DEFAULT_VALUE)
                        .setTitle("Таймаут повтора экспорта В Супермаг+ в случае ошибки")
                        .setType(NUMBER)
        );
        parameterProvider.insert(
                new Parameter()
                        .setName(INTEGRATION_SOURCE_LOCAL_PATH)
                        .setValue("./import")
                        .setTitle("Каталог файлов для загрузки ИЗ Супермаг+")
        );
        parameterProvider.insert(
                new Parameter()
                        .setName(INTEGRATION_SOURCE_LOCAL_TEMPORARY_PATH)
                        .setValue("./temp")
                        .setTitle("Каталог временных файлов ИЗ Супермаг+ ")
        );
        parameterProvider.insert(
                new Parameter()
                        .setName(INTEGRATION_TYPE)
                        .setValue(INTEGRATION_TYPE_DEFAULT_VALUE)
                        .setTitle("Тип интеграции с Супермаг+ (через каталог/через FTP)")
                        .setType(POPUP)
                        .setChooses(
                                newArrayList(
                                        new ParameterChooseItem()
                                                .setValue(IntegrationType.LOCAL.toString())
                                                .setTitle("КАТАЛОГ"),
                                        new ParameterChooseItem()
                                                .setValue(IntegrationType.FTP.toString())
                                                .setTitle("FTP")
                                )
                        )
        );
        parameterProvider.insert(
                new Parameter()
                        .setName(IMPORT_SCHEDULE_CRON)
                        .setValue("")
                        .setTitle("CRON-расписание для импорта ИЗ Супермаг+")
        );
        parameterProvider.insert(
                new Parameter()
                        .setName(IMPORT_SCHEDULE_INTERVAL)
                        .setValue(IMPORT_SCHEDULE_INTERVAL_DEFAULT_VALUE)
                        .setTitle("Интервал повтора импорта ИЗ Супермаг+ в миллисекудах")
                        .setType(NUMBER)
        );
        parameterProvider.insert(
                new Parameter()
                        .setName(IMPORT_SCHEDULE_MODE)
                        .setValue(IMPORT_SCHEDULE_MODE_DEFAULT_VALUE)
                        .setTitle("Тип повтора импорта из Супермаг+ (интервал/CRON-расписание)")
                        .setType(POPUP)
                        .setChooses(
                                newArrayList(
                                        new ParameterChooseItem()
                                                .setValue(SchedulingModeType.INTERVAL.toString())
                                                .setTitle("ИНТЕРВАЛ"),
                                        new ParameterChooseItem()
                                                .setValue(SchedulingModeType.CRON.toString())
                                                .setTitle("CRON-расписание")
                                )
                        )
        );
        parameterProvider.insert(
                new Parameter()
                        .setName(EXPORT_SCHEDULE_CRON)
                        .setValue("")
                        .setTitle("CRON-расписание экспорта В Супермаг+")
        );
        parameterProvider.insert(
                new Parameter()
                        .setName(EXPORT_SCHEDULE_INTERVAL)
                        .setValue(EXPORT_SCHEDULE_INTERVAL_DEFAULT_VALUE)
                        .setTitle("Интервал повтора экспорта В Супермаг+")
                        .setType(NUMBER)
        );
        parameterProvider.insert(
                new Parameter()
                        .setName(EXPORT_SCHEDULE_MODE)
                        .setValue(EXPORT_SCHEDULE_MODE_DEFAULT_VALUE)
                        .setTitle("Тип повтора экспорта В Супермаг+ (интмервал/CRON-расписание)")
                        .setType(POPUP)
                        .setChooses(
                                newArrayList(
                                        new ParameterChooseItem()
                                                .setValue(SchedulingModeType.INTERVAL.toString())
                                                .setTitle("ИНТЕРВАЛ"),
                                        new ParameterChooseItem()
                                                .setValue(SchedulingModeType.CRON.toString())
                                                .setTitle("CRON-расписание")
                                )
                        )
        );
        parameterProvider.insert(
                new Parameter()
                        .setName(INTEGRATION_SOURCE_FTP_HOST)
                        .setValue("")
                        .setTitle("Адрес сервера FTP")
        );
        parameterProvider.insert(
                new Parameter()
                        .setName(INTEGRATION_SOURCE_FTP_PORT)
                        .setValue("21")
                        .setTitle("Порт сервера FTP")
                        .setType(NUMBER)
        );
        parameterProvider.insert(
                new Parameter()
                        .setName(INTEGRATION_SOURCE_FTP_USERNAME)
                        .setValue("")
                        .setTitle("Пользователь сервера FTP")
        );
        parameterProvider.insert(
                new Parameter()
                        .setName(INTEGRATION_SOURCE_FTP_PASSWORD)
                        .setValue("")
                        .setTitle("Пароль к серверу FTP")
                        .setType(PASSWORD)
        );
        parameterProvider.insert(
                new Parameter()
                        .setName(INTEGRATION_SOURCE_FTP_PATH)
                        .setValue("")
                        .setTitle("Путь к файлам на сервере FTP для загрузки ИЗ Супермаг+")
        );
        parameterProvider.insert(
                new Parameter()
                        .setName(INTEGRATION_SOURCE_FTP_TEMPORARY_PATH)
                        .setValue("./temp")
                        .setTitle("Временный путь, куда скачивать файлы с FTP")
        );
        parameterProvider.insert(
                new Parameter()
                        .setName(INTEGRATION_TARGET_LOCAL_PATH)
                        .setValue("./export")
                        .setTitle("Путь к файлам на сервере FTP для выгрузки В Супермаг+")
        );
        parameterProvider.insert(
                new Parameter()
                        .setName(INTEGRATION_TARGET_LOCAL_TEMPORARY_PATH)
                        .setValue("./temp")
                        .setTitle("Каталог временных файлов для выгрузки В Супермаг+")
        );
        parameterProvider.insert(
                new Parameter()
                        .setName(SESSION_INACTIVE_MINUTES_EXPIRED_INTERVAL)
                        .setValue("60")
                        .setTitle("Интервал бездействия, по истечении которого пользователь отключается")
                        .setType(NUMBER)
        );
    }
}
