package com.it.savant.ukm.invent.service.web.interceptor;

import com.it.savant.ukm.invent.service.web.security.Authorized;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

import static com.it.savant.ukm.invent.service.web.constant.HttpHeaderConstant.HTTP_HEADER_CLIENT_UID;
import static com.it.savant.ukm.invent.service.web.constant.HttpHeaderConstant.HTTP_HEADER_SESSION_UID;

@Slf4j
@Component
public class AuthorizationHandlerInterceptor implements HandlerInterceptor {

    private final Authenticator authenticator;
    private final RequestHeaderExtractor headerExtractor;

    @Autowired
    public AuthorizationHandlerInterceptor(final Authenticator authenticator,
                                           final RequestHeaderExtractor headerExtractor) {
        this.authenticator = authenticator;
        this.headerExtractor = headerExtractor;
    }

    @Override
    public boolean preHandle(final HttpServletRequest request,
                             final HttpServletResponse response,
                             final Object handler)
            throws Exception {
        if (handler != null && handler instanceof HandlerMethod) {
            final HandlerMethod handlerMethod = (HandlerMethod) handler;
            final Authorized authorized = handlerMethod.getMethodAnnotation(Authorized.class);
            if (authorized == null) {
                return true;
            }

            String token = headerExtractor.extract(request, HTTP_HEADER_SESSION_UID);
            if (token == null) {
                if (authorized.administratorOnly()) {
                    log.error("Пользователь не авторизован в системе");
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    return false;
                }
                token = UUID.randomUUID().toString();
            }

            final boolean authenticated = authenticator.authenticate(
                    authorized.administratorOnly(),
                    token,
                    headerExtractor.extract(request, HTTP_HEADER_CLIENT_UID)
            );

            if (authenticated) {
                return true;
            }

            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return false;
        }
        log.error("Пользователь не авторизован в системе");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        return false;
    }

    @Override
    public void postHandle(final HttpServletRequest request,
                           final HttpServletResponse response,
                           final Object handler,
                           final ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(final HttpServletRequest request,
                                final HttpServletResponse response,
                                final Object handler,
                                final Exception exception) throws Exception {

    }
}
