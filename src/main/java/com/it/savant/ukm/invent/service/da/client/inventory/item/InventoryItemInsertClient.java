package com.it.savant.ukm.invent.service.da.client.inventory.item;

import com.it.savant.ukm.invent.service.da.entity.InventoryItemEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryItemInsertClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryItemInsertClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void insert(final InventoryItemEntity item) {
        jdbcTemplate.update(
                "REPLACE INTO " + DATABASE_NAME + ".inventory_item (" +
                        "    docId, \n" +
                        "    docType, \n" +
                        "    specItem, \n" +
                        "    article, \n" +
                        "    displayItem, \n" +
                        " \n" +
                        "    itemPrice, \n" +
                        "    itemPriceCur, \n" +
                        "    quantity, \n" +
                        "    totalPrice, \n" +
                        "    totalPriceCur, \n" +
                        " \n" +
                        "    actualQuantity, \n" +
                        "    awaitQuantity, \n" +
                        "    history, \n" +
                        "    actual, \n" +
                        "    source_docId \n" +
                        ") \n" +
                        "VALUES ( \n" +
                        "   ?, ?, ?, ?, ?, \n" +
                        "   ?, ?, ?, ?, ?, \n" +
                        "   ?, ?, ?, 1, ?\n" +
                        ")",
                item.getDocId(),
                item.getDocType(),
                item.getSpecItem(),
                item.getArticle(),
                item.getDisplayItem(),
                item.getItemPrice(),
                item.getItemPriceCur(),
                0,
                item.getTotalPrice(),
                item.getTotalPriceCur(),
                0,
                0,
                item.getHistory(),
                item.getDocId()
        );
    }
}