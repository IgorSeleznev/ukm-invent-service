package com.it.savant.ukm.invent.service.da.client.inventory.item.brief;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryItemBriefListDeleteByDocIdClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryItemBriefListDeleteByDocIdClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void delete(final String docId) {
        jdbcTemplate.update(
                "DELETE FROM " + DATABASE_NAME + ".inventory_item WHERE docid = ?",
                docId
        );
    }
}
