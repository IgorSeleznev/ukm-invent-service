package com.it.savant.ukm.invent.service.web.controller.admin.user;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.security.User;
import com.it.savant.ukm.invent.service.domain.service.user.UserDetailService;
import com.it.savant.ukm.invent.service.web.json.UserJson;
import com.it.savant.ukm.invent.service.web.security.Authorized;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class UserDetailController {

    private final UserDetailService service;
    private final Converter<User, UserJson> converter;

    @Autowired
    public UserDetailController(final UserDetailService service,
                                final Converter<User, UserJson> converter) {
        this.service = service;
        this.converter = converter;
    }

    @Authorized(administratorOnly = true)
    @GetMapping("/admin/user/detail/{id}")
    public UserJson userDetail(@PathVariable("id") final Long id) {
        log.info("Принят запрос на получение детальной информации о пользователе");
        return converter.convert(
                service.findById(id)
        );
    }
}
