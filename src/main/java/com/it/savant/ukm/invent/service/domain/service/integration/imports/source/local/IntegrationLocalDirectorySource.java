package com.it.savant.ukm.invent.service.domain.service.integration.imports.source.local;

import com.it.savant.ukm.invent.service.common.exception.UkmInventServiceSystemException;
import com.it.savant.ukm.invent.service.domain.service.integration.configuration.IntegrationLocalConfiguration;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.source.IntegrationDirectorySource;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.source.IntegrationFileSource;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Slf4j
public class IntegrationLocalDirectorySource implements IntegrationDirectorySource {

    private final IntegrationLocalConfiguration configuration;

    private final List<File> files;
    private int currentFileIndex = 0;

    public IntegrationLocalDirectorySource(final IntegrationLocalConfiguration configuration) {
        this.configuration = configuration;
        try {
            files = Files.list(Paths.get(configuration.getPath()))
                    .map(Path::toFile)
                    .collect(toList());
            currentFileIndex = 0;
        } catch (final IOException exception) {
            throw new UkmInventServiceSystemException(exception);
        }
    }

    @Override
    public boolean hasFiles() {
        return files != null && currentFileIndex < files.size();
    }

    @Override
    public IntegrationFileSource nextFileSource() {
        try {
            if (files == null) {
                return null;
            }
            if (currentFileIndex >= files.size()) {
                return null;
            }
            final IntegrationLocalFileSource fileSource = new IntegrationLocalFileSource(
                    IntegrationLocalFileSourceConfiguration.builder()
                            .path(configuration.getPath())
                            .filename(files.get(currentFileIndex).getName())
                            .localTemporaryPath(configuration.getLocalTemporaryPath())
                            .build()
            );
            currentFileIndex++;
            return fileSource;

        } catch (final Exception exception) {
            throw new UkmInventServiceSystemException(exception);
        }
    }
}
