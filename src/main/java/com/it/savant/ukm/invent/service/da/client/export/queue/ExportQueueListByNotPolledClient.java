package com.it.savant.ukm.invent.service.da.client.export.queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class ExportQueueListByNotPolledClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ExportQueueListByNotPolledClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<String> listDocumentIdByNotPolled() {
        return jdbcTemplate.query(
                "SELECT docId \n" +
                        "FROM " + DATABASE_NAME + ".export_queue \n" +
                        "WHERE polled = 0",
                (resultSet, i) -> resultSet.getString(1)
        );
    }
}
