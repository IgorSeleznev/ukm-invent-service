package com.it.savant.ukm.invent.service.domain.service.inventory;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.client.inventory.InventoryInsertClient;
import com.it.savant.ukm.invent.service.da.entity.InventoryEntity;
import com.it.savant.ukm.invent.service.domain.model.Inventory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InventoryInsertService {

    private final InventoryInsertClient insertClient;
    private final Converter<Inventory, InventoryEntity> converter;

    @Autowired
    public InventoryInsertService(final InventoryInsertClient insertClient,
                                  final Converter<Inventory, InventoryEntity> converter) {
        this.insertClient = insertClient;
        this.converter = converter;
    }

    public void insert(final Inventory inventory) {
        insertClient.insert(
                converter.convert(inventory)
        );
    }
}
