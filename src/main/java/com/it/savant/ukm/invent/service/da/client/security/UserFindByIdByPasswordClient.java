package com.it.savant.ukm.invent.service.da.client.security;

import com.it.savant.ukm.invent.service.da.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class UserFindByIdByPasswordClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public UserFindByIdByPasswordClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<UserEntity> findByIdByPassword(final Long userId, final String passwordHash) {
        return jdbcTemplate.query(
                "SELECT id, name, location, password_hash, administrator, administrator_login \n" +
                        "FROM " + DATABASE_NAME + ".user \n" +
                        "WHERE id = ? AND password_hash = ? AND deleted = 0 AND administrator = 0",
                (resultSet, i) -> new UserEntity()
                        .setId(resultSet.getLong(1))
                        .setName(resultSet.getString(2))
                        .setLocation(resultSet.getString(3))
                        .setPasswordHash(resultSet.getString(4))
                        .setAdministrator((int) resultSet.getShort(5))
                        .setAdministratorLogin(resultSet.getString(6)),
                userId,
                passwordHash
        );
    }
}
