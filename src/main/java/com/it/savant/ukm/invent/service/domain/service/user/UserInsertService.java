package com.it.savant.ukm.invent.service.domain.service.user;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.client.security.UserInsertClient;
import com.it.savant.ukm.invent.service.da.client.security.UserMaxIdClient;
import com.it.savant.ukm.invent.service.da.entity.UserEntity;
import com.it.savant.ukm.invent.service.domain.model.security.User;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserInsertService {

    private final UserInsertClient client;
    private final UserMaxIdClient maxIdClient;
    private final Converter<User, UserEntity> converter;

    @Autowired
    public UserInsertService(final UserInsertClient client,
                             final UserMaxIdClient maxIdClient,
                             final Converter<User, UserEntity> converter) {
        this.client = client;
        this.maxIdClient = maxIdClient;
        this.converter = converter;
    }

    public User insert(final User user) {
        client.insert(
                converter.convert(
                        user.setPasswordHash(
                                DigestUtils.sha1Hex(user.getPasswordHash())
                        )
                )
        );
        if (user.getId() == null) {
            return user.setId(
                    maxIdClient.maxId()
            );
        }
        return user;
    }
}
