package com.it.savant.ukm.invent.service.da.client.inventory.item;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryManualItemUnActualByParentDocIdClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryManualItemUnActualByParentDocIdClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void unActualize(final String docId) {

        jdbcTemplate.update(
                "UPDATE " + DATABASE_NAME + ".inventory_item \n" +
                        "SET actual = 0 \n" +
                        "WHERE actual = 1 AND docId IN ( \n" +
                        "    SELECT mi.id \n" +
                        "    FROM " + DATABASE_NAME + ".inventory ai, \n" +
                        "         " + DATABASE_NAME + ".inventory mi \n" +
                        "    WHERE mi.orderno = ai.orderno AND mi.location = ai.location \n" +
                        "      AND mi.created_manually = 1 and mi.confirmed = 0 " +
                        "      AND ai.id = ? \n" +
                        ") AND article IN ( \n" +
                        "  SELECT article \n" +
                        "  FROM (" +
                        "    SELECT mii.article \n" +
                        "    FROM " + DATABASE_NAME + ".inventory ai, \n" +
                        "         " + DATABASE_NAME + ".inventory mi, \n" +
                        "         " + DATABASE_NAME + ".inventory_item mii \n" +
                        "    WHERE mi.id = mii.docId \n" +
                        "      AND mi.confirmed = 0 \n" +
                        "      AND mi.orderno = ai.orderno \n" +
                        "      AND mi.created_manually = 1 \n" +
                        "      AND ai.id = ? \n" +
                        "      AND NOT EXISTS ( \n" +
                        "           SELECT 1 \n" +
                        "           FROM " + DATABASE_NAME + ".inventory_item aii \n" +
                        "           WHERE aii.docId = ai.id \n" +
                        "             AND aii.article = mii.article \n" +
                        "             AND aii.actual = 1" +
                        "    ) \n" +
                        "  ) A \n" +
                        ")",
                docId,
                docId
        );
    }
}
