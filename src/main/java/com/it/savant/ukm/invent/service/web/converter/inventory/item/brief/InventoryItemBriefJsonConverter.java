package com.it.savant.ukm.invent.service.web.converter.inventory.item.brief;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.InventoryItemBrief;
import com.it.savant.ukm.invent.service.web.json.InventoryItemBriefJson;
import org.springframework.stereotype.Component;

@Component
public class InventoryItemBriefJsonConverter implements Converter<InventoryItemBrief, InventoryItemBriefJson> {

    @Override
    public InventoryItemBriefJson convert(InventoryItemBrief source) {
        return new InventoryItemBriefJson()
                .setArticle(source.getArticle())
                .setName(source.getName())
                .setQuantity(source.getQuantity())
                .setMeasure(source.getMeasure())
                .setIncreaseStep(source.getIncreaseStep());
    }
}
