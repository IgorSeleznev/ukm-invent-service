package com.it.savant.ukm.invent.service.domain.service.security;

import com.it.savant.ukm.invent.service.domain.model.security.AuthenticateResponse;
import com.it.savant.ukm.invent.service.domain.model.security.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AnonymousSessionCreateService {

    private final AuthorizationUserRegisterService authenticationService;

    @Autowired
    public AnonymousSessionCreateService(final AuthorizationUserRegisterService authenticationService) {
        this.authenticationService = authenticationService;
    }

    public AuthenticateResponse registerAnonymous(final String clientUid) {
        return new AuthenticateResponse().setToken(
                authenticationService.register(
                        new User().setAdministrator(false),
                        clientUid
                ).getToken()
        );
    }

    public AuthenticateResponse registerAnonymous(final String clientUid, final String token) {
        return new AuthenticateResponse().setToken(
                authenticationService.register(
                        new User().setAdministrator(false),
                        clientUid
                ).getToken()
        );
    }
}
