package com.it.savant.ukm.invent.service.domain.service.scheduled;

import org.springframework.scheduling.TriggerContext;

import java.util.Date;

public interface ScheduledService {

    void execute();
    Date nextExecutionTime(final TriggerContext triggerContext);
}
