package com.it.savant.ukm.invent.service.da.client.license;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class LicenseResetAllClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public LicenseResetAllClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void resetAll() {
        jdbcTemplate.update(
                "DELETE FROM " + DATABASE_NAME + ".client_license"
        );
    }
}
