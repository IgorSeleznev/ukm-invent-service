package com.it.savant.ukm.invent.service.domain.service.database;

import com.it.savant.ukm.invent.service.da.client.database.DatabaseCreateClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Slf4j
@Component
public class DatabaseCreateService {

    private final DatabaseCreateClient createClient;

    @Autowired
    public DatabaseCreateService(final DatabaseCreateClient createClient) {
        this.createClient = createClient;
    }

    public void create() {
        log.info("Создание базы данных {}", DATABASE_NAME.toUpperCase());
        createClient.create(DATABASE_NAME);
        log.info("База данных {} успешно создана.", DATABASE_NAME.toUpperCase());
    }
}
