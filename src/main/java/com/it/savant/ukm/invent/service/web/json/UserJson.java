package com.it.savant.ukm.invent.service.web.json;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class UserJson {

    private Long id;
    private String name;
    private String administratorLogin;
    private String passwordHash;
    private String location;
    private Boolean administrator;
}
