package com.it.savant.ukm.invent.service.domain.service.integration.exports;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.scheduling.TriggerContext;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;

@Slf4j
@Service
public class ExportQueueTableListenNextExecutionTimeService {

    private static final long EXPORT_QUEUE_TABLE_LISTEN_INTERVAL = 5000;

    public Date nextExecutionTime(final TriggerContext triggerContext) {
        try {
            val lastScheduled = defaultIfNull(
                    triggerContext.lastScheduledExecutionTime(),
                    new Date()
            );
            return new Date(lastScheduled.getTime() + EXPORT_QUEUE_TABLE_LISTEN_INTERVAL);
        } catch (final Exception exception) {
            final Date nextDate = new Date(new Date().getTime() + 60000);
            log.error("Has got error when try to schedule next listen of  export queue table.", exception);
            log.error("Next listen of export queue table will run at {}", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(nextDate));

            return nextDate;
        }
    }
}
