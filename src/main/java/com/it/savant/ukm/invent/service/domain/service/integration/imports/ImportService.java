package com.it.savant.ukm.invent.service.domain.service.integration.imports;

import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.parser.XmlNode;

public interface ImportService {

    boolean isValid(final XmlNode rootNode);

    void doImport(final XmlNode rootNode);
}
