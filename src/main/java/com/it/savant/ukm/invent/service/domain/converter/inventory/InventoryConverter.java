package com.it.savant.ukm.invent.service.domain.converter.inventory;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.entity.InventoryEntity;
import com.it.savant.ukm.invent.service.da.entity.InventoryItemEntity;
import com.it.savant.ukm.invent.service.domain.model.Inventory;
import com.it.savant.ukm.invent.service.domain.model.InventoryItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class InventoryConverter implements Converter<InventoryEntity, Inventory> {

    private final Converter<InventoryItemEntity, InventoryItem> converter;
    private final InventoryItemListSpecItemFiller inventoryItemFiller;

    @Autowired
    public InventoryConverter(final Converter<InventoryItemEntity, InventoryItem> converter,
                              final InventoryItemListSpecItemFiller inventoryItemFiller) {
        this.converter = converter;
        this.inventoryItemFiller = inventoryItemFiller;
    }

    @Override
    public Inventory convert(final InventoryEntity source) {
        return new Inventory()
                .setId(source.getId())
                .setDocType(source.getDocType())
                .setBornIn(source.getBornIn())
                .setCommentary(source.getCommentary())
                .setCreateDat(source.getCreateDat())

                .setCurrencyMultOrder(source.getCurrencyMultOrder())
                .setCurrencyRate(source.getCurrencyRate())
                .setCurrencyType(source.getCurrencyType())
                .setDocState(source.getDocState())
                .setIsRoubles(source.getIsRoubles())

                .setLocation(source.getLocation())
                .setOpCode(source.getOpCode())
                .setPriceRoundMode(source.getPriceRoundMode())
                .setTotalSum(source.getTotalSum())
                .setTotalSumCur(source.getTotalSumCur())

                .setFillSpecType(source.getFillSpecType())
                .setFinalDate(source.getFinalDate())
                .setIsActiveOnly(source.getIsActiveOnly())
                .setIsFillComplete(source.getIsFillComplete())
                .setOrderNo(source.getOrderNo())

                .setOurSelfClient(source.getOurSelfClient())
                .setPreambleDate(source.getPreambleDate())
                .setPriceMode(source.getPriceMode())
                .setPriceType(source.getPriceType())
                .setStoreLoc(source.getStoreLoc())

                .setWithDue(source.getWithDue())
                .setItems(
                        inventoryItemFiller.fill(
                                source.getItems().stream()
                                        .map(converter::convert)
                                        .collect(Collectors.toList())
                        )
                )
                .setConfirmed(source.getConfirmed())
                .setCreatedManually(source.getCreatedManually());
    }
}
