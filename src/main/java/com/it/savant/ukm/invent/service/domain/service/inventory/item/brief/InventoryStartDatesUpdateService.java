package com.it.savant.ukm.invent.service.domain.service.inventory.item.brief;

import com.it.savant.ukm.invent.service.da.client.inventory.InventoryCreateDatUpdateClient;
import com.it.savant.ukm.invent.service.da.client.inventory.InventoryPreambleDateUpdateClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class InventoryStartDatesUpdateService {

    private final InventoryPreambleDateUpdateClient preambleDateUpdateClient;
    private final InventoryCreateDatUpdateClient createDatUpdateClient;

    @Autowired
    public InventoryStartDatesUpdateService(final InventoryPreambleDateUpdateClient preambleDateUpdateClient,
                                            final InventoryCreateDatUpdateClient createDatUpdateClient) {
        this.preambleDateUpdateClient = preambleDateUpdateClient;
        this.createDatUpdateClient = createDatUpdateClient;
    }

    public void update(final String docId, final Date date) {
        final String dateString = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(date);
        preambleDateUpdateClient.update(docId, dateString);
        createDatUpdateClient.update(docId, dateString);
    }
}
