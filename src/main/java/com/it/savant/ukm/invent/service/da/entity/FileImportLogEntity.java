package com.it.savant.ukm.invent.service.da.entity;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class FileImportLogEntity {

    private String filename;
    private String content;
    private boolean success;
    private String errorMessage;
}
