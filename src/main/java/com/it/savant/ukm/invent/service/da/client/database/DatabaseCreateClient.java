package com.it.savant.ukm.invent.service.da.client.database;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class DatabaseCreateClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public DatabaseCreateClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void create(final String databaseName) {
        jdbcTemplate.update(
                "CREATE DATABASE " + databaseName + "\n" +
                        "  CHARACTER SET utf8\n" +
                        "  COLLATE utf8_general_ci;"
        );
    }
}
