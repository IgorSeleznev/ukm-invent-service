package com.it.savant.ukm.invent.service.da.client.store;

import com.it.savant.ukm.invent.service.da.entity.StoreBriefEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class StoreBriefListClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public StoreBriefListClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<StoreBriefEntity> findAll() {
        return jdbcTemplate.query(
                "SELECT id, name \n" +
                        "FROM " + DATABASE_NAME + ".store",
                (resultSet, i) -> {
                    return new StoreBriefEntity()
                            .setId(resultSet.getString(1))
                            .setName(resultSet.getString(2));
                }
        );
    }
}
