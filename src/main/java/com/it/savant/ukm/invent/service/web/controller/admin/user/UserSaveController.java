package com.it.savant.ukm.invent.service.web.controller.admin.user;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.security.User;
import com.it.savant.ukm.invent.service.domain.service.user.UserSaveService;
import com.it.savant.ukm.invent.service.web.json.Response;
import com.it.savant.ukm.invent.service.web.json.UserJson;
import com.it.savant.ukm.invent.service.web.security.Authorized;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.it.savant.ukm.invent.service.web.json.Response.success;

@Slf4j
@RestController
public class UserSaveController {

    private final UserSaveService service;
    private final Converter<UserJson, User> userConverter;

    @Autowired
    public UserSaveController(final UserSaveService service,
                              final Converter<UserJson, User> userConverter) {
        this.service = service;
        this.userConverter = userConverter;
    }

    @Authorized(administratorOnly = true)
    @RequestMapping("/admin/user/save")
    public Response<Void> update(@RequestBody final UserJson request) {
        log.info("Принят запрос на сохранение пользователя");
        service.save(
                userConverter.convert(
                        request
                )
        );
        return success();
    }
}
