package com.it.savant.ukm.invent.service.domain.model.security;

import org.jlead.assets.security.SecurityGroup;

public class AuthorizedUserSecurityGroup implements SecurityGroup {

    private final User user;

    public AuthorizedUserSecurityGroup(final User user) {
        this.user = user;
    }

    @Override
    public boolean containsRoleName(String s) {
        return user.getAdministrator() && "ADMINISTRATOR".equals(s);
    }
}
