package com.it.savant.ukm.invent.service.da.client.export.record;

import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.client.export.record.InventoryExportRecordSqlConst.EXPORT_RECORD_FILTER_SQL;
import static org.apache.commons.lang3.StringUtils.isNoneEmpty;

@Component
public class InventoryExportRecordListSqlBuilder {

    private final InventoryExportRecordListSqlOrderExpressionBuilder orderExpressionBuilder;

    public InventoryExportRecordListSqlBuilder(final InventoryExportRecordListSqlOrderExpressionBuilder orderExpressionBuilder) {
        this.orderExpressionBuilder = orderExpressionBuilder;
    }

    public String build(final int pageNumber, final int pageSize, final String orderName, final String direction) {
        final StringBuilder sqlBuilder = new StringBuilder(
                "SELECT q.docId, i.orderno, i.finalDate, q.polled \n" + EXPORT_RECORD_FILTER_SQL
        );
        final String sqlOrderExpression = orderExpressionBuilder.build(orderName, direction);
        if (isNoneEmpty(sqlOrderExpression)) {
            sqlBuilder.append("ORDER BY ").append(sqlOrderExpression);
        }
        sqlBuilder.append(" LIMIT ").append(pageNumber * pageSize).append(", ").append(pageSize);

        return sqlBuilder.toString();
    }
}

