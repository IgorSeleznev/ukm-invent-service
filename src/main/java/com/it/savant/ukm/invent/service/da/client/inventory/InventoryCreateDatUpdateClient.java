package com.it.savant.ukm.invent.service.da.client.inventory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryCreateDatUpdateClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryCreateDatUpdateClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void update(final String docId, final String createDat) {
        jdbcTemplate.update(
                "UPDATE " + DATABASE_NAME + ".inventory \n" +
                        "SET createDat = ? \n" +
                        "WHERE id = ?",
                createDat,
                docId
        );
    }
}