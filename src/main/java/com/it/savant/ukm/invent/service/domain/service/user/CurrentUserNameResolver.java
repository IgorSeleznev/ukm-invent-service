package com.it.savant.ukm.invent.service.domain.service.user;

import com.it.savant.ukm.invent.service.domain.model.security.User;
import org.jlead.assets.security.v2.AuthenticationHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CurrentUserNameResolver {

    private final AuthenticationHolder<User, Boolean> authenticationHolder;

    @Autowired
    public CurrentUserNameResolver(final AuthenticationHolder<User, Boolean> authenticationHolder) {
        this.authenticationHolder = authenticationHolder;
    }

    public String resolve() {
        return authenticationHolder.current().getMember().getIdentification().getName();
    }
}
