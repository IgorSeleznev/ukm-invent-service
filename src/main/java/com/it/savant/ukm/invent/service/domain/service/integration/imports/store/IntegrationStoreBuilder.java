package com.it.savant.ukm.invent.service.domain.service.integration.imports.store;

import com.it.savant.ukm.invent.service.domain.model.Store;
import com.it.savant.ukm.invent.service.domain.model.integration.IntegrationStorePackage;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.store.converter.IntegrationStoreConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IntegrationStoreBuilder {

    private final IntegrationStoreConverter storeConverter;

    @Autowired
    public IntegrationStoreBuilder(final IntegrationStoreConverter storeConverter) {
        this.storeConverter = storeConverter;
    }

    public Store build(final IntegrationStorePackage source) {
        return storeConverter.convert(source);
    }
}