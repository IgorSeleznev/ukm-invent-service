package com.it.savant.ukm.invent.service.domain.service.inventory;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.client.inventory.item.brief.InventoryDifferenceItemListByDocIdClient;
import com.it.savant.ukm.invent.service.da.entity.InventoryDifferenceItemEntity;
import com.it.savant.ukm.invent.service.da.entity.InventoryItemBriefEntity;
import com.it.savant.ukm.invent.service.domain.model.InventoryDifferenceItem;
import com.it.savant.ukm.invent.service.domain.model.InventoryItemBrief;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class InventoryManuallyDifferenceItemListService {

    private final InventoryDifferenceItemListByDocIdClient client;
    private final Converter<InventoryItemBriefEntity, InventoryItemBrief> converter;

    @Autowired
    public InventoryManuallyDifferenceItemListService(
            final InventoryDifferenceItemListByDocIdClient client,
            final Converter<InventoryItemBriefEntity, InventoryItemBrief> converter
    ) {
        this.client = client;
        this.converter = converter;
    }

    public List<InventoryItemBrief> differenceList(final String docId) {
        return client.differenceListByDocId(docId).stream()
                .map(converter::convert)
                .collect(Collectors.toList());
    }
}
