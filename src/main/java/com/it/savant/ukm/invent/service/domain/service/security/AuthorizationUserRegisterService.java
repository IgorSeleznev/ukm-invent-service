package com.it.savant.ukm.invent.service.domain.service.security;

import com.it.savant.ukm.invent.service.domain.model.security.AuthenticateResponse;
import com.it.savant.ukm.invent.service.domain.model.security.User;
import com.it.savant.ukm.invent.service.web.security.Authenticated;
import com.it.savant.ukm.invent.service.web.security.AuthenticatedUser;
import org.jlead.assets.security.v2.AuthenticationHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

@Service
public class AuthorizationUserRegisterService {

    private final AuthenticationHolder<User, Boolean> authenticatedHolder;

    @Autowired
    public AuthorizationUserRegisterService(final AuthenticationHolder<User, Boolean> authenticatedHolder) {
        this.authenticatedHolder = authenticatedHolder;
    }

    public AuthenticateResponse register(final User user, final String clientUid) {
        return register(user, clientUid, UUID.randomUUID().toString());
    }

    public AuthenticateResponse register(final User user, final String clientUid, final String token) {
        authenticatedHolder.register(
                Authenticated.builder()
                        .lastActivityTime(new Date())
                        .token(token)
                        .clientUid(clientUid)
                        .member(new AuthenticatedUser(user))
                        .build()
        );

        return new AuthenticateResponse().setToken(token);
    }
}
