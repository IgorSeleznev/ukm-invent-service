package com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.parser;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Data
@Accessors(chain = true)
public class XmlNode {

    private String name;
    private XmlNode parent;
    private Map<String, String> values = new HashMap<>();
    private List<XmlNode> nodes = new LinkedList<>();

    @Override
    public String toString() {
        return "XmlNode{" +
                "name='" + name + '\'' +
                ", parent=" + (parent == null ? "null" : parent.getName()) +
                ", attributes=" + values +
                ", nodes=" + nodes +
                '}';
    }
}
