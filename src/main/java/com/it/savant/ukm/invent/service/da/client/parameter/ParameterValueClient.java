package com.it.savant.ukm.invent.service.da.client.parameter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class ParameterValueClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ParameterValueClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public String value(final String parameterName) {
        final List<String> values = jdbcTemplate.query(
                "SELECT value \n" +
                        "FROM " + DATABASE_NAME + ".parameter \n" +
                        "WHERE name = ?",
                (resultSet, i) -> resultSet.getString(1),
                parameterName
        );
        if (values.size() == 0) {
            return null;
        }
        return values.get(0);
    }
}
