package com.it.savant.ukm.invent.service.domain.constants;

public interface ParameterConstants {

    String DOCID_SEQUENCE = "docId.sequence";
    String DOCID_PREFIX_PLACEHOLDER = "docId.prefix.placeholder";
    String INTEGRATION_TYPE = "integration.type";
    String INTEGRATION_TYPE_DEFAULT_VALUE = "LOCAL";
    String IMPORT_SCHEDULE_MODE = "import.schedule.mode";
    String IMPORT_SCHEDULE_MODE_DEFAULT_VALUE = "INTERVAL";
    String IMPORT_FAILED_TRY_TIMEOUT = "import.failed.try.timeout";
    String IMPORT_FAILED_TRY_TIMEOUT_DEFAULT_VALUE = "60000";
    String INTEGRATION_SOURCE_FTP_HOST = "integration.source.ftp.host";
    String INTEGRATION_SOURCE_FTP_PORT = "integration.source.ftp.port";
    String INTEGRATION_SOURCE_FTP_USERNAME = "integration.source.ftp.username";
    String INTEGRATION_SOURCE_FTP_PASSWORD = "integration.source.ftp.password";
    String INTEGRATION_SOURCE_FTP_PATH = "integration.source.ftp.path";
    String INTEGRATION_SOURCE_FTP_TEMPORARY_PATH = "integration.source.ftp.temporary.path";
    String INTEGRATION_SOURCE_LOCAL_PATH = "integration.source.local.path";
    String INTEGRATION_SOURCE_LOCAL_TEMPORARY_PATH = "integration.source.local.temporary.path";
    String IMPORT_SCHEDULE_CRON = "import.schedule.cron";
    String IMPORT_SCHEDULE_INTERVAL = "import.schedule.interval";
    String IMPORT_SCHEDULE_INTERVAL_DEFAULT_VALUE = "300000";
    String EXPORT_SCHEDULE_MODE = "export.schedule.mode";
    String EXPORT_SCHEDULE_CRON = "export.schedule.cron";
    String EXPORT_SCHEDULE_MODE_DEFAULT_VALUE = "INTERVAL";
    String EXPORT_SCHEDULE_INTERVAL = "export.schedule.interval";
    String EXPORT_SCHEDULE_INTERVAL_DEFAULT_VALUE = "300000";
    String EXPORT_FAILED_TRY_TIMEOUT = "export.failed.try.timeout";
    String EXPORT_FAILED_TRY_TIMEOUT_DEFAULT_VALUE = "60000";
    String INTEGRATION_TARGET_FTP_PATH = "integration.target.ftp.path";
    String INTEGRATION_TARGET_FTP_TEMPORARY_PATH = "integration.target.ftp.temporary.path";
    String INTEGRATION_TARGET_LOCAL_PATH = "integration.target.local.path";
    String INTEGRATION_TARGET_LOCAL_TEMPORARY_PATH = "integration.target.local.temporary.path";
    String SESSION_INACTIVE_MINUTES_EXPIRED_INTERVAL = "session.inactive.minutes.expired.interval";
}
