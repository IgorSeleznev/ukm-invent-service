package com.it.savant.ukm.invent.service.da.client.inventory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryExistsClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryExistsClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public boolean exists(final String docId) {
        final List<Integer> counts = jdbcTemplate.query(
                "SELECT count(*) \n" +
                        "FROM " + DATABASE_NAME + ".inventory \n" +
                        "WHERE id = ?",
                (resultSet, i) -> resultSet.getInt(1),
                docId
        );

        if (counts.size() == 0) {
            return false;
        }

        return counts.get(0) > 0;
    }
}
