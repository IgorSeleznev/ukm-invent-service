package com.it.savant.ukm.invent.service.domain.service.integration.exports;

import com.it.savant.ukm.invent.service.domain.model.Inventory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Component
public class IntegrationInventoryItemListXmlBuilder {

    private final IntegrationInventoryItemXmlBuilder itemXmlBuilder;

    @Autowired
    public IntegrationInventoryItemListXmlBuilder(final IntegrationInventoryItemXmlBuilder itemXmlBuilder) {
        this.itemXmlBuilder = itemXmlBuilder;
    }

    public List<String> xmlBuild(final Inventory inventory) {
        final StringBuilder specBuilder = new StringBuilder();
        final StringBuilder specIlBuilder = new StringBuilder();
        inventory.getItems().forEach(
                item -> {
                    final List<String> itemXml = itemXmlBuilder.xmlBuild(item);
                    specBuilder.append(itemXml.get(0));
                    specIlBuilder.append(itemXml.get(1));
                }
        );

        return newArrayList(
                specBuilder.toString(),
                specIlBuilder.toString()
        );
    }
}
