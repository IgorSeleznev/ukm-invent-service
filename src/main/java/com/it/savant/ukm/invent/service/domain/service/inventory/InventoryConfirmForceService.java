package com.it.savant.ukm.invent.service.domain.service.inventory;

import com.it.savant.ukm.invent.service.common.exception.UkmInventServiceBusinessException;
import com.it.savant.ukm.invent.service.da.client.inventory.InventoryConfirmClient;
import com.it.savant.ukm.invent.service.da.client.inventory.InventoryExistsByDocIdByNotConfirmedClient;
import com.it.savant.ukm.invent.service.domain.model.security.User;
import com.it.savant.ukm.invent.service.domain.service.user.CurrentUserNameResolver;
import org.jlead.assets.security.v2.AuthenticationHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class InventoryConfirmForceService {

    private final InventoryExistsByDocIdByNotConfirmedClient existsClient;
    private final InventoryConfirmClient confirmClient;
    private final InventoryManuallyCheckedCreateService checkedCreateService;
    private final CurrentUserNameResolver userNameResolver;

    @Autowired
    public InventoryConfirmForceService(
            final InventoryExistsByDocIdByNotConfirmedClient existsClient,
            final InventoryConfirmClient confirmClient,
            final InventoryManuallyCheckedCreateService checkedCreateService,
            final CurrentUserNameResolver userNameResolver
    ) {
        this.existsClient = existsClient;
        this.confirmClient = confirmClient;
        this.checkedCreateService = checkedCreateService;
        this.userNameResolver = userNameResolver;
    }

    @Transactional
    public void confirmForce(final String docId, final String clientId) {
        if (!existsClient.exists(docId)) {
            throw new UkmInventServiceBusinessException("Акт инвентаризации не существует или уже был подтвержден");
        }
        confirmClient.confirm(
                docId,
                new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date()),
                userNameResolver.resolve()
        );
        checkedCreateService.checkAndCreate(docId, clientId);
    }
}
