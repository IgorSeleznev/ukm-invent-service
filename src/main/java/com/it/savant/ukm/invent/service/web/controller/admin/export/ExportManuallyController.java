package com.it.savant.ukm.invent.service.web.controller.admin.export;

import com.it.savant.ukm.invent.service.domain.service.export.ExportManuallyService;
import com.it.savant.ukm.invent.service.web.json.DocIdRequestJson;
import com.it.savant.ukm.invent.service.web.security.Authorized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExportManuallyController {

    private final ExportManuallyService service;

    @Autowired
    public ExportManuallyController(final ExportManuallyService service) {
        this.service = service;
    }

    @Authorized(administratorOnly = true)
    @PostMapping("/admin/export/manually")
    public void export(@RequestBody final DocIdRequestJson request) {
        service.export(request.getDocId());
    }
}
