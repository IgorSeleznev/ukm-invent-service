package com.it.savant.ukm.invent.service.web.interceptor;

import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class RequestHeaderExtractor {

    public String extract(final HttpServletRequest request, final String headerName) {
        String value = request.getHeader(headerName);
        if (value == null) {
            value = request.getHeader(headerName.toLowerCase());
        }
        if (value == null) {
            value = request.getHeader(headerName.toLowerCase().replace('_', '-'));
        }
        return value;
    }
}
