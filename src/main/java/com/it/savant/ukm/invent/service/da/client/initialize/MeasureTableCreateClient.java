package com.it.savant.ukm.invent.service.da.client.initialize;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class MeasureTableCreateClient implements CreateTableClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public MeasureTableCreateClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public String getTableName() {
        return "measure";
    }

    @Override
    public void create() {
        jdbcTemplate.update(
                "CREATE TABLE IF NOT EXISTS " + DATABASE_NAME + ".measure ( " +
                        "    id VARCHAR(7) NOT NULL, \n" +
                        "    abbrev VARCHAR(6), \n" +
                        "    baseType VARCHAR(7), \n" +
                        "    code VARCHAR(3), \n" +
                        "    codeIso VARCHAR(3), \n" +
                        "    mesType VARCHAR(1), \n" +
                        "    name VARCHAR(20), \n" +
                        "    prec VARCHAR(7), \n" +
                        "    unitName VARCHAR(20), " +
                        "    increase_step VARCHAR(10) DEFAULT '1' NOT NULL, \n" +
                        "    PRIMARY KEY (id) \n" +
                        ") ENGINE = InnoDB DEFAULT CHARSET = UTF8"
        );
    }
}
