package com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.it.savant.ukm.invent.service.common.exception.UkmInventServiceBusinessException;
import com.it.savant.ukm.invent.service.domain.model.integration.IntegrationInventoryPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IntegrationInventoryXmlParseService {

    private final XmlMapper xmlMapper;

    @Autowired
    public IntegrationInventoryXmlParseService(final XmlMapper xmlMapper) {
        this.xmlMapper = xmlMapper;
    }

    public IntegrationInventoryPackage parse(final String xml) {
        try {
            final JsonNode rootNode = xmlMapper.readTree(xml);
            final IntegrationInventoryPackage result = new IntegrationInventoryPackage();
            //rootNode.fields().elements();
            return xmlMapper.readValue(xml, IntegrationInventoryPackage.class);
        } catch (final JsonProcessingException e) {
            e.printStackTrace();
            throw new UkmInventServiceBusinessException(e);
        }
    }
}
