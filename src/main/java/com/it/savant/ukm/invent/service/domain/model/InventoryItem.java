package com.it.savant.ukm.invent.service.domain.model;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class InventoryItem {

    private String docId;
    private String docType;
    private String specItem;
    private String article;
    private String displayItem;
    private String itemPrice;
    private String itemPriceCur;
    private String quantity;
    private String totalPrice;
    private String totalPriceCur;
    private String actualQuantity;
    private String awaitQuantity;
    private String history;
}
