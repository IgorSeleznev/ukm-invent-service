package com.it.savant.ukm.invent.service.domain.service.integration.exports;

import com.it.savant.ukm.invent.service.da.client.export.queue.ExportQueueListByNotPolledClient;
import com.it.savant.ukm.invent.service.da.client.export.queue.ExportQueueMarkExportedClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExportQueueTableListenService {

    private final InventoryExportEventAsyncDocIdQueue queue;
    private final ExportQueueListByNotPolledClient listenClient;
    private final ExportQueueMarkExportedClient markExportedClient;

    @Autowired
    public ExportQueueTableListenService(final InventoryExportEventAsyncDocIdQueue queue,
                                         final ExportQueueListByNotPolledClient listenClient,
                                         final ExportQueueMarkExportedClient markExportedClient) {
        this.queue = queue;
        this.listenClient = listenClient;
        this.markExportedClient = markExportedClient;
    }

    public void listen() {
        listenClient.listDocumentIdByNotPolled().forEach(
                docId -> {
                    queue.push(docId);
                    markExportedClient.markPolled(docId);
                }
        );
    }

}
