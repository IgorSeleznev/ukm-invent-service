package com.it.savant.ukm.invent.service.domain.service.integration.imports;

import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.parser.XmlNode;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.parser.XmlParser;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.source.IntegrationDirectorySource;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.source.IntegrationFileSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Slf4j
@Component
public class ImportManager {

    private volatile boolean inImport = false;

    private final IntegrationDirectorySourceResolver sourceResolver;
    private final ImportServiceResolver importServiceResolver;
    private final XmlParser xmlParser;
    private final FileImportLogService logService;

    @Autowired
    public ImportManager(final IntegrationDirectorySourceResolver sourceResolver,
                         final ImportServiceResolver importServiceResolver,
                         final XmlParser xmlParser,
                         final FileImportLogService logService) {
        this.sourceResolver = sourceResolver;
        this.importServiceResolver = importServiceResolver;
        this.xmlParser = xmlParser;
        this.logService = logService;
    }

    public void doImport() {
        //TODO: получить источник
        if (inImport) {
            return;
        }
        synchronized (this) {
            if (inImport) {
                return;
            }
            log.warn("Запущен импорт данных из Супермаг+ в систему инвентаризации, но процесс импорта уже осуществляется. Попытка повторного запуска отклонена.");
            inImport = true;
            try {
                log.info("Запущен импорт данных из Супермаг+ в систему инвентаризации.");
                final IntegrationDirectorySource directorySource = sourceResolver.resolve();
                while (directorySource.hasFiles()) {
                    //TODO: итерация по коллекции ImportService: прочитать из источника, направить результат в того, кто сохраняет
                    final IntegrationFileSource fileSource = directorySource.nextFileSource();
                    String content = "";
                    try {
                        log.info("Импорт данных: чтение файл '{}'", fileSource.getFilename());
                        content = fileSource.read();
                        final XmlNode rootNode = xmlParser.parse(content);
                        final Optional<ImportService> importService = importServiceResolver.resolve(rootNode);
                        importService.ifPresent(service -> service.doImport(rootNode));
                        logService.logSuccess(fileSource.getFilename(), content);
                        log.info("Импорт данных: удаление файла '{}'", fileSource.getFilename());
                        fileSource.deleteOriginalFileService().delete();
                        fileSource.deleteTemporaryFileService().delete();
                    } catch (final Exception exception) {
                        try {
                            log.error("Ошибка импорта файла '" + fileSource.getFilename() + "'", exception);
                            logService.logFailure(fileSource.getFilename(), content, exception);
                            fileSource.deleteTemporaryFileService().delete();
                        } catch (final Exception e) {

                        }
                    }
                }
            } catch (final Exception exception) {
                log.error("Error with directory", exception);
            }
            log.info("Импорт данных из Супермаг+ в систему инвентаризации завершен.");
            inImport = false;
        }
    }
}