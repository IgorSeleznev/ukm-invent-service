package com.it.savant.ukm.invent.service.domain.service.integration.imports.storelocations;

import com.it.savant.ukm.invent.service.domain.service.integration.imports.ImportService;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.IntegrationDeserializeService;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.parser.XmlNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class IntegrationSmStoreLocationsImportService implements ImportService {


    private final IntegrationDeserializeService deserializeService;
    private final IntegrationSmStoreLocationsSaveService saveService;

    @Autowired
    public IntegrationSmStoreLocationsImportService(final IntegrationDeserializeService deserializeService,
                                                    final IntegrationSmStoreLocationsSaveService saveService) {
        this.deserializeService = deserializeService;
        this.saveService = saveService;
    }

    @Override
    public boolean isValid(final XmlNode xml) {
        final List<XmlNode> nodes = xml.getNodes();
        for (final XmlNode node : nodes) {
            if ("SmStoreLocations".equals(node.getName())) {
                log.info("Импорт данных: обнаружен файл описания SmStoreLocations");
                return true;
            }
        }

        return false;
    }

    @Override
    public void doImport(final XmlNode xml) {
        log.info("Импорт данных: SmStoreLocations");
        saveService.save(
                deserializeService.deserialize(xml, "SmStoreLocations")
        );
    }
}
