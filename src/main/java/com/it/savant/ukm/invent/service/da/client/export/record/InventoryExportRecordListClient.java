package com.it.savant.ukm.invent.service.da.client.export.record;

import com.it.savant.ukm.invent.service.da.entity.InventoryExportRecordEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class InventoryExportRecordListClient {

    private final JdbcTemplate jdbcTemplate;
    private final InventoryExportRecordListSqlBuilder sqlBuilder;

    @Autowired
    public InventoryExportRecordListClient(
            final JdbcTemplate jdbcTemplate,
            final InventoryExportRecordListSqlBuilder sqlBuilder
    ) {
        this.jdbcTemplate = jdbcTemplate;
        this.sqlBuilder = sqlBuilder;
    }

    public List<InventoryExportRecordEntity> list(
            final int pageNumber,
            final int pageSize,
            final String orderName,
            final String direction
    ) {
        return jdbcTemplate.query(
                sqlBuilder.build(pageNumber, pageSize, orderName, direction),
                (resultSet, i) -> new InventoryExportRecordEntity()
                        .setDocId(resultSet.getString(1))
                        .setAssortmentName(resultSet.getString(2))
                        .setConfirmDate(resultSet.getString(3))
                        .setPolled(resultSet.getShort(4))
        );
    }
}
