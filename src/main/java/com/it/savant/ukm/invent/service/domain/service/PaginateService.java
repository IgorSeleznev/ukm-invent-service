package com.it.savant.ukm.invent.service.domain.service;

import com.it.savant.ukm.invent.service.domain.model.PageableSortableRequest;
import com.it.savant.ukm.invent.service.domain.model.ResultPage;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.it.savant.ukm.invent.service.domain.model.ResultPage.resultOf;

@Service
public class PaginateService {

    public <T> ResultPage<T> paginate(final List<T> list, final PageableSortableRequest request) {
        if (list.size() >= request.getPageNumber() * request.getPageSize() + request.getPageSize()) {
            return resultOf(
                    list.subList(
                            request.getPageNumber() * request.getPageSize() > list.size()
                                    ? list.size() - 1
                                    : request.getPageNumber() * request.getPageSize(),
                            request.getPageNumber() * request.getPageSize() + request.getPageSize()
                    ),
                    list.size()
            );
        }
        return resultOf(
                list.subList(
                        request.getPageNumber() * request.getPageSize() > list.size()
                                ? list.size() - 1
                                : request.getPageNumber() * request.getPageSize(),
                        list.size()
                ),
                list.size()
        );
    }
}