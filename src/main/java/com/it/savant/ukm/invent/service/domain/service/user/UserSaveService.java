package com.it.savant.ukm.invent.service.domain.service.user;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.client.security.UserInsertClient;
import com.it.savant.ukm.invent.service.da.client.security.UserUpdateClient;
import com.it.savant.ukm.invent.service.da.entity.UserEntity;
import com.it.savant.ukm.invent.service.domain.model.security.User;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserSaveService {

    private final UserFindByIdService findService;
    private final UserUpdateClient updateClient;
    private final UserInsertClient insertClient;
    private final Converter<User, UserEntity> converter;

    @Autowired
    public UserSaveService(final UserFindByIdService findService,
                           final UserUpdateClient updateClient,
                           final UserInsertClient insertClient,
                           final Converter<User, UserEntity> converter) {
        this.findService = findService;
        this.updateClient = updateClient;
        this.insertClient = insertClient;
        this.converter = converter;
    }

    public void save(final User user) {
        if (user.getId() == null) {
            insertClient.insert(
                    converter.convert(
                            user.setPasswordHash(
                                    DigestUtils.sha1Hex(user.getPasswordHash())
                            )
                    )
            );
        } else {
            final User actual = findService.findById(user.getId());
            updateClient.update(
                    converter.convert(
                            user.setPasswordHash(
                                    actual.getPasswordHash().equals(user.getPasswordHash())
                                            ? actual.getPasswordHash()
                                            : DigestUtils.sha1Hex(user.getPasswordHash())
                            )
                    )
            );
        }
    }
}