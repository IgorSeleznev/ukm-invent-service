package com.it.savant.ukm.invent.service.da.client.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class UserAdministratorCountClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public UserAdministratorCountClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Long administratorsCount() {
        final List<Long> counts = jdbcTemplate.query(
                "SELECT count(*) \n" +
                        "FROM " + DATABASE_NAME + ".user \n" +
                        "WHERE administrator = 1",
                (resultSet, i) -> resultSet.getLong(1)
        );
        if (counts.get(0) == null) {
            return 0L;
        }
        return counts.get(0);
    }
}
