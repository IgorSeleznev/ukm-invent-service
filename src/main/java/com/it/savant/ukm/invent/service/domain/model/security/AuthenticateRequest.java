package com.it.savant.ukm.invent.service.domain.model.security;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class AuthenticateRequest {

    public static AuthenticateRequest anonymousRequest() {
        return new AuthenticateRequest();
    }

    private Long id;
    private String password;
}
