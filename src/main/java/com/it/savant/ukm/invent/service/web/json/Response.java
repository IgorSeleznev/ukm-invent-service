package com.it.savant.ukm.invent.service.web.json;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class Response<T> {

    public static Response<Void> success() {
        return new Response<Void>().setSuccess(true);
    }

    public static <T> Response<T> success(final T body) {
        return new Response<T>()
                .setSuccess(true)
                .setBody(body);
    }

    public static <T> Response<T> failed(final String message) {
        return new Response<T>()
                .setSuccess(false)
                .setMessage(message);
    }

    public static <T> Response<T> failed(final String message, final T body) {
        return new Response<T>()
                .setSuccess(false)
                .setMessage(message)
                .setBody(body);
    }

    private boolean success;
    private String message;
    private T body;
}
