package com.it.savant.ukm.invent.service.domain.service.security;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.scheduling.TriggerContext;
import org.springframework.stereotype.Service;

import java.util.Date;

import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;

@Slf4j
@Service
public class UnAuthenticateNextExecutionTimeService {

    public Date nextExecutionTime(final TriggerContext triggerContext) {
        val lastScheduled = defaultIfNull(triggerContext.lastScheduledExecutionTime(), new Date());
        return new Date(lastScheduled.getTime() + 60000);
    }
}
