package com.it.savant.ukm.invent.service.domain.converter.inventory;

import com.it.savant.ukm.invent.service.domain.model.InventoryItem;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class InventoryItemListSpecItemFiller {

    public List<InventoryItem> fill(final List<InventoryItem> source) {
        final List<InventoryItem> target = new ArrayList<>();
        for (int i = 0; i < source.size(); i++) {
            final String specItem = String.valueOf(i + 1);
            target.add(
                    source.get(i)
                            .setSpecItem(specItem)
                            .setDisplayItem(specItem)
            );
        }
        return target;
    }
}