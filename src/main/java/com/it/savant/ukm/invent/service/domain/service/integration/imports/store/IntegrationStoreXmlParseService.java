package com.it.savant.ukm.invent.service.domain.service.integration.imports.store;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.it.savant.ukm.invent.service.common.exception.UkmInventServiceBusinessException;
import com.it.savant.ukm.invent.service.domain.model.integration.IntegrationStorePackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IntegrationStoreXmlParseService {

    private final XmlMapper xmlMapper;

    @Autowired
    public IntegrationStoreXmlParseService(final XmlMapper xmlMapper) {
        this.xmlMapper = xmlMapper;
    }

    public IntegrationStorePackage parse(final String xml) {
        try {
            return xmlMapper.readValue(xml, IntegrationStorePackage.class);
        } catch (final JsonProcessingException e) {
            e.printStackTrace();
            throw new UkmInventServiceBusinessException(e);
        }
    }
}
