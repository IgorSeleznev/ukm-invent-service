package com.it.savant.ukm.invent.service.da.entity;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ParameterEntity {

    private Long id;
    private String name;
    private String value;
    private String type;
    private String chooses;
    private Short editable;
    private String title;
}
