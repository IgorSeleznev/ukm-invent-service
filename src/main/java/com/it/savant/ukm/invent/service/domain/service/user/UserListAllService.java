package com.it.savant.ukm.invent.service.domain.service.user;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.entity.UserEntity;
import com.it.savant.ukm.invent.service.da.entity.UserListAllClient;
import com.it.savant.ukm.invent.service.domain.model.security.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserListAllService {

    private final UserListAllClient client;
    private final Converter<UserEntity, User> converter;

    @Autowired
    public UserListAllService(final UserListAllClient client,
                              final Converter<UserEntity, User> converter) {
        this.client = client;
        this.converter = converter;
    }

    public List<User> listAll() {
        return client.listAll().stream()
                .map(converter::convert)
                .collect(Collectors.toList());
    }
}
