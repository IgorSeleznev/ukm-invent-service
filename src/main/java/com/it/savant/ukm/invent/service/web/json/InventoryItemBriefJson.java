package com.it.savant.ukm.invent.service.web.json;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class InventoryItemBriefJson {

    private String article;
    private String name;
    private String quantity;
    private String measure;
    private String increaseStep;
}
