package com.it.savant.ukm.invent.service.domain.service.parameter;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.common.exception.NotFoundException;
import com.it.savant.ukm.invent.service.da.client.parameter.ParameterFindByIdClient;
import com.it.savant.ukm.invent.service.da.entity.ParameterEntity;
import com.it.savant.ukm.invent.service.domain.model.parameter.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ParameterFindOneByIdService {

    private final ParameterFindByIdClient client;
    private final Converter<ParameterEntity, Parameter> converter;

    @Autowired
    public ParameterFindOneByIdService(final ParameterFindByIdClient client,
                                       final Converter<ParameterEntity, Parameter> converter) {
        this.client = client;
        this.converter = converter;
    }

    public Parameter findOneById(final Long id) {
        final Optional<ParameterEntity> found = client.findById(id);
        if (found.isEmpty()) {
            throw new NotFoundException("Paarameter does not exists with specified id = [" + id + "]");
        }
        return converter.convert(found.get());
    }
}
