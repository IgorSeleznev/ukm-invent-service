package com.it.savant.ukm.invent.service.web.json;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class InventoryExportRecordJson {

    private String id;
    private String docId;
    private String assortmentName;
    private String confirmDate;
    private Boolean polled;
}
