package com.it.savant.ukm.invent.service.web.controller.admin.parameter;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.parameter.Parameter;
import com.it.savant.ukm.invent.service.domain.service.parameter.ParameterUpdateService;
import com.it.savant.ukm.invent.service.web.json.ParameterJson;
import com.it.savant.ukm.invent.service.web.security.Authorized;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class ParameterUpdateController {

    private final ParameterUpdateService parameterUpdateService;
    private final Converter<ParameterJson, Parameter> converter;

    @Autowired
    public ParameterUpdateController(final ParameterUpdateService parameterUpdateService,
                                     final Converter<ParameterJson, Parameter> converter) {
        this.parameterUpdateService = parameterUpdateService;
        this.converter = converter;
    }

    @Authorized(administratorOnly = true)
    @PostMapping("/admin/parameter/update")
    public void update(@RequestBody final ParameterJson request) {
        log.info("Получен запрос на изменение параметра '{}' на значение '{}'", request.getName(), request.getValue());
        parameterUpdateService.update(
                converter.convert(request)
        );
    }
}
