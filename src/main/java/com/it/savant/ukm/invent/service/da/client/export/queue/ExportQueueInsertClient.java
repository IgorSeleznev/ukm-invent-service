package com.it.savant.ukm.invent.service.da.client.export.queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class ExportQueueInsertClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ExportQueueInsertClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void replace(final String docId) {
        jdbcTemplate.update(
                "REPLACE INTO " + DATABASE_NAME + ".export_queue ( " +
                        "    docId, push_date, poll_date, polled \n" +
                        ") \n" +
                        "VALUES (?, now(), null, 0) ",
                docId
        );
    }
}
