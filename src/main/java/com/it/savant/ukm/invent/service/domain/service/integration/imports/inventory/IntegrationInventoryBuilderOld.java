package com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory;

import com.it.savant.ukm.invent.service.domain.model.Inventory;
import com.it.savant.ukm.invent.service.domain.model.integration.IntegrationInventoryPackage;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.converter.IntegrationInventoryConverter;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.converter.IntegrationInventoryItemConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IntegrationInventoryBuilderOld {

    private final IntegrationInventoryConverter inventoryConverter;
    private final IntegrationInventoryItemConverter itemConverter;

    @Autowired
    public IntegrationInventoryBuilderOld(final IntegrationInventoryConverter inventoryConverter,
                                          final IntegrationInventoryItemConverter itemConverter) {
        this.inventoryConverter = inventoryConverter;
        this.itemConverter = itemConverter;
    }

    public Inventory build(final IntegrationInventoryPackage source) {
        return inventoryConverter.convert(source)
                .setItems(itemConverter.convert(source));
    }
}