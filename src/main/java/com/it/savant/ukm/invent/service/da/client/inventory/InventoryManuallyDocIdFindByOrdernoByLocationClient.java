package com.it.savant.ukm.invent.service.da.client.inventory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryManuallyDocIdFindByOrdernoByLocationClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryManuallyDocIdFindByOrdernoByLocationClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public String docIdByOrdernoByLocation(final String assortmentName, final String location) {
        final List<String> result = jdbcTemplate.query(
                "SELECT id \n" +
                        "FROM " + DATABASE_NAME + ".inventory \n" +
                        "WHERE orderno = ? AND created_manually = 1 AND location = ? AND confirmed = 0 \n" +
                        "ORDER BY record_date DESC \n" +
                        "LIMIT 1",
                (resultSet, i) -> resultSet.getString(1),
                assortmentName,
                location
        );
        if (result.size() == 0 || result.get(0) == null) {
            return null;
        }
        return result.get(0);
    }
}
