package com.it.savant.ukm.invent.service.domain.model.security;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class User {

    private Long id;
    private String name;
    private String administratorLogin;
    private String passwordHash;
    private String location;
    private Boolean administrator;
}
