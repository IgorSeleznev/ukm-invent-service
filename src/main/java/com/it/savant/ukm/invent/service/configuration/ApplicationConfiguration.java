package com.it.savant.ukm.invent.service.configuration;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;

import javax.sql.DataSource;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

@Slf4j
@Configuration
public class ApplicationConfiguration {

    @Bean
    public XmlMapper xmlMapper() {
        return new XmlMapper();
    }

    @Primary
    @Bean
    public JsonMapper jsonMapper() {
        final JsonMapper jsonMapper = new JsonMapper();
        jsonMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        return jsonMapper;
    }

    @Scope(SCOPE_PROTOTYPE)
    @Bean
    public FTPClient ftpClient() {
        return new FTPClient();
    }

    @Bean
    public DataSource dataSource(
            @Value("${spring.datasource.url}") final String url,
            @Value("${spring.datasource.username}") final String username,
            @Value("${spring.datasource.password}") final String password,
            @Value("${spring.datasource.driver-class-name}") final String driverName,
            final ApplicationArguments applicationArguments

    ) {
        final HikariConfig config = new HikariConfig();
        if (applicationArguments.containsOption("url")) {
            log.warn("URL for database connection was specified in arguments: {}", applicationArguments.getOptionValues("url").get(0));
            config.setJdbcUrl(applicationArguments.getOptionValues("url").get(0));
        } else {
            config.setJdbcUrl(url);
        }
        if (applicationArguments.containsOption("username")) {
            log.warn("Username for database connection was specified in arguments: {}", applicationArguments.getOptionValues("username").get(0));
            config.setUsername(applicationArguments.getOptionValues("username").get(0));
        } else {
            config.setUsername(username);
        }
        if (applicationArguments.containsOption("password")) {
            log.warn("Password for database connection was specified in arguments: {}", applicationArguments.getOptionValues("password").get(0));
            config.setPassword(applicationArguments.getOptionValues("password").get(0));
        } else {
            config.setPassword(password);
        }
        config.setPoolName("mysqlConnectionPool");
        config.setConnectionTestQuery("select 1");
        config.setDriverClassName(driverName);
        //config.addDataSourceProperty("cachePrepStmts", "true");
        //config.addDataSourceProperty("prepStmtCacheSize", "250");
        //config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        config.addDataSourceProperty("characterEncoding", "utf8");
        config.addDataSourceProperty("useUnicode", "true");

        return new HikariDataSource(config);
    }
}