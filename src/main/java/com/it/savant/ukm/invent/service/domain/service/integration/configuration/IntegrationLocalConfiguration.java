package com.it.savant.ukm.invent.service.domain.service.integration.configuration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.ToString;
import lombok.Value;

@Value
@AllArgsConstructor
@ToString
@Builder
public class IntegrationLocalConfiguration {

    private final String path;
    private final String localTemporaryPath;
}
