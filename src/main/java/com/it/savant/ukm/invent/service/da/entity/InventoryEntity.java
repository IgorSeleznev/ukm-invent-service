package com.it.savant.ukm.invent.service.da.entity;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Data
@ToString
@Accessors(chain = true)
public class InventoryEntity {

    private String id;
    private String docType;
    private String bornIn;
    private String commentary;
    private String createDat;

    private String currencyMultOrder;
    private String currencyRate;
    private String currencyType;
    private String docState;
    private String isRoubles;

    private String location;
    private String opCode;
    private String priceRoundMode;
    private String totalSum;
    private String totalSumCur;

    private String fillSpecType;
    private String finalDate;
    private String isActiveOnly;
    private String isFillComplete;
    private String orderNo;

    private String ourSelfClient;
    private String preambleDate;
    private String priceMode;
    private String priceType;
    private String storeLoc;

    private String withDue;
    private List<InventoryItemEntity> items = newArrayList();
    private Boolean confirmed;
    private Boolean createdManually;
}
