package com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.map.validator;

import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class IntegrationSmInvListMapValidator {

    public boolean isValid(final Map<String, String> map) {
        return map.containsKey("ID")
                && map.containsKey("DOCTYPE")
                && map.containsKey("FILLSPECTYPE")
                && map.containsKey("FINALDATE")
                && map.containsKey("ISACTIVEONLY")
                && map.containsKey("ISFILLCOMPLETE")
                && map.containsKey("ORDERNO")
                && map.containsKey("OURSELFCLIENT")
                && map.containsKey("PREAMBLEDATE")
                && map.containsKey("PRICEMODE")
                && map.containsKey("PRICETYPE")
                && map.containsKey("STORELOC")
                && map.containsKey("WITHDUE")
                && map.size() == 13;
    }
}
