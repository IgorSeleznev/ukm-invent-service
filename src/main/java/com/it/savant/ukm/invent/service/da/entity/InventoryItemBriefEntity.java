package com.it.savant.ukm.invent.service.da.entity;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class InventoryItemBriefEntity {

    private String article;
    private String name;
    private String quantity;
    private String measure;
    private String increaseStep;
}
