package com.it.savant.ukm.invent.service.da.client.export.record;

import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public interface InventoryExportRecordSqlConst {

    String EXPORT_RECORD_FILTER_SQL = "FROM " + DATABASE_NAME + ".export_queue q \n" +
            "         INNER JOIN " + DATABASE_NAME + ".inventory i \n" +
            "                    ON q.docId = id \n";
}
