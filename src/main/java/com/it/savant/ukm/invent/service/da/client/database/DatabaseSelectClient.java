package com.it.savant.ukm.invent.service.da.client.database;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class DatabaseSelectClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public DatabaseSelectClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void select(final String databaseName) {
        jdbcTemplate.update(
                "USE " + databaseName
        );
    }
}
