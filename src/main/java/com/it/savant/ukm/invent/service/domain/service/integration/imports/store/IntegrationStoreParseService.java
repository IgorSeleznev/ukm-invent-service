package com.it.savant.ukm.invent.service.domain.service.integration.imports.store;

import com.it.savant.ukm.invent.service.domain.model.Store;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IntegrationStoreParseService {

    private final IntegrationStoreBuilder storeBuilder;
    private final IntegrationStoreXmlParseService xmlParseService;

    @Autowired
    public IntegrationStoreParseService(final IntegrationStoreBuilder storeBuilder,
                                        final IntegrationStoreXmlParseService xmlParseService) {
        this.storeBuilder = storeBuilder;
        this.xmlParseService = xmlParseService;
    }

    public Store parse(final String xml) {
        return storeBuilder.build(
                xmlParseService.parse(xml)
        );
    }
}
