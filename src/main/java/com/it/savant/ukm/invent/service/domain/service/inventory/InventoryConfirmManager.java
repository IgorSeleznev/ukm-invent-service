package com.it.savant.ukm.invent.service.domain.service.inventory;

import com.it.savant.ukm.invent.service.domain.model.InventoryConfirmRequest;
import com.it.savant.ukm.invent.service.domain.model.InventoryItemBrief;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

import static java.util.Collections.emptyList;
import static org.jlead.assets.common.utils.NullableUtil.defaultIfNull;

@Service
public class InventoryConfirmManager {

    private final InventoryManuallyDifferenceItemListService differenceService;
    private final InventoryConfirmService checkedConfirmService;

    @Autowired
    public InventoryConfirmManager(final InventoryManuallyDifferenceItemListService differenceService,
                                   final InventoryConfirmService checkedConfirmService) {
        this.differenceService = differenceService;
        this.checkedConfirmService = checkedConfirmService;
    }

    public List<InventoryItemBrief> confirm(final InventoryConfirmRequest request, final String clientId) {
        final List<InventoryItemBrief> differences = differenceService.differenceList(request.getDocId());
        if (differences.size() == 0 || defaultIfNull(request.getForce(), false)) {
            checkedConfirmService.confirm(request.getDocId(), clientId);
            return emptyList();
        }
        return differences;
    }
}
