package com.it.savant.ukm.invent.service.domain.model;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class PageableSortableRequest {

    private Integer pageNumber;
    private Integer pageSize;
    private String sortField;
    private String sortDirection;
}
