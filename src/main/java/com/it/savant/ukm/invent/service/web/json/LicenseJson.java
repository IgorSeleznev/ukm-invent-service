package com.it.savant.ukm.invent.service.web.json;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class LicenseJson {

    private String clientId;
    private String location;
    private String lastActivityDateTime;
}
