package com.it.savant.ukm.invent.service.domain.service.integration.exports;

import com.it.savant.ukm.invent.service.domain.model.InventoryItem;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static org.apache.commons.lang3.StringUtils.defaultIfBlank;

@Component
public class IntegrationInventoryItemXmlBuilder {

    public List<String> xmlBuild(final InventoryItem inventoryItem) {
        final List<String> result = newArrayList();
        final StringBuilder specXml = new StringBuilder();
        specXml.append("      <SMSPEC>\n");
        specXml.append("        <DOCID>" + inventoryItem.getDocId() + "</DOCID>\n");
        specXml.append("        <DOCTYPE>RL</DOCTYPE>\n");
        specXml.append("        <SPECITEM>" + inventoryItem.getSpecItem() + "</SPECITEM>\n");
        specXml.append("        <ARTICLE>" + inventoryItem.getArticle() + "</ARTICLE>\n");
        specXml.append("        <DISPLAYITEM>" + inventoryItem.getDisplayItem() + "</DISPLAYITEM>\n");
        specXml.append("        <ITEMPRICE>0</ITEMPRICE>\n");
        specXml.append("        <ITEMPRICECUR>0</ITEMPRICECUR>\n");
        specXml.append("        <QUANTITY>" + defaultIfBlank(inventoryItem.getQuantity(), "0") + "</QUANTITY>\n");
        specXml.append("        <TOTALPRICE>0</TOTALPRICE>\n");
        specXml.append("        <TOTALPRICECUR>0</TOTALPRICECUR>\n");
        specXml.append("      </SMSPEC>\n");
        result.add(specXml.toString());

        final StringBuilder specIlXml = new StringBuilder();
        specIlXml.append("      <SMSPECIL>\n");
        specIlXml.append("        <DOCID>" + inventoryItem.getDocId() + "</DOCID>\n");
        specIlXml.append("        <DOCTYPE>RL</DOCTYPE>\n");
        specIlXml.append("        <SPECITEM>" + inventoryItem.getSpecItem() + "</SPECITEM>\n");
        specIlXml.append("        <ACTUALQUANTITY>" +  defaultIfBlank(inventoryItem.getActualQuantity(), "0") + "</ACTUALQUANTITY>\n");
        specIlXml.append("        <AWAITQUANTITY>" + defaultIfBlank(inventoryItem.getAwaitQuantity(), "0") + "</AWAITQUANTITY>\n");
        specIlXml.append("        <HISTORY>" + inventoryItem.getHistory() + "</HISTORY>\n");
        specIlXml.append("      </SMSPECIL>\n");
        result.add(specIlXml.toString());

        return result;
    }
}
