package com.it.savant.ukm.invent.service.da.client.license;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class LicenseExistsClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public LicenseExistsClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Boolean exists(final String license) {
        final List<Boolean> result = jdbcTemplate.query(
                "SELECT count(*) \n" +
                        "FROM " + DATABASE_NAME + ".client_license \n" +
                        "WHERE client_id = ?",
                (resultSet, i) -> resultSet.getInt(1) > 0,
                license
        );
        if (result.size() == 0) {
            return false;
        }
        return result.get(0);
    }
}
