package com.it.savant.ukm.invent.service.domain.service.integration.imports.measure;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.client.measure.MeasureIncreaseStepByIdClient;
import com.it.savant.ukm.invent.service.da.client.measure.MeasureReplaceClient;
import com.it.savant.ukm.invent.service.da.entity.MeasureEntity;
import com.it.savant.ukm.invent.service.domain.model.measure.Measure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class IntegrationMeasureSaveService {

    private static final String DEFAULT_INCREASE_STEP = "1";

    private final MeasureIncreaseStepByIdClient increaseStepClient;
    private final MeasureReplaceClient replaceClient;
    private final Converter<Measure, MeasureEntity> converter;

    @Autowired
    public IntegrationMeasureSaveService(final MeasureIncreaseStepByIdClient increaseStepClient,
                                         final MeasureReplaceClient replaceClient,
                                         final Converter<Measure, MeasureEntity> converter) {
        this.increaseStepClient = increaseStepClient;
        this.replaceClient = replaceClient;
        this.converter = converter;
    }

    public void save(final List<Measure> measures) {
        for (final Measure measure : measures) {
            final Optional<String> increaseStep = increaseStepClient.increaseStepFindById(measure.getId());
            replaceClient.replace(
                    converter.convert(measure)
                            .setIncreaseStep(increaseStep.orElse(DEFAULT_INCREASE_STEP))
            );
        }
    }
}
