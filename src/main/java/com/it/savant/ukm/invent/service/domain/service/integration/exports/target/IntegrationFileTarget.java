package com.it.savant.ukm.invent.service.domain.service.integration.exports.target;

public interface IntegrationFileTarget {

    void write(final String docId, final String content);
}
