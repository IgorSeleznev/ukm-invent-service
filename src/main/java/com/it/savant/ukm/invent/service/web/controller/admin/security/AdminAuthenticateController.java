package com.it.savant.ukm.invent.service.web.controller.admin.security;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.security.AdminAuthenticateRequest;
import com.it.savant.ukm.invent.service.domain.model.security.AuthenticateResponse;
import com.it.savant.ukm.invent.service.domain.service.security.AdminAuthenticateService;
import com.it.savant.ukm.invent.service.web.json.AdminAuthenticateRequestJson;
import com.it.savant.ukm.invent.service.web.json.Response;
import com.it.savant.ukm.invent.service.web.json.security.AuthenticateResponseJson;
import com.it.savant.ukm.invent.service.web.security.Licensed;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import static com.it.savant.ukm.invent.service.web.constant.HttpHeaderConstant.HTTP_HEADER_CLIENT_UID;

@Slf4j
@RestController
public class AdminAuthenticateController {

    private final AdminAuthenticateService authenticateService;
    private final Converter<AdminAuthenticateRequestJson, AdminAuthenticateRequest> requestConverter;
    private final Converter<AuthenticateResponse, AuthenticateResponseJson> responseConverter;

    @Autowired
    public AdminAuthenticateController(
            final AdminAuthenticateService authenticateService,
            final Converter<AdminAuthenticateRequestJson, AdminAuthenticateRequest> requestConverter,
            final Converter<AuthenticateResponse, AuthenticateResponseJson> responseConverter
    ) {
        this.authenticateService = authenticateService;
        this.requestConverter = requestConverter;
        this.responseConverter = responseConverter;
    }

    @PostMapping(value = "/admin/authorize")
    public Response<AuthenticateResponseJson> authorize(
            @RequestBody final AdminAuthenticateRequestJson request,
            final HttpServletRequest httpRequest
    ) {
        log.info("Принят запрос на авторизацию для пользователя с логином {}", request.getLogin());
        final AuthenticateResponse response = authenticateService.authorize(
                requestConverter.convert(request),
                httpRequest.getHeader(HTTP_HEADER_CLIENT_UID)
        );
        if (response.isFailed()) {
            log.warn("Авторизация отклонена для пользователя с идентификатором {}", request.getLogin());
            return Response.failed("Неверный пароль или пользователь не имеет доступа");
        }
        return Response.success(
                responseConverter.convert(response)
        );
    }

}
