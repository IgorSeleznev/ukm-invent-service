package com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.converter;

import com.it.savant.ukm.invent.service.domain.model.Inventory;
import com.it.savant.ukm.invent.service.domain.model.integration.IntegrationInventoryPackage;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.map.resolver.IntegrationSmDocumentsMapResolver;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.map.resolver.IntegrationSmInvListMapResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class IntegrationInventoryConverter {

    private final IntegrationSmDocumentsMapResolver smDocumentsMapResolver;
    private final IntegrationSmInvListMapResolver smInvListMapResolver;

    @Autowired
    public IntegrationInventoryConverter(final IntegrationSmDocumentsMapResolver smDocumentsMapResolver,
                                         final IntegrationSmInvListMapResolver smInvListMapResolver
    ) {
        this.smDocumentsMapResolver = smDocumentsMapResolver;
        this.smInvListMapResolver = smInvListMapResolver;
    }

    public Inventory convert(final IntegrationInventoryPackage source) {
        final Map<String, String> smDocuments = smDocumentsMapResolver.resolve(source);
        final Map<String, String> smInvlist = smInvListMapResolver.resolve(source);

        return new Inventory().setId(smDocuments.get("ID"))
                .setDocType(smDocuments.get("DOCTYPE"))
                .setBornIn(smDocuments.get("BORNIN"))
                .setCommentary(smDocuments.get("COMMENTARY"))
                .setCreateDat(smDocuments.get("CREATEDAT"))
                .setCurrencyMultOrder(smDocuments.get("CURRENCYMULTORDER"))
                .setCurrencyRate(smDocuments.get("CURRENCYRATE"))
                .setCurrencyType(smDocuments.get("CURRENCYTYPE"))
                .setDocState(smDocuments.get("DOCSTATE"))
                .setIsRoubles(smDocuments.get("ISROUBLES"))
                .setLocation(smDocuments.get("LOCATION"))
                .setOpCode(smDocuments.get("OPCODE"))
                .setPriceRoundMode(smDocuments.get("PRICEROUNDMODE"))
                .setTotalSum(smDocuments.get("TOTALSUM"))
                .setTotalSumCur(smDocuments.get("TOTALSUMCUR"))

                .setFillSpecType(smInvlist.get("FILLSPECTYPE"))
                .setFinalDate(smInvlist.get("FINALDATE"))
                .setIsActiveOnly(smInvlist.get("ISACTIVEONLY"))
                .setIsFillComplete(smInvlist.get("ISFILLCOMPLETE"))
                .setOrderNo(smInvlist.get("ORDERNO"))
                .setOurSelfClient(smInvlist.get("OURSELFCLIENT"))
                .setPreambleDate(smInvlist.get("PREAMBLEDATE"))
                .setPriceMode(smInvlist.get("PRICEMODE"))
                .setPriceType(smInvlist.get("PRICETYPE"))
                .setStoreLoc(smInvlist.get("STORELOC"))
                .setWithDue(smInvlist.get("WITHDUE"))
                .setCreatedManually(false);
    }
}
