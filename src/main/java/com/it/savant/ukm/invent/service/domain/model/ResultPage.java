package com.it.savant.ukm.invent.service.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@ToString
@AllArgsConstructor
@Accessors(chain = true)
public class ResultPage<T> {

    public static <T> ResultPage<T> resultOf(final List<T> list, final Integer totalCount) {
        return new ResultPage<>(list, totalCount);
    }

    private List<T> result;
    private Integer totalCount;
}
