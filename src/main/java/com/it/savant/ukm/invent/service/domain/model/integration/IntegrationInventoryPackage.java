package com.it.savant.ukm.invent.service.domain.model.integration;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
@JacksonXmlRootElement(localName = "PACKAGE")
public class IntegrationInventoryPackage {

    @JacksonXmlProperty(localName = "name", isAttribute = true)
    private String name;
    @JacksonXmlProperty(localName = "POSTOBJECT")
    private IntegrationInventoryPostObject postObject;
}
