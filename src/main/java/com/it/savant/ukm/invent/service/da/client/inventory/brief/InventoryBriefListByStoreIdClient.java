package com.it.savant.ukm.invent.service.da.client.inventory.brief;

import com.it.savant.ukm.invent.service.da.entity.InventoryBriefEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Slf4j
@Component
public class InventoryBriefListByStoreIdClient {

    private static final String SQL_SELECT_ASSORTMENT_LIST = "SELECT id, orderno \n" +
            "FROM " + DATABASE_NAME + ".inventory \n" +
            "WHERE confirmed = 0 and location = ? and created_manually = 1";


    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryBriefListByStoreIdClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<InventoryBriefEntity> listByStoreId(final String storeId) {
        log.warn("We will select assortment list now: {}", SQL_SELECT_ASSORTMENT_LIST);
        final List<InventoryBriefEntity> result = jdbcTemplate.query(
                SQL_SELECT_ASSORTMENT_LIST,
                (resultSet, i) -> new InventoryBriefEntity()
                        .setDocId(resultSet.getString(1))
                        .setAssortmentName(resultSet.getString(2)),
                storeId
        );
        log.warn("And now we have got assortment list: {}", result);

        return result;
    }
}
