package com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory;

import com.it.savant.ukm.invent.service.domain.service.integration.imports.ImportService;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.parser.XmlNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
public class IntegrationInventoryImportService implements ImportService {

    private final IntegrationDeserializeService deserializeService;
    private final IntegrationInventoryImportFlowService importFlowService;

    @Autowired
    public IntegrationInventoryImportService(
            final IntegrationDeserializeService deserializeService,
            final IntegrationInventoryImportFlowService importFlowService
    ) {
        this.deserializeService = deserializeService;
        this.importFlowService = importFlowService;
    }

    @Override
    public boolean isValid(final XmlNode xml) {
        final List<XmlNode> documentNodes = xml.getNodes().get(0)
                .getNodes().get(0)
                .getNodes();
        for (final XmlNode child : documentNodes) {
            if ("SMDOCUMENTS".equals(child.getName().toUpperCase())) {
                log.info("Импорт данных: обнаружен файл акта инвентаризации");
                return true;
            }
        }
        return false;
    }

    @Transactional
    @Override
    public void doImport(final XmlNode xml) {
        log.info("Импорт данных: импорт акта инвентаризации");
        importFlowService.importFlow(
                deserializeService.deserialize(xml, "INVENTORY")
        );
    }
}
