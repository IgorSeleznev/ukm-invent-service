package com.it.savant.ukm.invent.service.domain.service.license;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.client.license.LicenseListClient;
import com.it.savant.ukm.invent.service.da.entity.LicenseEntity;
import com.it.savant.ukm.invent.service.domain.model.PageableSortableRequest;
import com.it.savant.ukm.invent.service.domain.model.license.License;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class LicenseListService {

    private final LicenseListClient client;
    private final Converter<LicenseEntity, License> converter;

    @Autowired
    public LicenseListService(final LicenseListClient client,
                              final Converter<LicenseEntity, License> converter) {
        this.client = client;
        this.converter = converter;
    }

    public List<License> list(final PageableSortableRequest request) {
        return client.list(
                request.getPageNumber(),
                request.getPageSize(),
                request.getSortField(),
                request.getSortDirection()
        ).stream()
                .map(converter::convert)
                .collect(toList());
    }
}
