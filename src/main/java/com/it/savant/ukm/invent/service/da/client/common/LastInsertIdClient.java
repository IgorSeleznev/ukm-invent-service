package com.it.savant.ukm.invent.service.da.client.common;

import com.it.savant.ukm.invent.service.common.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class LastInsertIdClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public LastInsertIdClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Long lastInsertId() {
        final List<Long> results = jdbcTemplate.query(
                "SELECT LAST_INSERT_ID()",
                (resultSet, i) -> resultSet.getLong(1)
        );
        if (results.size() == 0) {
            throw new NotFoundException("Result of SELECT LAST_INSERT_ID is empty");
        }
        ;
        return results.get(0);
    }
}
