package com.it.savant.ukm.invent.service.web.json;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@ToString
@Accessors(chain = true)
public class InventoryBriefDetailedJson {

    private String docId;
    private String assortmentName;
    private List<InventoryItemBriefJson> items;
}
