package com.it.savant.ukm.invent.service.web.configuration;

import com.it.savant.ukm.invent.service.web.interceptor.AuthorizationHandlerInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InventServiceWebConfiguration implements WebMvcConfigurer {

    private final AuthorizationHandlerInterceptor authorizationHandlerInterceptor;

    @Autowired
    public InventServiceWebConfiguration(final AuthorizationHandlerInterceptor authorizationHandlerInterceptor) {
        this.authorizationHandlerInterceptor = authorizationHandlerInterceptor;
    }

    @Override
    public void addInterceptors(final InterceptorRegistry registry) {
        registry.addInterceptor(authorizationHandlerInterceptor).addPathPatterns("/**");
    }

    //@Bean
    //public CommonsRequestLoggingFilter logFilter() {
    //    final CommonsRequestLoggingFilter filter = new CommonsRequestLoggingFilter();
    //    filter.setIncludeQueryString(true);
    //    filter.setIncludePayload(true);
    //    filter.setMaxPayloadLength(10000);
    //    filter.setIncludeHeaders(false);
    //    filter.setAfterMessagePrefix("REQUEST DATA : ");
    //    return filter;
    //}
}
