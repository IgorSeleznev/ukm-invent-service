package com.it.savant.ukm.invent.service.domain.service.integration.imports;

import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.parser.XmlNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.of;

@Slf4j
@Component
public class ImportServiceResolver {

    private final List<ImportService> services;

    @Autowired
    public ImportServiceResolver(final List<ImportService> services) {
        this.services = services;
    }

    public Optional<ImportService> resolve(final XmlNode xmlNode) {
        for (final ImportService importService : services) {
            try {
                if (importService.isValid(xmlNode)) {
                    return of(importService);
                }
            } catch (final Exception exception) {
                log.warn("Импорт данных: внимание, обнаружен xml-файл, который, возможно, не может быть обработан либо просто не соответствует структуре одного из поддерживаемых файлов (второй вариант - допустим)");
            }
        }
        return empty();
    }
}
