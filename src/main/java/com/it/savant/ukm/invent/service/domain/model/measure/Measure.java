package com.it.savant.ukm.invent.service.domain.model.measure;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class Measure {

    private String id;
    private String abbrev;
    private String baseType;
    private String code;
    private String codeIso;
    private String mesType;
    private String name;
    private String prec;
    private String unitName;
    private String increaseStep;
}
