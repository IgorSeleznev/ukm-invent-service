package com.it.savant.ukm.invent.service.da.client.inventory.item.brief;

import com.it.savant.ukm.invent.service.da.entity.InventoryItemBriefEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryItemBriefListByDocIdClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryItemBriefListByDocIdClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<InventoryItemBriefEntity> findAllByClassifier(final String docId) {
        return jdbcTemplate.query(
                "SELECT i.article, a.name, i.quantity, a.mesabbrev, m.increase_step \n" +
                        "FROM " + DATABASE_NAME + ".inventory_item i, " + DATABASE_NAME + ".article a, \n" +
                        "     " + DATABASE_NAME + ".measure m \n" +
                        "WHERE i.docid = ? AND i.actual = 1 \n" +
                        "  AND m.id = a.idmeasurement \n" +
                        "  AND i.article = a.article",
                (resultSet, i) -> new InventoryItemBriefEntity()
                        .setArticle(resultSet.getString(1))
                        .setName(resultSet.getString(2))
                        .setQuantity(resultSet.getString(3))
                        .setMeasure(resultSet.getString(4))
                        .setIncreaseStep(resultSet.getString(5)),
                docId
        );
    }
}
