package com.it.savant.ukm.invent.service.da.client.inventory.item;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryManualItemUpdateByParentDocIdClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryManualItemUpdateByParentDocIdClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void update(final String parentDocId, final String docId) {
        jdbcTemplate.update(
                "update " + DATABASE_NAME + ".inventory_item i inner join " + DATABASE_NAME + ".inventory_item p \n" +
                        "set " +
                        "    i.docType = p.docType, \n" +
                        "    i.specItem = p.specItem, \n" +
                        "    i.displayItem = p.displayItem, \n" +
                        " \n" +
                        "    i.itemPrice = p.itemPrice, \n" +
                        "    i.itemPriceCur = p.itemPriceCur, \n" +
                        "    i.quantity = p.quantity, \n" +
                        "    i.totalPrice = p.totalPrice, \n" +
                        "    i.totalPriceCur = p.totalPriceCur, \n" +
                        " \n" +
                        "    i.actualQuantity = p.actualQuantity, \n" +
                        "    i.awaitQuantity = p.awaitQuantity, \n" +
                        "    i.history = p.history, \n" +
                        "    i.actual = 1, \n" +
                        "    i.source_docId = ? \n" +
                        "where i.article = p.article \n" +
                        "  and i.docId = ? and p.docId = ? \n" +
                        "  and p.actual = 1",
                parentDocId,
                docId,
                parentDocId
        );
    }
}