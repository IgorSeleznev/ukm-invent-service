package com.it.savant.ukm.invent.service.domain.service.transaction;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

@Slf4j
@Service
public class TransactionalExecutor {

    private final TransactionManageService transactionManageService;

    @Autowired
    public TransactionalExecutor(final TransactionManageService transactionManageService) {
        this.transactionManageService = transactionManageService;
    }

    public <T, R> R execute(final Function<T, R> function, final T arg) {
        transactionManageService.start();
        try {
            final R result = function.apply(arg);
            transactionManageService.commit();
            return result;
        } catch (final Throwable throwable) {
            log.error("Error has occurred in transaction", throwable);
            transactionManageService.rollback();
            throw throwable;
        }
    }

    public <T> void execute(final Consumer<T> consumer, final T arg) {
        transactionManageService.start();
        try {
            consumer.accept(arg);
            transactionManageService.commit();
        } catch (final Throwable throwable) {
            log.error("Error has occurred in transaction", throwable);
            transactionManageService.rollback();
            throw throwable;
        }
    }

    public <T> T execute(final Supplier<T> supplier) {
        transactionManageService.start();
        try {
            final T result = supplier.get();
            transactionManageService.commit();
            return result;
        } catch (final Throwable throwable) {
            log.error("Error has occurred in transaction", throwable);
            transactionManageService.rollback();
            throw throwable;
        }
    }

}
