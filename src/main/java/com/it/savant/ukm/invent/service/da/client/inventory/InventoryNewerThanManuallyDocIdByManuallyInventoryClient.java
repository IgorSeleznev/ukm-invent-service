package com.it.savant.ukm.invent.service.da.client.inventory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;
import static java.util.Optional.empty;
import static java.util.Optional.of;

@Component
public class InventoryNewerThanManuallyDocIdByManuallyInventoryClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryNewerThanManuallyDocIdByManuallyInventoryClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Optional<String> differenceDocId(final String manuallyInventoryDocId) {
        final List<String> results = jdbcTemplate.query(
                "SELECT ai.id \n" +
                        "FROM " + DATABASE_NAME + ".inventory mi, " + DATABASE_NAME + ".inventory ai \n" +
                        "WHERE mi.orderno = ai.orderno \n" +
                        "  AND mi.docId = ? and ai.created_manually = 0 \n" +
                        "  AND ai.record_date > mi.record_date \n" +
                        "  AND ai.location = mi.location \n" +
                        "ORDER BY ai.record_date DESC \n" +
                        "LIMIT 1",
                (resultSet, i) -> resultSet.getString(1),
                manuallyInventoryDocId
        );
        if (results.size() == 0) {
            return empty();
        }
        return of(results.get(0));
    }
}
