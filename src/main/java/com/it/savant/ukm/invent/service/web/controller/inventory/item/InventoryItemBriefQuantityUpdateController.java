package com.it.savant.ukm.invent.service.web.controller.inventory.item;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.InventoryItemBriefQuantityUpdateRequest;
import com.it.savant.ukm.invent.service.domain.service.inventory.item.brief.InventoryItemBriefQuantityUpdateService;
import com.it.savant.ukm.invent.service.web.json.InventoryItemBriefQuantityUpdateRequestJson;
import com.it.savant.ukm.invent.service.web.json.Response;
import com.it.savant.ukm.invent.service.web.security.Authorized;
import com.it.savant.ukm.invent.service.web.security.Licensed;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static com.it.savant.ukm.invent.service.web.json.Response.success;

@Slf4j
@RestController
public class InventoryItemBriefQuantityUpdateController {

    private final InventoryItemBriefQuantityUpdateService service;
    private final Converter<InventoryItemBriefQuantityUpdateRequestJson, InventoryItemBriefQuantityUpdateRequest> converter;

    @Autowired
    public InventoryItemBriefQuantityUpdateController(final InventoryItemBriefQuantityUpdateService service,
                                                      final Converter<InventoryItemBriefQuantityUpdateRequestJson, InventoryItemBriefQuantityUpdateRequest> converter) {
        this.service = service;
        this.converter = converter;
    }

    @Licensed
    @Authorized
    @PostMapping(value = "/inventory/item/quantity/update")
    public Response<Void> updateQuantity(@RequestBody InventoryItemBriefQuantityUpdateRequestJson request) {
        log.info(
                "Принят запрос на изменение количества товара с артикулом '{}' на {} для акта инвентаризации {}",
                request.getArticle(),
                request.getQuantity(),
                request.getDocId()
        );
        service.updateQuantity(converter.convert(request));
        return success();
    }
}
