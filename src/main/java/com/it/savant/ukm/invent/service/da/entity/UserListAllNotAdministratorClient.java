package com.it.savant.ukm.invent.service.da.entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class UserListAllNotAdministratorClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public UserListAllNotAdministratorClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<UserListItemEntity> listAll() {
        return jdbcTemplate.query(
                "SELECT id, name \n" +
                        "FROM " + DATABASE_NAME + ".user \n" +
                        "WHERE deleted = 0 AND administrator = 0",
                (resultSet, i) -> new UserListItemEntity()
                        .setId(resultSet.getLong(1))
                        .setName(resultSet.getString(2))
        );
    }
}
