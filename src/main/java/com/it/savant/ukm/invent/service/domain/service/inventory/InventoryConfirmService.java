package com.it.savant.ukm.invent.service.domain.service.inventory;

public interface InventoryConfirmService {

    void confirm(final String docId, final String clientId);
}
