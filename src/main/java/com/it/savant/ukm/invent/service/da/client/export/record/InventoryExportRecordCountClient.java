package com.it.savant.ukm.invent.service.da.client.export.record;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.it.savant.ukm.invent.service.da.client.export.record.InventoryExportRecordSqlConst.EXPORT_RECORD_FILTER_SQL;

@Component
public class InventoryExportRecordCountClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryExportRecordCountClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int count() {
        final List<Integer> result = jdbcTemplate.query(
                "SELECT count(*) \n" + EXPORT_RECORD_FILTER_SQL,
                (resultSet, i) -> resultSet.getInt(1)
        );
        if (result.size() == 0) {
            return 0;
        }
        return result.get(0);
    }
}
