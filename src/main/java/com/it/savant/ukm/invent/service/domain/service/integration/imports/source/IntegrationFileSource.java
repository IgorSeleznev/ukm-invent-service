package com.it.savant.ukm.invent.service.domain.service.integration.imports.source;

public interface IntegrationFileSource {

    IntegrationFileSourceDeleteService deleteTemporaryFileService();

    IntegrationFileSourceDeleteService deleteOriginalFileService();

    String getFilename();

    String read();
}
