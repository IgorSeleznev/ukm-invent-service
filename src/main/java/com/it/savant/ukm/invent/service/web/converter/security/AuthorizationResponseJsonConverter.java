package com.it.savant.ukm.invent.service.web.converter.security;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.security.AuthenticateResponse;
import com.it.savant.ukm.invent.service.web.json.security.AuthenticateResponseJson;
import org.springframework.stereotype.Component;

@Component
public class AuthorizationResponseJsonConverter implements Converter<AuthenticateResponse, AuthenticateResponseJson> {

    @Override
    public AuthenticateResponseJson convert(final AuthenticateResponse source) {
        return new AuthenticateResponseJson().setToken(source.getToken());
    }
}
