package com.it.savant.ukm.invent.service.domain.model.smstorelocations;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class SmStoreLocations {

    private String id;
    private String name;
}
