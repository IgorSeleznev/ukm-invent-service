package com.it.savant.ukm.invent.service.domain.service.inventory;

import com.it.savant.ukm.invent.service.da.client.inventory.InventoryOrdernoByDocIdClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InventoryOrdernoService {

    private final InventoryOrdernoByDocIdClient client;

    @Autowired
    public InventoryOrdernoService(final InventoryOrdernoByDocIdClient client) {
        this.client = client;
    }

    public String orderno(final String docId) {
        return client.ordernoByDocId(docId);
    }
}
