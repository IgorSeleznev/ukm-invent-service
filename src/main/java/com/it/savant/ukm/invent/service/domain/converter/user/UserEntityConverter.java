package com.it.savant.ukm.invent.service.domain.converter.user;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.entity.UserEntity;
import com.it.savant.ukm.invent.service.domain.model.security.User;
import org.springframework.stereotype.Component;

import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;

@Component
public class UserEntityConverter implements Converter<User, UserEntity> {

    @Override
    public UserEntity convert(final User source) {
        return new UserEntity()
                .setId(source.getId())
                .setLocation(source.getLocation())
                .setName(source.getName())
                .setPasswordHash(source.getPasswordHash())
                .setAdministratorLogin(source.getAdministratorLogin())
                .setAdministrator(defaultIfNull(source.getAdministrator(), false) ? 1 : 0);
    }
}
