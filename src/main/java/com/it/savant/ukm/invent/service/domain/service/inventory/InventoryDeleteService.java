package com.it.savant.ukm.invent.service.domain.service.inventory;

import com.it.savant.ukm.invent.service.da.client.inventory.InventoryDeleteClient;
import com.it.savant.ukm.invent.service.da.client.inventory.InventoryItemListDeleteClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InventoryDeleteService {

    private final InventoryDeleteClient inventoryDeleteClient;
    private final InventoryItemListDeleteClient itemListDeleteClient;

    @Autowired
    public InventoryDeleteService(final InventoryDeleteClient inventoryDeleteClient,
                                  final InventoryItemListDeleteClient itemListDeleteClient) {
        this.inventoryDeleteClient = inventoryDeleteClient;
        this.itemListDeleteClient = itemListDeleteClient;
    }

    public void delete(final String docId) {
        inventoryDeleteClient.delete(docId);
        itemListDeleteClient.delete(docId);
    }
}
