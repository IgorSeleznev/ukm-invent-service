package com.it.savant.ukm.invent.service.domain.service.inventory;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.client.inventory.InventoryLastDocIdByOrdernoClient;
import com.it.savant.ukm.invent.service.da.client.inventory.brief.InventoryBriefDetailedFindByDocIdClient;
import com.it.savant.ukm.invent.service.da.entity.InventoryBriefDetailedEntity;
import com.it.savant.ukm.invent.service.domain.model.InventoryBriefDetailed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InventoryLastByOrdernoByLocationService {

    private final InventoryLastDocIdByOrdernoClient lastDocIdClient;
    private final InventoryBriefDetailedFindByDocIdClient findClient;
    private final Converter<InventoryBriefDetailedEntity, InventoryBriefDetailed> converter;

    @Autowired
    public InventoryLastByOrdernoByLocationService(
            final InventoryLastDocIdByOrdernoClient lastDocIdClient,
            final InventoryBriefDetailedFindByDocIdClient findClient,
            final Converter<InventoryBriefDetailedEntity, InventoryBriefDetailed> converter
    ) {
        this.lastDocIdClient = lastDocIdClient;
        this.findClient = findClient;
        this.converter = converter;
    }

    public InventoryBriefDetailed inventory(final String assortmentName, final String location) {
        return converter.convert(
                findClient.findOneByDocId(
                        lastDocIdClient.lastDocId(assortmentName, location)
                )
        );
    }
}
