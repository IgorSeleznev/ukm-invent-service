package com.it.savant.ukm.invent.service.domain.service.security;

import com.it.savant.ukm.invent.service.domain.model.security.User;
import com.it.savant.ukm.invent.service.domain.service.parameter.ParameterProvider;
import lombok.extern.slf4j.Slf4j;
import org.jlead.assets.security.v2.Authentication;
import org.jlead.assets.security.v2.AuthenticationHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import static com.it.savant.ukm.invent.service.domain.constants.ParameterConstants.SESSION_INACTIVE_MINUTES_EXPIRED_INTERVAL;

@Slf4j
@Service
public class UnAuthenticateExpiredService {

    private final MobileClientSessionTokenHolder tokenHolder;
    private final AuthenticationHolder<User, Boolean> authenticationHolder;
    private final ParameterProvider parameterProvider;

    @Autowired
    public UnAuthenticateExpiredService(
            final MobileClientSessionTokenHolder tokenHolder,
            final AuthenticationHolder<User, Boolean> authenticationHolder,
            final ParameterProvider parameterProvider
    ) {
        this.tokenHolder = tokenHolder;
        this.authenticationHolder = authenticationHolder;
        this.parameterProvider = parameterProvider;
    }

    public void unAuthenticateExpired() {
        final List<Authentication<User, Boolean>> expired = authenticationHolder.expire(
                new Date(),
                Long.parseLong(
                        parameterProvider.read(SESSION_INACTIVE_MINUTES_EXPIRED_INTERVAL)
                ) * 60000
        );
        expired.forEach(
                expiredSession -> tokenHolder.removeByToken(expiredSession.getToken())
        );
    }
}