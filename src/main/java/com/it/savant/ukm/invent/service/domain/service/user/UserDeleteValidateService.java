package com.it.savant.ukm.invent.service.domain.service.user;

import com.it.savant.ukm.invent.service.common.exception.UkmInventServiceBusinessException;
import com.it.savant.ukm.invent.service.da.client.security.UserAdministratorCountClient;
import com.it.savant.ukm.invent.service.domain.model.security.User;
import org.jlead.assets.security.v2.AuthenticationHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserDeleteValidateService {

    private final UserAdministratorCountClient administratorCountClient;
    private final AuthenticationHolder<User, Boolean> authenticationHolder;

    @Autowired
    public UserDeleteValidateService(final UserAdministratorCountClient administratorCountClient,
                                     final AuthenticationHolder<User, Boolean> authenticationHolder) {
        this.administratorCountClient = administratorCountClient;
        this.authenticationHolder = authenticationHolder;
    }

    public void deleteValidate(final Long userId) {
        if (administratorCountClient.administratorsCount() < 2) {
            throw new UkmInventServiceBusinessException("Должен оставаться хотя бы один пользователь с доступом администратора");
        }
        if (authenticationHolder.current().getMember().getIdentification().getId().equals(userId)) {
            throw new UkmInventServiceBusinessException("Нельзя удалить собственную учетную запись");
        }
    }
}
