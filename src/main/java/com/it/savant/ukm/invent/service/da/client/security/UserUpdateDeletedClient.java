package com.it.savant.ukm.invent.service.da.client.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class UserUpdateDeletedClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public UserUpdateDeletedClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void updateDeleted(final Long id) {
        jdbcTemplate.update(
                "UPDATE " + DATABASE_NAME + ".user \n" +
                        "SET deleted = 1 \n" +
                        "WHERE id = ?",
                id
        );
    }
}
