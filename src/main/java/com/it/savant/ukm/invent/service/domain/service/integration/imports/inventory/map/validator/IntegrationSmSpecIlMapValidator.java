package com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.map.validator;

import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class IntegrationSmSpecIlMapValidator {

    public boolean isValid(final Map<String, String> map) {
        return map.containsKey("DOCID")
                && map.containsKey("DOCTYPE")
                && map.containsKey("SPECITEM")
                && map.containsKey("ACTUALQUANTITY")
                && map.containsKey("AWAITQUANTITY")
                && map.containsKey("HISTORY")
                && map.size() == 6;
    }
}
