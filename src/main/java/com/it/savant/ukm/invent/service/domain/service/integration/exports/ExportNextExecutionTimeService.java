package com.it.savant.ukm.invent.service.domain.service.integration.exports;

import com.it.savant.ukm.invent.service.common.exception.UkmInventServiceSystemException;
import com.it.savant.ukm.invent.service.domain.service.scheduled.SchedulingModeType;
import com.it.savant.ukm.invent.service.domain.service.parameter.ParameterProvider;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.it.savant.ukm.invent.service.domain.constants.ParameterConstants.*;
import static com.it.savant.ukm.invent.service.domain.service.scheduled.SchedulingModeType.CRON;
import static com.it.savant.ukm.invent.service.domain.service.scheduled.SchedulingModeType.INTERVAL;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;

@Slf4j
@Service
public class ExportNextExecutionTimeService {

    private final ParameterProvider parameterProvider;

    @Autowired
    public ExportNextExecutionTimeService(final ParameterProvider parameterProvider) {
        this.parameterProvider = parameterProvider;
    }

    public Date nextExecutionTime(final TriggerContext triggerContext) {
        val failedTryTimeout = Long.parseLong(
                defaultIfNull(
                        parameterProvider.safetyRead(EXPORT_FAILED_TRY_TIMEOUT, EXPORT_FAILED_TRY_TIMEOUT_DEFAULT_VALUE),
                        EXPORT_FAILED_TRY_TIMEOUT_DEFAULT_VALUE
                )
        );

        try {
            val schedulingMode = SchedulingModeType.valueOf(
                    parameterProvider.read(EXPORT_SCHEDULE_MODE)
            );

            if (CRON.equals(schedulingMode)) {
                return new CronTrigger(
                        parameterProvider.read(EXPORT_SCHEDULE_CRON)
                ).nextExecutionTime(triggerContext);
            }

            if (INTERVAL.equals(schedulingMode)) {
                val lastScheduled = defaultIfNull(triggerContext.lastScheduledExecutionTime(), new Date());
                val interval = Long.parseLong(
                        parameterProvider.read(EXPORT_SCHEDULE_INTERVAL)
                );

                return new Date(lastScheduled.getTime() + interval);
            }
            throw new UkmInventServiceSystemException("Illegal scheduling mode type specified for EXPORT with value [" + schedulingMode + "]");
        } catch (final Exception exception) {
            final Date nextDate = new Date(new Date().getTime() + failedTryTimeout);
            log.error("Has got error when try to schedule next import.", exception);
            log.error("Next import will run at {}", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(nextDate));

            return nextDate;
        }
    }
}
