package com.it.savant.ukm.invent.service.common.exception;

public class WrongPasswordException extends RuntimeException {

    public WrongPasswordException(final Long userId, final String password) {
        super("User not found for identifier [" + userId + "] and password [" + password + "]");
    }
}