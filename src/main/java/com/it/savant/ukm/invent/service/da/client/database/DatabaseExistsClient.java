package com.it.savant.ukm.invent.service.da.client.database;

import com.it.savant.ukm.invent.service.common.exception.UkmInventServiceBusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DatabaseExistsClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public DatabaseExistsClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public boolean exists(final String databaseName) {
        final List<Integer> result = jdbcTemplate.query(
                "SELECT count(SCHEMA_NAME)\n" +
                        "  FROM INFORMATION_SCHEMA.SCHEMATA\n" +
                        " WHERE SCHEMA_NAME = ?",
                (resultSet, i) -> resultSet.getInt(1),
                databaseName
        );

        if (result.size() == 0) {
            throw new UkmInventServiceBusinessException("Could not to check database exists because result of sql-query was empty");
        }
        return result.get(0) > 0;
    }
}
