package com.it.savant.ukm.invent.service.domain.service.integration.imports.measure;

import com.it.savant.ukm.invent.service.domain.service.integration.imports.ImportService;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.IntegrationDeserializeService;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.parser.XmlNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class IntegrationMeasureImportService implements ImportService {

    private final IntegrationDeserializeService deserializeService;
    private final IntegrationMeasureSaveService saveService;

    @Autowired
    public IntegrationMeasureImportService(
            final IntegrationDeserializeService deserializeService,
            final IntegrationMeasureSaveService saveService
    ) {
        this.deserializeService = deserializeService;
        this.saveService = saveService;
    }

    @Override
    public boolean isValid(final XmlNode xml) {
        final List<XmlNode> nodes = xml.getNodes().get(0)
                .getNodes().get(0)
                .getNodes();
        for (final XmlNode node : nodes) {
            if ("SAMEASUREMENT".equals(node.getName())) {
                log.info("Импорт данных: обнаружен файл описания единицы измерения");
                return true;
            }
        }

        return false;
    }

    @Override
    public void doImport(final XmlNode xml) {
        log.info("Импорт данных: импорт единицы измерения");
        saveService.save(
                deserializeService.deserialize(xml, "MEASURE")
        );
    }
}
