package com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory;

import com.it.savant.ukm.invent.service.domain.model.Inventory;
import com.it.savant.ukm.invent.service.domain.service.inventory.InventoryDeleteService;
import com.it.savant.ukm.invent.service.domain.service.inventory.InventoryManuallyMakeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class IntegrationInventoryImportFlowService {

    private final IntegrationInventorySaveService saveService;
    private final InventoryDeleteService deleteService;
    private final InventoryManuallyMakeService makeService;

    @Autowired
    public IntegrationInventoryImportFlowService(
            final IntegrationInventorySaveService saveService,
            final InventoryDeleteService deleteService,
            final InventoryManuallyMakeService makeService
    ) {
        this.saveService = saveService;
        this.deleteService = deleteService;
        this.makeService = makeService;
    }

    public void importFlow(final Inventory inventory) {
        deleteService.delete(inventory.getId());
        saveService.save(inventory);
        makeService.make(inventory);
    }
}
