package com.it.savant.ukm.invent.service.web.converter.parameter;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.parameter.Parameter;
import com.it.savant.ukm.invent.service.web.json.ParameterJson;
import org.springframework.stereotype.Component;

@Component
public class ParameterDomainConverter implements Converter<ParameterJson, Parameter> {

    @Override
    public Parameter convert(final ParameterJson source) {
        return new Parameter()
                .setName(source.getName())
                .setValue(source.getValue());
    }
}
