package com.it.savant.ukm.invent.service.domain.service.integration.exports.target.ftp;

import com.it.savant.ukm.invent.service.domain.service.integration.IntegrationType;
import com.it.savant.ukm.invent.service.domain.service.integration.configuration.IntegrationFTPConfiguration;
import com.it.savant.ukm.invent.service.domain.service.integration.exports.target.IntegrationFileTarget;
import com.it.savant.ukm.invent.service.domain.service.integration.exports.target.IntegrationFileTargetBuilder;
import com.it.savant.ukm.invent.service.domain.service.parameter.ParameterProvider;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.domain.constants.ParameterConstants.*;
import static com.it.savant.ukm.invent.service.domain.service.integration.IntegrationType.FTP;

@Component
public class IntegrationFTPFileTargetBuilder implements IntegrationFileTargetBuilder {

    private final FTPClient ftpClient;
    private final ParameterProvider parameterProvider;

    @Autowired
    public IntegrationFTPFileTargetBuilder(final FTPClient ftpClient,
                                           final ParameterProvider parameterProvider) {
        this.parameterProvider = parameterProvider;
        this.ftpClient = ftpClient;
    }

    @Override
    public IntegrationFileTarget build() {
        return new IntegrationFTPFileTarget(
                ftpClient,
                new IntegrationFTPConfiguration(
                        parameterProvider.read(INTEGRATION_SOURCE_FTP_HOST),
                        Integer.parseInt(parameterProvider.read(INTEGRATION_SOURCE_FTP_PORT)),
                        parameterProvider.read(INTEGRATION_SOURCE_FTP_USERNAME),
                        parameterProvider.read(INTEGRATION_SOURCE_FTP_PASSWORD),
                        parameterProvider.read(INTEGRATION_TARGET_FTP_PATH),
                        parameterProvider.read(INTEGRATION_TARGET_FTP_TEMPORARY_PATH)
                )
        );
    }

    @Override
    public IntegrationType getIntegrationType() {
        return FTP;
    }
}
