package com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.converter;

import com.it.savant.ukm.invent.service.domain.model.InventoryItem;
import com.it.savant.ukm.invent.service.domain.model.integration.IntegrationInventoryPackage;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.map.resolver.IntegrationSmSpecMapResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class IntegrationInventoryItemConverter {

    private final IntegrationSmSpecMapResolver smSpecListResolver;

    @Autowired
    public IntegrationInventoryItemConverter(final IntegrationSmSpecMapResolver smSpecListResolver) {
        this.smSpecListResolver = smSpecListResolver;
    }

    public List<InventoryItem> convert(final IntegrationInventoryPackage source) {
        return smSpecListResolver.resolve(source)
                .stream()
                .map(
                        map -> new InventoryItem()
                                .setDocId(map.get("DOCID"))
                                .setDocType(map.get("DOCTYPE"))
                                .setSpecItem(map.get("SPECITEM"))
                                .setArticle(map.get("ARTICLE"))
                                .setDisplayItem(map.get("DISPLAYITEM"))
                                .setItemPrice(map.get("ITEMPRICE"))
                                .setItemPriceCur(map.get("ITEMPRICECUR"))
                                .setQuantity(map.get("QUANTITY"))
                                .setTotalPrice(map.get("TOTALPRICE"))
                                .setTotalPriceCur(map.get("TOTALPRICECUR"))
                                .setActualQuantity(map.get("ACTUALQUANTITY"))
                                .setAwaitQuantity(map.get("AWAITQUANTITY"))
                                .setHistory(map.get("HISTORY"))
                )
                .collect(Collectors.toList());
    }
}
