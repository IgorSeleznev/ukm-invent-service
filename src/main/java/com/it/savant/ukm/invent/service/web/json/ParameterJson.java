package com.it.savant.ukm.invent.service.web.json;

import com.it.savant.ukm.invent.service.domain.model.parameter.ParameterChooseItem;
import com.it.savant.ukm.invent.service.domain.model.parameter.ParameterType;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Data
@ToString
@Accessors(chain = true)
public class ParameterJson {

    private Long id;
    private String name;
    private String value;
    private ParameterType type;
    private List<ParameterChooseItem> chooses = newArrayList();
    private Boolean editable;
    private String title;
}
