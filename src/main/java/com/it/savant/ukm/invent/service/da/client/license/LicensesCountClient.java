package com.it.savant.ukm.invent.service.da.client.license;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class LicensesCountClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public LicensesCountClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Integer count() {
        final List<Integer> counts = jdbcTemplate.query(
                "SELECT count(*) as license_count \n" +
                        "FROM " + DATABASE_NAME + ".client_license",
                (resultSet, i) -> resultSet.getInt(1)
        );

        if (counts.size() == 0) {
            return 0;
        }
        return counts.get(0);
    }
}