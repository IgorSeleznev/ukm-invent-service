package com.it.savant.ukm.invent.service.da.client.inventory.item;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryItemListCloneByDocIdClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryItemListCloneByDocIdClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void clone(final String assortmentInventoryDocId, final String manuallyInventoryDocId) {
        jdbcTemplate.update(
                "INSERT INTO " + DATABASE_NAME + ".inventory_item (" +
                        "    docId, \n" +
                        "    docType, \n" +
                        "    specItem, \n" +
                        "    article, \n" +
                        "    displayItem, \n" +
                        " \n" +
                        "    itemPrice, \n" +
                        "    itemPriceCur, \n" +
                        "    quantity, \n" +
                        "    totalPrice, \n" +
                        "    totalPriceCur, \n" +
                        " \n" +
                        "    actualQuantity, \n" +
                        "    awaitQuantity, \n" +
                        "    history, \n" +
                        "    actual, \n" +
                        "    source_docId \n" +
                        ") \n" +
                        "SELECT  \n" +
                        "    ?, \n" +
                        "    aii.docType, \n" +
                        "    aii.specItem, \n" +
                        "    aii.article, \n" +
                        "    aii.displayItem, \n" +
                        " \n" +
                        "    aii.itemPrice, \n" +
                        "    aii.itemPriceCur, \n" +
                        "    0 as quantity, \n" +
                        "    aii.totalPrice, \n" +
                        "    aii.totalPriceCur, \n" +
                        " \n" +
                        "    0 as actualQuantity, \n" +
                        "    0 as awaitQuantity, \n" +
                        "    aii.history, \n" +
                        "    1 as actual, \n" +
                        "    ? \n" +
                        "FROM " + DATABASE_NAME + ".inventory_item aii, "+ DATABASE_NAME + ".inventory ai \n" +
                        "WHERE ai.id = aii.docid " +
                        "  AND ai.id = ?\n",
                manuallyInventoryDocId,
                manuallyInventoryDocId,
                assortmentInventoryDocId
        );
    }
}
