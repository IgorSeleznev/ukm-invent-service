package com.it.savant.ukm.invent.service.da.client.security;

import com.it.savant.ukm.invent.service.common.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class UserMaxIdClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public UserMaxIdClient(final JdbcTemplate source) {
        this.jdbcTemplate = source;
    }

    public Long maxId() {
        final List<Long> ids = jdbcTemplate.query(
                "SELECT max(id) FROM " + DATABASE_NAME + ".user",
                (resultSet, i) -> resultSet.getLong(1)
        );
        if (ids.size() == 0) {
            throw new NotFoundException("Not found max user id");
        }
        return ids.get(0);
    }

}
