package com.it.savant.ukm.invent.service.domain.service.inventory;

import com.it.savant.ukm.invent.service.common.exception.UkmInventServiceBusinessException;
import com.it.savant.ukm.invent.service.da.client.inventory.InventoryOrdernoByDocIdClient;
import com.it.savant.ukm.invent.service.da.client.inventory.InventoryManuallyExistByOrdernoNotConfirmedClient;
import com.it.savant.ukm.invent.service.da.client.inventory.InventoryOrdernoLocationByDocIdClient;
import com.it.savant.ukm.invent.service.da.entity.InventoryOrdernoLocationEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InventoryManuallyCheckedCreateService {

    private final InventoryOrdernoLocationByDocIdClient ordernoClient;
    private final InventoryManuallyExistByOrdernoNotConfirmedClient notConfirmedExistsClient;
    private final InventoryManuallyCreateByDocIdService createService;

    @Autowired
    public InventoryManuallyCheckedCreateService(
            final InventoryOrdernoLocationByDocIdClient ordernoClient,
            final InventoryManuallyExistByOrdernoNotConfirmedClient notConfirmedExistsClient,
            final InventoryManuallyCreateByDocIdService createService) {
        this.ordernoClient = ordernoClient;
        this.notConfirmedExistsClient = notConfirmedExistsClient;
        this.createService = createService;
    }

    public void checkAndCreate(final String docId, final String clientId) {
        final InventoryOrdernoLocationEntity ordernoLocation = ordernoClient.ordernoLocationByDocId(docId);
        if (notConfirmedExistsClient.existNotConfirmed(ordernoLocation.getOrderno(), ordernoLocation.getLocation())) {
            throw new UkmInventServiceBusinessException(
                    "Для ассортимента '" + ordernoLocation.getLocation() + "' и магазина с идентификатором " + ordernoLocation.getLocation() + " уже ест неподтвержденные, созданные вручную акты инвентаризации"
            );
        }
        createService.create(docId, clientId);
    }
}
