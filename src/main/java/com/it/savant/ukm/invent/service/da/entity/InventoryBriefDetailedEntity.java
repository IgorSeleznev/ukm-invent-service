package com.it.savant.ukm.invent.service.da.entity;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@ToString
@Accessors(chain = true)
public class InventoryBriefDetailedEntity {

    private String docId;
    private String assortmentName;
    private List<InventoryItemBriefEntity> items;
}
