package com.it.savant.ukm.invent.service.da.client.security;

import com.it.savant.ukm.invent.service.da.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class UserInsertClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public UserInsertClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void insert(final UserEntity userEntity) {
        jdbcTemplate.update(
                "INSERT INTO " + DATABASE_NAME + ".user ( \n" +
                        "    name, \n" +
                        "    password_hash, \n" +
                        "    location, " +
                        "    administrator, \n" +
                        "    administrator_login, \n" +
                        "    deleted \n" +
                        ") \n" +
                        "VALUES (?, ?, ?, ?, ?, 0)",
                userEntity.getName(),
                userEntity.getPasswordHash(),
                userEntity.getLocation(),
                Short.parseShort(userEntity.getAdministrator().toString()),
                userEntity.getAdministratorLogin()
        );
    }
}