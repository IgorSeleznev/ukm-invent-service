package com.it.savant.ukm.invent.service.da.client.store;

import com.it.savant.ukm.invent.service.common.exception.MoreThanOneFoundException;
import com.it.savant.ukm.invent.service.common.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class StoreIdFindByClientIdClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public StoreIdFindByClientIdClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public String findByClientId(final String clientId) {
        final List<String> results = jdbcTemplate.query(
                "SELECT location \n" +
                        "FROM " + DATABASE_NAME + ".client_license \n" +
                        "WHERE client_id = ?",
                (resultSet, i) -> resultSet.getString(1),
                clientId
        );
        if (results.size() == 0) {
            throw new NotFoundException("Store (location) not found by client id [" + clientId + "]");
        }
        if (results.size() > 1) {
            throw new MoreThanOneFoundException("Found more than one locations for client [" + clientId + "]");
        }
        return results.get(0);
    }
}
