package com.it.savant.ukm.invent.service.web.controller.admin.license;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.PageableSortableRequest;
import com.it.savant.ukm.invent.service.domain.model.license.License;
import com.it.savant.ukm.invent.service.domain.service.license.LicenseListService;
import com.it.savant.ukm.invent.service.web.json.LicenseJson;
import com.it.savant.ukm.invent.service.web.json.PageableSortableRequestJson;
import com.it.savant.ukm.invent.service.web.security.Authorized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
public class LicenseListController {

    private final LicenseListService service;
    private final Converter<PageableSortableRequestJson, PageableSortableRequest> requestConverter;
    private final Converter<License, LicenseJson> licenseConverter;

    @Autowired
    public LicenseListController(
            final LicenseListService service,
            final Converter<PageableSortableRequestJson, PageableSortableRequest> requestConverter,
            final Converter<License, LicenseJson> licenseConverter
    ) {
        this.service = service;
        this.requestConverter = requestConverter;
        this.licenseConverter = licenseConverter;
    }

    @Authorized(administratorOnly = true)
    @PostMapping("/license/list")
    public List<LicenseJson> list(@RequestBody final PageableSortableRequestJson request) {
        return service.list(
                requestConverter.convert(request)
        ).stream()
                .map(licenseConverter::convert)
                .collect(toList());
    }
}
