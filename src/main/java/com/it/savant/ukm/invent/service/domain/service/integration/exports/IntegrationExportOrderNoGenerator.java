package com.it.savant.ukm.invent.service.domain.service.integration.exports;

import com.it.savant.ukm.invent.service.common.exception.UkmInventServiceSystemException;
import com.it.savant.ukm.invent.service.domain.model.Inventory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@Slf4j
@Component
public class IntegrationExportOrderNoGenerator {

    public String exportOrderNoString(final Inventory inventory) {
        try {
            return String.format(
                    "%s/%s/%s",
                    inventory.getOrderNo(),
                    inventory.getLocation(),
                    new SimpleDateFormat("dd.MM.yyyy").format(
                            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(inventory.getFinalDate())
                    )
            );
        } catch (final ParseException exception) {
            log.error("Невозможно привести к дате значение {}", inventory.getFinalDate());
            throw new UkmInventServiceSystemException(exception);
        }
    }

}
