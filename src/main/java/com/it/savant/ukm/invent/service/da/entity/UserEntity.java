package com.it.savant.ukm.invent.service.da.entity;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class UserEntity {

    private Long id;
    private String name;
    private String administratorLogin;
    private String passwordHash;
    private String location;
    private Integer administrator;
}
