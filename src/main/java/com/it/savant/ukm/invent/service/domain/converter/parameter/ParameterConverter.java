package com.it.savant.ukm.invent.service.domain.converter.parameter;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.entity.ParameterEntity;
import com.it.savant.ukm.invent.service.domain.model.parameter.Parameter;
import com.it.savant.ukm.invent.service.domain.model.parameter.ParameterChooseItem;
import com.it.savant.ukm.invent.service.domain.model.parameter.ParameterType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ParameterConverter implements Converter<ParameterEntity, Parameter> {

    private final Converter<String, List<ParameterChooseItem>> itemConverter;

    @Autowired
    public ParameterConverter(final Converter<String, List<ParameterChooseItem>> itemConverter) {
        this.itemConverter = itemConverter;
    }

    @Override
    public Parameter convert(final ParameterEntity source) {
        return new Parameter()
                .setId(source.getId())
                .setName(source.getName())
                .setValue(source.getValue())
                .setType(ParameterType.valueOf(source.getType().toUpperCase()))
                .setTitle(source.getTitle())
                .setEditable(source.getEditable() == 1)
                .setChooses(itemConverter.convert(source.getChooses()));
    }
}
