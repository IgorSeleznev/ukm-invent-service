package com.it.savant.ukm.invent.service.domain.service.integration.imports.source;

import com.it.savant.ukm.invent.service.domain.service.integration.IntegrationType;

public interface IntegrationDirectorySourceBuilder {

    IntegrationType integrationType();

    IntegrationDirectorySource source();
}
