package com.it.savant.ukm.invent.service.domain.model.parameter;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static com.it.savant.ukm.invent.service.domain.model.parameter.ParameterType.TEXT;

@Data
@ToString
@Accessors(chain = true)
public class Parameter {

    private Long id;
    private String name;
    private String value;
    private ParameterType type = TEXT;
    private List<ParameterChooseItem> chooses = newArrayList();
    private Boolean editable = true;
    private String title;
}
