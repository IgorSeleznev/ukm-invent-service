package com.it.savant.ukm.invent.service.domain.service.integration.imports;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.client.fileimportlog.FileImportLogInsertClient;
import com.it.savant.ukm.invent.service.da.entity.FileImportLogEntity;
import com.it.savant.ukm.invent.service.domain.model.FileImportLog;
import com.it.savant.ukm.invent.service.domain.service.parameter.ParameterProvider;
import org.jlead.assets.common.utils.StackTracePrinter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FileImportLogService {

    private final FileImportLogInsertClient client;
    private final Converter<FileImportLog, FileImportLogEntity> converter;

    @Autowired
    public FileImportLogService(final FileImportLogInsertClient client,
                                final Converter<FileImportLog, FileImportLogEntity> converter) {
        this.client = client;
        this.converter = converter;
    }

    public void logSuccess(final String filename, final String content) {
        //client.insert(
        //        converter.convert(
        //                new FileImportLog()
        //                        .setSuccess(true)
        //                        .setFilename(filename)
        //                        .setContent(content)
        //        )
        //);
    }

    public void logFailure(final String filename, final String content, final Throwable throwable) {
        //client.insert(
        //        converter.convert(
        //                new FileImportLog()
        //                        .setSuccess(false)
        //                        .setErrorMessage(StackTracePrinter.print(throwable))
        //                        .setFilename(filename)
        //                        .setContent(content)
        //        )
        //);
    }
}
