package com.it.savant.ukm.invent.service.domain.service.integration.imports.source;

public interface IntegrationDirectorySource {

    boolean hasFiles();

    IntegrationFileSource nextFileSource();
}
