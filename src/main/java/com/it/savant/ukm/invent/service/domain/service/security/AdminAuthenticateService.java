package com.it.savant.ukm.invent.service.domain.service.security;

import com.it.savant.ukm.invent.service.domain.model.security.AdminAuthenticateRequest;
import com.it.savant.ukm.invent.service.domain.model.security.AuthenticateResponse;
import com.it.savant.ukm.invent.service.domain.model.security.User;
import com.it.savant.ukm.invent.service.domain.service.user.UserFindByLoginByPasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.it.savant.ukm.invent.service.domain.model.security.AuthenticateResponse.failed;

@Service
public class AdminAuthenticateService {

    private final AuthorizationUserRegisterService registerService;
    private final UserFindByLoginByPasswordService userFindService;

    @Autowired
    public AdminAuthenticateService(final AuthorizationUserRegisterService registerService,
                                    final UserFindByLoginByPasswordService userFindService) {
        this.registerService = registerService;
        this.userFindService = userFindService;
    }

    public AuthenticateResponse authorize(final AdminAuthenticateRequest request, final String clientUid) {
        final Optional<User> user = userFindService.findByIdByPassword(request.getLogin(), request.getPassword());
        if (user.isEmpty()) {
            return failed();
        }
        return new AuthenticateResponse()
                .setToken(
                        registerService.register(user.get(), clientUid).getToken()
                );
    }
}
