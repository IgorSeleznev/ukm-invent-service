package com.it.savant.ukm.invent.service.domain.service.transaction;

import com.it.savant.ukm.invent.service.da.client.CommitClient;
import com.it.savant.ukm.invent.service.da.client.RollbackClient;
import com.it.savant.ukm.invent.service.da.client.StartTransactionClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TransactionManageService {

    private final CommitClient commitClient;
    private final RollbackClient rollbackClient;
    private final StartTransactionClient startTransactionClient;

    @Autowired
    public TransactionManageService(final CommitClient commitClient,
                                    final RollbackClient rollbackClient,
                                    final StartTransactionClient startTransactionClient) {
        this.commitClient = commitClient;
        this.rollbackClient = rollbackClient;
        this.startTransactionClient = startTransactionClient;
    }

    public void start() {
        startTransactionClient.start();
    }

    public void commit() {
        commitClient.commit();
    }

    public void rollback() {
        rollbackClient.rollback();
    }
}
