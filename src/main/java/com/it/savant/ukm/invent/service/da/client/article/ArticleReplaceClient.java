package com.it.savant.ukm.invent.service.da.client.article;

import com.it.savant.ukm.invent.service.da.entity.ArticleEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class ArticleReplaceClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ArticleReplaceClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void replace(final ArticleEntity article) {
        jdbcTemplate.update(
                "REPLACE INTO "+ DATABASE_NAME + ".article ( " +
                        "    article, \n" +
                        "    accepted, \n" +
                        "    bornin, \n" +
                        "    cashload, \n" +
                        "    country, \n" +
                        "    cutpricedays, \n" +
                        "    datasubtype, \n" +
                        "    datatype, \n" +
                        "    flags, \n" +
                        "    globalarticle, \n" +
                        "    idclass, \n" +
                        "    idmeasurement, \n" +
                        "    losses, \n" +
                        "    mesabbrev, \n" +
                        "    mesname, \n" +
                        "    minprofit, \n" +
                        "    name, \n" +
                        "    quantitydeviation, \n" +
                        "    receiptok, \n" +
                        "    scaleload, \n" +
                        "    scrap, \n" +
                        "    shortname, \n" +
                        "    storage, \n" +
                        "    usetimedim, \n" +
                        "    waste \n" +
                        ") \n" +
                        "VALUES ( \n" +
                        "    ?, \n" +
                        "    ?, \n" +
                        "    ?, \n" +
                        "    ?, \n" +
                        "    ?, \n" +
                        "    ?, \n" +
                        "    ?, \n" +
                        "    ?, \n" +
                        "    ?, \n" +
                        "    ?, \n" +
                        "    ?, \n" +
                        "    ?, \n" +
                        "    ?, \n" +
                        "    ?, \n" +
                        "    ?, \n" +
                        "    ?, \n" +
                        "    ?, \n" +
                        "    ?, \n" +
                        "    ?, \n" +
                        "    ?, \n" +
                        "    ?, \n" +
                        "    ?, \n" +
                        "    ?, \n" +
                        "    ?, \n" +
                        "    ? \n" +
                        ")",
                article.getArticle(),
                article.getAccepted(),
                article.getBornin(),
                article.getCashload(),
                article.getCountry(),
                article.getCutpricedays(),
                article.getDatasubtype(),
                article.getDatatype(),
                article.getFlags(),
                article.getGlobalarticle(),
                article.getIdclass(),
                article.getIdmeasurement(),
                article.getLosses(),
                article.getMesabbrev(),
                article.getMesname(),
                article.getMinprofit(),
                article.getName(),
                article.getQuantitydeviation(),
                article.getReceiptok(),
                article.getScaleload(),
                article.getScrap(),
                article.getShortname(),
                article.getStorage(),
                article.getUsetimedim(),
                article.getWaste()
        );
    }
}
