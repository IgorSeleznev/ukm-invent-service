package com.it.savant.ukm.invent.service.domain.service.integration.imports.assemble;

import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.parser.XmlNode;

public interface IntegrationAssembleService<T> {

    String getAssembleName();

    T assemble(final XmlNode rootNode);
}
