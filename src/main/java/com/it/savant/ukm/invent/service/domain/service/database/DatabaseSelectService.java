package com.it.savant.ukm.invent.service.domain.service.database;

import com.it.savant.ukm.invent.service.da.client.database.DatabaseSelectClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class DatabaseSelectService {

    private final DatabaseSelectClient client;

    @Autowired
    public DatabaseSelectService(final DatabaseSelectClient client) {
        this.client = client;
    }

    public void select() {
        client.select(DATABASE_NAME);
    }
}
