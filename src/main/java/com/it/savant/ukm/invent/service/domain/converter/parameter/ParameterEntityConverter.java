package com.it.savant.ukm.invent.service.domain.converter.parameter;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.entity.ParameterEntity;
import com.it.savant.ukm.invent.service.domain.model.parameter.Parameter;
import com.it.savant.ukm.invent.service.domain.model.parameter.ParameterChooseItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ParameterEntityConverter implements Converter<Parameter, ParameterEntity> {

    private final Converter<List<ParameterChooseItem>, String> stringConverter;

    @Autowired
    public ParameterEntityConverter(final Converter<List<ParameterChooseItem>, String> stringConverter) {
        this.stringConverter = stringConverter;
    }

    @Override
    public ParameterEntity convert(final Parameter source) {
        return new ParameterEntity()
                .setId(source.getId())
                .setName(source.getName())
                .setValue(source.getValue())
                .setEditable((short) (source.getEditable() ? 1 : 0))
                .setTitle(source.getTitle())
                .setType(source.getType().toString())
                .setChooses(
                        stringConverter.convert(source.getChooses())
                );
    }
}
