package com.it.savant.ukm.invent.service.domain.service.store.brief;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.client.store.StoreBriefListClient;
import com.it.savant.ukm.invent.service.da.entity.StoreBriefEntity;
import com.it.savant.ukm.invent.service.domain.model.StoreBrief;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class StoreBriefListService {

    private final StoreBriefListClient client;
    private final Converter<StoreBriefEntity, StoreBrief> converter;

    @Autowired
    public StoreBriefListService(final StoreBriefListClient client,
                                 final Converter<StoreBriefEntity, StoreBrief> converter) {
        this.client = client;
        this.converter = converter;
    }

    public List<StoreBrief> listAll() {
        return client.findAll().stream()
                .map(converter::convert)
                .collect(toList());
    }
}
