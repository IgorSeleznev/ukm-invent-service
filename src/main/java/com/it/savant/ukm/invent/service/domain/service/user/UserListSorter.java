package com.it.savant.ukm.invent.service.domain.service.user;

import com.it.savant.ukm.invent.service.domain.model.PageableSortableRequest;
import com.it.savant.ukm.invent.service.domain.model.StoreBrief;
import com.it.savant.ukm.invent.service.domain.model.security.User;
import com.it.savant.ukm.invent.service.domain.service.store.brief.StoreBriefListService;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@Component
public class UserListSorter {

    private final StoreBriefListService storeListService;

    @Autowired
    public UserListSorter(final StoreBriefListService storeListService) {
        this.storeListService = storeListService;
    }

    public List<User> sort(final List<User> users, final PageableSortableRequest request) {
        final List<StoreBrief> stores = storeListService.listAll();
        final Map<String, StoreBrief> storeMap = stores.stream()
                .collect(toMap(StoreBrief::getId, storeBrief -> storeBrief));
        final String fieldName = request.getSortField() == null
                ? "name"
                : request.getSortField().toLowerCase();
        final String direction = request.getSortDirection() == null
                ? "asc"
                : request.getSortDirection().toLowerCase();
        return users.stream()
                .sorted(
                        (left, right) -> {
                            if ("name".equals(fieldName)) {
                                return "asc".equals(direction)
                                        ? StringUtils.compare(left.getName(), right.getName())
                                        : StringUtils.compare(right.getName(), left.getName());
                            }
                            if ("administratorlogin".equals(request.getSortField().toLowerCase())) {
                                return "asc".equals(direction)
                                        ? StringUtils.compare(left.getAdministratorLogin(), right.getAdministratorLogin())
                                        : StringUtils.compare(right.getAdministratorLogin(), left.getAdministratorLogin());
                            }
                            if ("administrator".equals(request.getSortField().toLowerCase())) {
                                return "asc".equals(direction)
                                        ? BooleanUtils.compare(left.getAdministrator(), right.getAdministrator())
                                        : BooleanUtils.compare(right.getAdministrator(), left.getAdministrator());
                            }
                            if ("passwordhash".equals(request.getSortField().toLowerCase())) {
                                return "asc".equals(direction)
                                        ? StringUtils.compare(left.getPasswordHash(), right.getPasswordHash())
                                        : StringUtils.compare(right.getPasswordHash(), left.getPasswordHash());
                            }
                            if ("location".equals(request.getSortField().toLowerCase())) {
                                return "asc".equals(direction)
                                        ?
                                        StringUtils.compare(
                                                storeMap.get(left.getLocation()).getName(),
                                                storeMap.get(right.getLocation()).getName()
                                        )
                                        :
                                        StringUtils.compare(
                                                storeMap.get(right.getLocation()).getName(),
                                                storeMap.get(left.getLocation()).getName()
                                        );
                            }
                            return 0;
                        }
                )
                .collect(toList());
    }
}