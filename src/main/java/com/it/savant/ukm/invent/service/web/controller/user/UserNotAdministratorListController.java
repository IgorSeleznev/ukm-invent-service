package com.it.savant.ukm.invent.service.web.controller.user;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.security.UserListItem;
import com.it.savant.ukm.invent.service.domain.service.user.UserListAllNotAdministratorService;
import com.it.savant.ukm.invent.service.web.json.Response;
import com.it.savant.ukm.invent.service.web.json.security.UserListItemJson;
import com.it.savant.ukm.invent.service.web.security.Licensed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static com.it.savant.ukm.invent.service.web.json.Response.success;

@RestController
public class UserNotAdministratorListController {

    private final UserListAllNotAdministratorService service;
    private final Converter<UserListItem, UserListItemJson> converter;

    @Autowired
    public UserNotAdministratorListController(final UserListAllNotAdministratorService service,
                                              final Converter<UserListItem, UserListItemJson> converter
    ) {
        this.service = service;
        this.converter = converter;
    }

    @Licensed(checkLocation = false)
    @GetMapping("/user/list")
    public Response<List<UserListItemJson>> listAll() {
        return success(
                service.listAllNotAdministrator().stream()
                        .map(converter::convert)
                        .collect(Collectors.toList())
        );
    }
}
