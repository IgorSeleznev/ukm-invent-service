package com.it.savant.ukm.invent.service.da.client.inventory;

import com.it.savant.ukm.invent.service.da.entity.InventoryEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryInsertClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryInsertClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void insert(final InventoryEntity inventory) {
        jdbcTemplate.update("REPLACE INTO " + DATABASE_NAME + ".inventory ( \n" +
                        "    id, \n" +
                        "    docType, \n" +
                        "    bornIn, \n" +
                        "    commentary, \n" +
                        "    createDat, \n" +

                        "    currencyMultOrder, \n" +
                        "    currencyRate, \n" +
                        "    currencyType, \n" +
                        "    docState, \n" +
                        "    isRoubles, \n" +

                        "    location, \n" +
                        "    opCode, \n" +
                        "    priceRoundMode, \n" +
                        "    totalSum, \n" +
                        "    totalSumCur, \n" +

                        "    fillSpecType, \n" +
                        "    finalDate, \n" +
                        "    isActiveOnly, \n" +
                        "    isFillComplete, \n" +
                        "    orderNo, \n" +

                        "    ourSelfClient, \n" +
                        "    preambleDate, \n" +
                        "    priceMode, \n" +
                        "    priceType, \n" +
                        "    storeLoc, \n" +

                        "    withDue, \n" +
                        "    confirmed, \n" +
                        "    created_manually \n" +
                        ") \n" +
                        "VALUES ( \n" +
                        "     ?, ?, ?, ?, ?, \n" +
                        "     ?, ?, ?, ?, ?, \n" +
                        "     ?, ?, ?, ?, ?, \n" +
                        "     ?, ?, ?, ?, ?, \n" +
                        "     ?, ?, ?, ?, ?, \n" +
                        "     ?, ?, ? \n" +
                        ")",
                inventory.getId(),
                inventory.getDocType(),
                inventory.getBornIn(),
                inventory.getCommentary(),
                inventory.getCreateDat(),

                inventory.getCurrencyMultOrder(),
                inventory.getCurrencyRate(),
                inventory.getCurrencyType(),
                inventory.getDocState(),
                inventory.getIsRoubles(),

                inventory.getLocation(),
                inventory.getOpCode(),
                inventory.getPriceRoundMode(),
                inventory.getTotalSum(),
                inventory.getTotalSumCur(),

                inventory.getFillSpecType(),
                inventory.getFinalDate(),
                inventory.getIsActiveOnly(),
                inventory.getIsFillComplete(),
                inventory.getOrderNo(),

                inventory.getOurSelfClient(),
                inventory.getPreambleDate(),
                inventory.getPriceMode(),
                inventory.getPriceType(),
                inventory.getStoreLoc(),

                inventory.getWithDue(),
                inventory.getConfirmed(),
                inventory.getCreatedManually()
        );
    }
}