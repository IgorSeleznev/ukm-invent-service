package com.it.savant.ukm.invent.service.da.client.inventory.brief;

import com.it.savant.ukm.invent.service.da.entity.InventoryBriefDetailedEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryBriefDetailedFindByDocIdClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryBriefDetailedFindByDocIdClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public InventoryBriefDetailedEntity findOneByDocId(final String docId) {
        final List<InventoryBriefDetailedEntity> result = jdbcTemplate.query(
                "SELECT id, orderno as assortmentName \n" +
                        "FROM " + DATABASE_NAME + ".inventory \n" +
                        "WHERE id = ?",
                (resultSet, i) -> new InventoryBriefDetailedEntity()
                        .setDocId(resultSet.getString(1))
                        .setAssortmentName(resultSet.getString(2)),
                docId
        );

        if (result.size() == 0) {
            return null;
        }
        return result.get(0);
    }
}