package com.it.savant.ukm.invent.service.domain.service.parameter;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.client.parameter.ParameterListAllClient;
import com.it.savant.ukm.invent.service.da.entity.ParameterEntity;
import com.it.savant.ukm.invent.service.domain.model.parameter.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class ParameterListAllService {

    private final ParameterListAllClient client;
    private final Converter<ParameterEntity, Parameter> converter;

    @Autowired
    public ParameterListAllService(final ParameterListAllClient client,
                                   final Converter<ParameterEntity, Parameter> converter) {
        this.client = client;
        this.converter = converter;
    }

    public List<Parameter> listAll() {
        return client.listAll().stream()
                .map(converter::convert)
                .collect(toList());
    }
}
