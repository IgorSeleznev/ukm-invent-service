package com.it.savant.ukm.invent.service.domain.service.integration.exports.target;

import com.it.savant.ukm.invent.service.domain.service.integration.IntegrationType;
import com.it.savant.ukm.invent.service.domain.service.parameter.ParameterProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;
import static com.it.savant.ukm.invent.service.domain.constants.ParameterConstants.INTEGRATION_TYPE;

@Component
public class IntegrationFileTargetBuilderResolver {

    private final ParameterProvider parameterProvider;
    private final Map<IntegrationType, IntegrationFileTargetBuilder> builders = newHashMap();

    @Autowired
    public IntegrationFileTargetBuilderResolver(final ParameterProvider parameterProvider,
                                                final List<IntegrationFileTargetBuilder> builders) {
        this.parameterProvider = parameterProvider;
        builders.forEach(
                builder -> this.builders.put(builder.getIntegrationType(), builder)
        );
    }

    public IntegrationFileTargetBuilder resolve() {
        final String integrationType = parameterProvider.read(INTEGRATION_TYPE);
        return builders.get(IntegrationType.valueOf(integrationType));
    }
}
