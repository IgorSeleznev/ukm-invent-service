package com.it.savant.ukm.invent.service.da.client.inventory.item;

import com.it.savant.ukm.invent.service.da.entity.InventoryItemEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryItemListByDocIdClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryItemListByDocIdClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<InventoryItemEntity> listByDocId(final String docId) {
        return jdbcTemplate.query(
                "SELECT \n" +
                        "      docId, \n" +
                        "      docType, \n" +
                        "      specItem, \n" +
                        "      article, \n" +
                        "      displayItem, \n" +
                        "   \n" +
                        "      itemPrice, \n" +
                        "      itemPriceCur, \n" +
                        "      quantity, \n" +
                        "      totalPrice, \n" +
                        "      totalPriceCur, \n" +
                        "   \n" +
                        "      actualQuantity, \n" +
                        "      awaitQuantity, \n" +
                        "      history \n" +
                        "FROM " + DATABASE_NAME + ".inventory_item \n" +
                        "WHERE docId = ? AND actual = 1",
                (resultSet, i) -> new InventoryItemEntity()
                        .setDocId(resultSet.getString(1))
                        .setDocType(resultSet.getString(2))
                        .setSpecItem(resultSet.getString(3))
                        .setArticle(resultSet.getString(4))
                        .setDisplayItem(resultSet.getString(5))

                        .setItemPrice(resultSet.getString(6))
                        .setItemPriceCur(resultSet.getString(7))
                        .setQuantity(resultSet.getString(8))
                        .setTotalPrice(resultSet.getString(9))
                        .setTotalPriceCur(resultSet.getString(10))

                        .setActualQuantity(resultSet.getString(11))
                        .setAwaitQuantity(resultSet.getString(12))
                        .setHistory(resultSet.getString(13)),
                docId
        );
    }
}
