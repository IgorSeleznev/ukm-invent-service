package com.it.savant.ukm.invent.service.web.controller.inventory.confirm;

import com.it.savant.ukm.invent.service.domain.service.inventory.InventoryConfirmForceService;
import com.it.savant.ukm.invent.service.web.json.DocIdRequestJson;
import com.it.savant.ukm.invent.service.web.json.Response;
import com.it.savant.ukm.invent.service.web.security.Authorized;
import com.it.savant.ukm.invent.service.web.security.Licensed;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import static com.it.savant.ukm.invent.service.web.constant.HttpHeaderConstant.HTTP_HEADER_CLIENT_UID;
import static com.it.savant.ukm.invent.service.web.json.Response.success;

@Slf4j
@RestController
public class InventoryConfirmForceController {

    private final InventoryConfirmForceService service;

    @Autowired
    public InventoryConfirmForceController(final InventoryConfirmForceService service) {
        this.service = service;
    }

    @Licensed
    @Authorized
    @PostMapping("/inventory/confirm/force")
    public Response<Void> confirmForce(@RequestBody final DocIdRequestJson request, final HttpServletRequest httpRequest) {
        log.info(
                "Получен запрос на принудительное подтверждение акта инвентаризации '{}' от клиента с идентификатором '{}'",
                request.getDocId(),
                httpRequest.getHeader(HTTP_HEADER_CLIENT_UID)
        );
        service.confirmForce(request.getDocId(), httpRequest.getHeader(HTTP_HEADER_CLIENT_UID));
        return success();
    }
}
