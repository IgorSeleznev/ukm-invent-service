package com.it.savant.ukm.invent.service.da.client.initialize;

public interface CreateTableClient {

    void create();

    String getTableName();
}
