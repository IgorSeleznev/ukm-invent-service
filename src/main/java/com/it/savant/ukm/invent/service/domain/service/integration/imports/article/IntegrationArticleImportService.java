package com.it.savant.ukm.invent.service.domain.service.integration.imports.article;

import com.it.savant.ukm.invent.service.domain.service.integration.imports.ImportService;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.IntegrationDeserializeService;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.parser.XmlNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class IntegrationArticleImportService implements ImportService {

    private final IntegrationDeserializeService deserializeService;
    private final IntegrationArticleSaveService saveService;

    @Autowired
    public IntegrationArticleImportService(final IntegrationDeserializeService deserializeService,
                                           final IntegrationArticleSaveService saveService) {
        this.deserializeService = deserializeService;
        this.saveService = saveService;
    }

    @Override
    public boolean isValid(final XmlNode xml) {
        final List<XmlNode> nodes = xml.getNodes().get(0)
                .getNodes().get(0)
                .getNodes();
        for (final XmlNode node : nodes) {
            if ("SMCARD".equals(node.getName())) {
                log.info("Импорт данных: обнаружен файл описания товарной позиции");
                return true;
            }
        }
        return false;
    }

    @Override
    public void doImport(final XmlNode xml) {
        log.info("Импорт данных: импорт товарной позиции");
        saveService.save(
                deserializeService.deserialize(xml, "ARTICLE")
        );
    }
}
