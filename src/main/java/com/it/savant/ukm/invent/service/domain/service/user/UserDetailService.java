package com.it.savant.ukm.invent.service.domain.service.user;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.common.exception.NotFoundException;
import com.it.savant.ukm.invent.service.da.client.security.UserFindByIdClient;
import com.it.savant.ukm.invent.service.da.entity.UserEntity;
import com.it.savant.ukm.invent.service.domain.model.security.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class UserDetailService {

    private final UserFindByIdClient findClient;
    private final Converter<UserEntity, User> converter;

    @Autowired
    public UserDetailService(final UserFindByIdClient findClient,
                             final Converter<UserEntity, User> converter) {
        this.findClient = findClient;
        this.converter = converter;
    }

    public User findById(final Long id) {
        final Optional<UserEntity> found = findClient.findById(id);
        if (found.isPresent()) {
            return converter.convert(found.get());
        }
        log.error("Не найден пользователь с идентификатором {}", id);
        throw new NotFoundException("Не найден пользователь с идентификатором " + id);
    }
}
