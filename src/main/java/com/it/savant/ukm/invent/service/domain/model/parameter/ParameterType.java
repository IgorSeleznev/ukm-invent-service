package com.it.savant.ukm.invent.service.domain.model.parameter;

public enum ParameterType {

    TEXT,
    NUMBER,
    PASSWORD,
    POPUP
}
