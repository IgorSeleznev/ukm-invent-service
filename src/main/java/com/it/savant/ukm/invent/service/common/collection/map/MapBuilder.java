package com.it.savant.ukm.invent.service.common.collection.map;

import java.util.HashMap;
import java.util.Map;

public class MapBuilder<K, V> {

    public static <K, V> MapBuilder<K, V> mapBuilder() {
        return new MapBuilder<K, V>();
    }

    private final Map<K, V> map = new HashMap<>();

    public MapBuilder<K, V> put(final K key, final V value) {
        map.put(key, value);
        return this;
    }

    public MapBuilder<K, V> put(final Object... args) {
        for (int i = 0; i < args.length; i += 2) {
            map.put((K) args[i], (V) args[i+1]);
        }
        return this;
    }

    public MapBuilder<K, V> putIfExists(final K key, final V value) {
        if (value != null) {
            return put(key, value);
        }
        return this;
    }

    public MapBuilder<K, V> putIfTrue(final boolean condition, final K key, final V value) {
        if (condition) {
            return put(key, value);
        }
        return this;
    }

    public MapBuilder<K, V> putAll(final Map<K, V> map) {
        this.map.putAll(map);
        return this;
    }

    public Map<K, V> build() {
        return map;
    }
}
