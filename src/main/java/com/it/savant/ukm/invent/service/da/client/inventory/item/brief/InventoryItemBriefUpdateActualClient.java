package com.it.savant.ukm.invent.service.da.client.inventory.item.brief;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryItemBriefUpdateActualClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryItemBriefUpdateActualClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void update(final String docId, final short actual) {
        jdbcTemplate.update(
                "UPDATE " + DATABASE_NAME + ".inventory_item \n" +
                        "SET actual = ? \n" +
                        "WHERE docid = ?",
                actual,
                docId
        );
    }
}
