package com.it.savant.ukm.invent.service.common.value;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ValueHolder<T> {

    public static <T> ValueHolder<T> hold(final T value) {
        return new ValueHolder<>(value);
    }

    public static <T> ValueHolder<T> hold() {
        return new ValueHolder<>();
    }

    private T value = null;
}
