package com.it.savant.ukm.invent.service.domain.service.inventory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.it.savant.ukm.invent.service.domain.service.inventory.InventoryConfirmAndCreateService.INVENTORY_CONFIRM_AND_CREATE_SERVICE;

@Service(INVENTORY_CONFIRM_AND_CREATE_SERVICE)
public class InventoryConfirmAndCreateService implements InventoryConfirmService {

    static final String INVENTORY_CONFIRM_AND_CREATE_SERVICE = "com.it.savant.ukm.invent.service.domain.service.inventory.InventoryConfirmAndCreateService";

    private final InventoryManuallyCreateByDocIdService createService;
    private final InventoryConfirmAndExportService confirmAndExportService;

    @Autowired
    public InventoryConfirmAndCreateService(final InventoryManuallyCreateByDocIdService createService,
                                            final InventoryConfirmAndExportService confirmAndExportService) {
        this.createService = createService;
        this.confirmAndExportService = confirmAndExportService;
    }

    @Transactional
    public void confirm(final String docId, final String clientId) {
        confirmAndExportService.confirmAndExport(docId);
        createService.create(docId, clientId);
    }
}
