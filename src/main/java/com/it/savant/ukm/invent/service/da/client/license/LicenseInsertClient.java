package com.it.savant.ukm.invent.service.da.client.license;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class LicenseInsertClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public LicenseInsertClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void insert(final String license) {
        jdbcTemplate.update(
                "INSERT INTO " + DATABASE_NAME + ".client_license (client_id) \n" +
                        "VALUES (?)",
                license
        );
    }
}
