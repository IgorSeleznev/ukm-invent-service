package com.it.savant.ukm.invent.service.da.client.measure;

import com.it.savant.ukm.invent.service.da.entity.MeasureEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class MeasureReplaceClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public MeasureReplaceClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void replace(final MeasureEntity measureEntity) {
        jdbcTemplate.update(
                "REPLACE INTO " + DATABASE_NAME + ".measure ( \n" +
                        "    id, \n" +
                        "    abbrev, \n" +
                        "    baseType, \n" +
                        "    code, \n" +
                        "    codeIso, \n" +
                        "    mesType, \n" +
                        "    name, \n" +
                        "    prec, \n" +
                        "    unitName, \n" +
                        "    increase_step \n" +
                        ") \n" +
                        "VALUES ( \n" +
                        "    ?, ?, ?, ?, \n" +
                        "    ?, ?, ?, ?, \n" +
                        "    ?, ? \n" +
                        ")",
                measureEntity.getId(),
                measureEntity.getAbbrev(),
                measureEntity.getBaseType(),
                measureEntity.getCode(),
                measureEntity.getCodeIso(),
                measureEntity.getMesType(),
                measureEntity.getName(),
                measureEntity.getPrec(),
                measureEntity.getUnitName(),
                measureEntity.getIncreaseStep()
        );
    }
}
