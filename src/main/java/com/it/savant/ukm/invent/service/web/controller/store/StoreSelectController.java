package com.it.savant.ukm.invent.service.web.controller.store;

import com.it.savant.ukm.invent.service.domain.service.license.LicenseLocationLinkService;
import com.it.savant.ukm.invent.service.web.interceptor.RequestHeaderExtractor;
import com.it.savant.ukm.invent.service.web.json.Response;
import com.it.savant.ukm.invent.service.web.json.StoreClientLinkUpdateRequestJson;
import com.it.savant.ukm.invent.service.web.security.Authorized;
import com.it.savant.ukm.invent.service.web.security.Licensed;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import static com.it.savant.ukm.invent.service.web.constant.HttpHeaderConstant.HTTP_HEADER_CLIENT_UID;

@Slf4j
@RestController
public class StoreSelectController {

    private final LicenseLocationLinkService service;
    private final RequestHeaderExtractor headerExtractor;

    @Autowired
    public StoreSelectController(final LicenseLocationLinkService service,
                                 final RequestHeaderExtractor headerExtractor) {
        this.service = service;
        this.headerExtractor = headerExtractor;
    }

    @Licensed(checkLocation = false)
    @PostMapping("/store/select")
    public Response<Void> linkUpdate(
            @RequestBody final StoreClientLinkUpdateRequestJson request,
            final HttpServletRequest httpRequest
    ) {
        log.info(
                "Клиент с уникальным идентификатором '{}' привязан к магазину с идентификатором {}",
                httpRequest.getHeader(HTTP_HEADER_CLIENT_UID),
                request.getStoreId()
        );
        service.link(
                request.getStoreId(),
                headerExtractor.extract(httpRequest, HTTP_HEADER_CLIENT_UID)
        );
        return Response.success();
    }
}
