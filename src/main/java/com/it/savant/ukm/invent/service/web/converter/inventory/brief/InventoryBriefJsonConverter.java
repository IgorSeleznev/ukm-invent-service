package com.it.savant.ukm.invent.service.web.converter.inventory.brief;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.InventoryBrief;
import com.it.savant.ukm.invent.service.web.json.InventoryBriefJson;
import org.springframework.stereotype.Component;

@Component
public class InventoryBriefJsonConverter implements Converter<InventoryBrief, InventoryBriefJson> {

    @Override
    public InventoryBriefJson convert(final InventoryBrief source) {
        return new InventoryBriefJson()
                .setDocId(source.getDocId())
                .setAssortmentName(source.getAssortmentName());
    }
}
