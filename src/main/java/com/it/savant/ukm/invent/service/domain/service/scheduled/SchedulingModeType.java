package com.it.savant.ukm.invent.service.domain.service.scheduled;

public enum SchedulingModeType {

    CRON,
    INTERVAL
}
