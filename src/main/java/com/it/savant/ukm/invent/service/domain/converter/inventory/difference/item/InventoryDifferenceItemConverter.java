package com.it.savant.ukm.invent.service.domain.converter.inventory.difference.item;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.entity.InventoryDifferenceItemEntity;
import com.it.savant.ukm.invent.service.domain.model.InventoryDifferenceItem;
import org.springframework.stereotype.Component;

@Component
public class InventoryDifferenceItemConverter implements Converter<InventoryDifferenceItemEntity, InventoryDifferenceItem> {

    @Override
    public InventoryDifferenceItem convert(final InventoryDifferenceItemEntity source) {
        return new InventoryDifferenceItem()
                .setArticle(source.getArticle())
                .setName(source.getName());
    }
}
