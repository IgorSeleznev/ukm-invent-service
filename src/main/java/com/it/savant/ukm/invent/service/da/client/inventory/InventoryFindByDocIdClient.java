package com.it.savant.ukm.invent.service.da.client.inventory;

import com.it.savant.ukm.invent.service.da.entity.InventoryEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;
import static java.util.Optional.empty;
import static java.util.Optional.of;

@Component
public class InventoryFindByDocIdClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryFindByDocIdClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Optional<InventoryEntity> findByDocId(final String docId) {
        final List<InventoryEntity> found = jdbcTemplate.query(
                "SELECT \n" +
                        "                        id, \n" +
                        "                        docType, \n" +
                        "                        bornIn, \n" +
                        "                        commentary, \n" +
                        "                        createDat, \n" +

                        "                        currencyMultOrder, \n" +
                        "                        currencyRate, \n" +
                        "                        currencyType, \n" +
                        "                        docState, \n" +
                        "                        isRoubles, \n" +

                        "                        location, \n" +
                        "                        opCode, \n" +
                        "                        priceRoundMode, \n" +
                        "                        totalSum, \n" +
                        "                        totalSumCur, \n" +

                        "                        fillSpecType, \n" +
                        "                        finalDate, \n" +
                        "                        isActiveOnly, \n" +
                        "                        isFillComplete, \n" +
                        "                        orderNo, \n" +

                        "                        ourSelfClient, \n" +
                        "                        preambleDate, \n" +
                        "                        priceMode, \n" +
                        "                        priceType, \n" +
                        "                        storeLoc, \n" +

                        "                        withDue " +
                        "FROM " + DATABASE_NAME + ".inventory \n" +
                        "WHERE id = ?",
                (resultSet, i) -> new InventoryEntity()
                        .setId(resultSet.getString(1))
                        .setDocType(resultSet.getString(2))
                        .setBornIn(resultSet.getString(3))
                        .setCommentary(resultSet.getString(4))
                        .setCreateDat(resultSet.getString(5))

                        .setCurrencyMultOrder(resultSet.getString(6))
                        .setCurrencyRate(resultSet.getString(7))
                        .setCurrencyType(resultSet.getString(8))
                        .setDocState(resultSet.getString(9))
                        .setIsRoubles(resultSet.getString(10))

                        .setLocation(resultSet.getString(11))
                        .setOpCode(resultSet.getString(12))
                        .setPriceRoundMode(resultSet.getString(13))
                        .setTotalSum(resultSet.getString(14))
                        .setTotalSumCur(resultSet.getString(15))

                        .setFillSpecType(resultSet.getString(16))
                        .setFinalDate(resultSet.getString(17))
                        .setIsActiveOnly(resultSet.getString(18))
                        .setIsFillComplete(resultSet.getString(19))
                        .setOrderNo(resultSet.getString(20))

                        .setOurSelfClient(resultSet.getString(21))
                        .setPreambleDate(resultSet.getString(22))
                        .setPriceMode(resultSet.getString(23))
                        .setPriceType(resultSet.getString(24))
                        .setStoreLoc(resultSet.getString(25))

                        .setWithDue(resultSet.getString(26))
                ,
                docId
        );

        if (found.size() == 0) {
            return empty();
        }
        return of(found.get(0));
    }
}
