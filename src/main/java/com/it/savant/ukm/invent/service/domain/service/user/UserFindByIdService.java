package com.it.savant.ukm.invent.service.domain.service.user;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.common.exception.NotFoundException;
import com.it.savant.ukm.invent.service.da.client.security.UserFindByIdClient;
import com.it.savant.ukm.invent.service.da.entity.UserEntity;
import com.it.savant.ukm.invent.service.domain.model.security.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserFindByIdService {

    private final UserFindByIdClient client;
    private final Converter<UserEntity, User> converter;

    @Autowired
    public UserFindByIdService(final UserFindByIdClient client,
                               final Converter<UserEntity, User> converter) {
        this.client = client;
        this.converter = converter;
    }

    public User findById(final Long id) {
        final Optional<UserEntity> found = client.findById(id);
        if (found.isEmpty()) {
            throw new NotFoundException("Не найден пользователь с идентификатором " + id);
        }
        return converter.convert(found.get());
    }
}
