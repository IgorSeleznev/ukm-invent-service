package com.it.savant.ukm.invent.service.da.client.inventory.brief;

import com.it.savant.ukm.invent.service.da.client.inventory.item.brief.InventoryItemBriefListByDocIdClient;
import com.it.savant.ukm.invent.service.da.entity.InventoryBriefDetailedEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InventoryBriefDetailedEntityComposeService {

    private final InventoryBriefDetailedFindByDocIdClient inventoryBriefClient;
    private final InventoryItemBriefListByDocIdClient cardListClient;

    @Autowired
    public InventoryBriefDetailedEntityComposeService(final InventoryBriefDetailedFindByDocIdClient inventoryBriefClient,
                                                      final InventoryItemBriefListByDocIdClient cardListClient) {
        this.inventoryBriefClient = inventoryBriefClient;
        this.cardListClient = cardListClient;
    }

    public InventoryBriefDetailedEntity findCompose(final String docId) {
        return inventoryBriefClient
                .findOneByDocId(docId)
                .setItems(
                        cardListClient.findAllByClassifier(docId)
                );
    }
}
