package com.it.savant.ukm.invent.service.domain.service.store;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.client.store.StoreReplaceClient;
import com.it.savant.ukm.invent.service.da.entity.StoreEntity;
import com.it.savant.ukm.invent.service.domain.model.Store;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StoreInsertService {

    private final StoreReplaceClient insertClient;
    private final Converter<Store, StoreEntity> converter;

    @Autowired
    public StoreInsertService(final StoreReplaceClient insertClient,
                              final Converter<Store, StoreEntity> converter) {
        this.insertClient = insertClient;
        this.converter = converter;
    }

    public void insert(final Store store) {
        insertClient.replace(
                converter.convert(store)
        );
    }
}
