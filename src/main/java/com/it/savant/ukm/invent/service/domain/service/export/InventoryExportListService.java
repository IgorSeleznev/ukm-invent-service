package com.it.savant.ukm.invent.service.domain.service.export;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.client.export.record.InventoryExportRecordCountClient;
import com.it.savant.ukm.invent.service.da.client.export.record.InventoryExportRecordListClient;
import com.it.savant.ukm.invent.service.da.entity.InventoryExportRecordEntity;
import com.it.savant.ukm.invent.service.domain.model.PageableSortableRequest;
import com.it.savant.ukm.invent.service.domain.model.ResultPage;
import com.it.savant.ukm.invent.service.domain.model.export.InventoryExportRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class InventoryExportListService {

    private final InventoryExportRecordListClient listClient;
    private final InventoryExportRecordCountClient countClient;
    private final Converter<InventoryExportRecordEntity, InventoryExportRecord> converter;

    @Autowired
    public InventoryExportListService(
            final InventoryExportRecordListClient listClient,
            final InventoryExportRecordCountClient countClient,
            final Converter<InventoryExportRecordEntity, InventoryExportRecord> converter
    ) {
        this.listClient = listClient;
        this.countClient = countClient;
        this.converter = converter;
    }

    public ResultPage<InventoryExportRecord> list(final PageableSortableRequest request) {
        return ResultPage.resultOf(
                listClient.list(
                        request.getPageNumber(),
                        request.getPageSize(),
                        request.getSortField(),
                        request.getSortDirection()
                ).stream()
                        .map(converter::convert)
                        .collect(Collectors.toList()),
                countClient.count()
        );
    }
}
