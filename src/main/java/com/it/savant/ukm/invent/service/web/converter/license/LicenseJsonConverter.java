package com.it.savant.ukm.invent.service.web.converter.license;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.license.License;
import com.it.savant.ukm.invent.service.web.json.LicenseJson;
import org.springframework.stereotype.Component;

import static java.time.format.DateTimeFormatter.ofPattern;

@Component
public class LicenseJsonConverter implements Converter<License, LicenseJson> {

    @Override
    public LicenseJson convert(final License source) {
        return new LicenseJson()
                .setClientId(source.getClientId())
                .setLastActivityDateTime(
                        source.getLastActivityDateTime().format(
                                ofPattern("yyyy-MM-dd HH:mm:ss")
                        )
                )
                .setLocation(source.getLocation());
    }
}
