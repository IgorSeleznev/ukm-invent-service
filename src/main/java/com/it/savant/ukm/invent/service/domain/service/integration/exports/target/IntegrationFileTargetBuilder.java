package com.it.savant.ukm.invent.service.domain.service.integration.exports.target;

import com.it.savant.ukm.invent.service.domain.service.integration.IntegrationType;

public interface IntegrationFileTargetBuilder {

    IntegrationFileTarget build();

    IntegrationType getIntegrationType();
}
