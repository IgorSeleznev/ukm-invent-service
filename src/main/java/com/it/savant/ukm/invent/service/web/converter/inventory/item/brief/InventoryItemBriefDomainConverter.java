package com.it.savant.ukm.invent.service.web.converter.inventory.item.brief;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.InventoryItemBrief;
import com.it.savant.ukm.invent.service.web.json.InventoryItemBriefJson;
import org.springframework.stereotype.Component;

@Component
public class InventoryItemBriefDomainConverter implements Converter<InventoryItemBriefJson, InventoryItemBrief> {

    @Override
    public InventoryItemBrief convert(final InventoryItemBriefJson source) {
        return new InventoryItemBrief()
                .setArticle(source.getArticle())
                .setName(source.getName())
                .setQuantity(source.getQuantity())
                .setMeasure(source.getMeasure())
                .setIncreaseStep(source.getIncreaseStep());
    }
}
