package com.it.savant.ukm.invent.service.domain.model.article;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class Article {

    private String article;
    private String accepted;
    private String bornin;
    private String cashload;
    private String country;
    private String cutpricedays;
    private String datasubtype;
    private String datatype;
    private String flags;
    private String globalarticle;
    private String idclass;
    private String idmeasurement;
    private String losses;
    private String mesabbrev;
    private String mesname;
    private String minprofit;
    private String name;
    private String quantitydeviation;
    private String receiptok;
    private String scaleload;
    private String scrap;
    private String shortname;
    private String storage;
    private String usetimedim;
    private String waste;
//    private String rgnid;
//    private String datefrom;
//    private String dateto;
//    private String taxgroupid;
}
