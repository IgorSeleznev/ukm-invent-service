package com.it.savant.ukm.invent.service.da.client.parameter;

import com.it.savant.ukm.invent.service.common.exception.MoreThanOneFoundException;
import com.it.savant.ukm.invent.service.da.entity.ParameterEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;
import static java.util.Optional.empty;
import static java.util.Optional.of;

@Component
public class ParameterFindByIdClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ParameterFindByIdClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Optional<ParameterEntity> findById(final Long id) {
        final List<ParameterEntity> found = jdbcTemplate.query(
                "SELECT id, name, value, type, title, `values`, editable \n" +
                        "FROM " + DATABASE_NAME + ".parameter \n" +
                        "WHERE id = ?",
                (resultSet, i) -> new ParameterEntity()
                        .setId(resultSet.getLong(1))
                        .setName(resultSet.getString(2))
                        .setValue(resultSet.getString(3))
                        .setType(resultSet.getString(4))
                        .setTitle(resultSet.getString(5))
                        .setChooses(resultSet.getString(6))
                        .setEditable(resultSet.getShort(7)),
                id
        );
        if (found.size() == 0) {
            return empty();
        }
        if (found.size() > 1) {
            throw new MoreThanOneFoundException("There are more than parameters with specified id = [" + id + "] found.");
        }
        return of(found.get(0));
    }
}
