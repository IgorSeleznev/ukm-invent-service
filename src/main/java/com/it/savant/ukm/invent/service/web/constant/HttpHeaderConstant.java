package com.it.savant.ukm.invent.service.web.constant;

public interface HttpHeaderConstant {

    String HTTP_HEADER_CLIENT_UID = "INVENT_SERVICE_CLIENT_UID";
    String HTTP_HEADER_SESSION_UID = "INVENT_SERVICE_SESSION_UID";
}
