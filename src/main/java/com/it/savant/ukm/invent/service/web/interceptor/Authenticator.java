package com.it.savant.ukm.invent.service.web.interceptor;

import com.it.savant.ukm.invent.service.da.client.store.StoreIdFindByClientIdClient;
import com.it.savant.ukm.invent.service.domain.model.security.User;
import com.it.savant.ukm.invent.service.domain.service.security.MobileClientSessionTokenHolder;
import com.it.savant.ukm.invent.service.web.security.Authenticated;
import com.it.savant.ukm.invent.service.web.security.AuthenticatedUser;
import org.jlead.assets.security.exception.UnknownSecurityToken;
import org.jlead.assets.security.v2.Authentication;
import org.jlead.assets.security.v2.AuthenticationHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

import static org.apache.commons.codec.digest.DigestUtils.sha1Hex;

@Component
public class Authenticator {

    private final MobileClientSessionTokenHolder tokenHolder;
    private final AuthenticationHolder<User, Boolean> authenticationHolder;
    private final StoreIdFindByClientIdClient storeIdFindByClientIdClient;

    @Autowired
    public Authenticator(
            final MobileClientSessionTokenHolder tokenHolder,
            final AuthenticationHolder<User, Boolean> authenticationHolder,
            final StoreIdFindByClientIdClient storeIdFindByClientIdClient
    ) {
        this.tokenHolder = tokenHolder;
        this.authenticationHolder = authenticationHolder;
        this.storeIdFindByClientIdClient = storeIdFindByClientIdClient;
    }

    public boolean authenticate(final boolean forAdministrator, final String token, final String clientId)
            throws UnknownSecurityToken {
        authenticationHolder.removeCurrent();
        if (!forAdministrator && !authenticationHolder.isRegistered(token)) {
            if (authenticationHolder.isMaxActiveSessions()) {
                final Authentication<User, Boolean> oldest = authenticationHolder.oldest();
                if (new Date().getTime() - oldest.getLastActivityTime().getTime() < 60 * 60 * 1000) {
                    return false;
                }
                try {
                    authenticationHolder.unregister(oldest.getToken());
                    tokenHolder.removeByClientId(clientId);
                } catch (final UnknownSecurityToken unknownSecurityToken) {
                    return false;
                }
            }
            final String actualToken = tokenHolder.getToken(clientId);
            if (actualToken != null && !token.equals(actualToken)) {
                authenticationHolder.unregister(actualToken);
            }
            if (!token.equals(actualToken)) {
                tokenHolder.put(clientId, token);
            }
            authenticationHolder.register(
                    Authenticated.builder()
                            .lastActivityTime(new Date())
                            .token(token)
                            .member(
                                    new AuthenticatedUser(
                                            new User()
                                                    .setAdministrator(false)
                                                    .setLocation(
                                                            storeIdFindByClientIdClient.findByClientId(sha1Hex(clientId))
                                                    )
                                    )
                            )
                            .build()
            );
            authenticationHolder.updateCurrent(token);
            return true;
        }

        if (!forAdministrator) {
            authenticationHolder.updateCurrent(token);
            return true;
        }

        if (authenticationHolder.isAccessible(token, true)) {
            authenticationHolder.updateCurrent(token);
            return true;
        }

        return false;
    }
}
