package com.it.savant.ukm.invent.service.domain.service.license;

import com.it.savant.ukm.invent.service.da.client.license.LicenseResetAllClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class LicenseResetAllService {

    private final LicenseResetAllClient client;

    @Autowired
    public LicenseResetAllService(final LicenseResetAllClient client) {
        this.client = client;
    }

    @Transactional
    public void resetAll() {
        client.resetAll();
    }
}
