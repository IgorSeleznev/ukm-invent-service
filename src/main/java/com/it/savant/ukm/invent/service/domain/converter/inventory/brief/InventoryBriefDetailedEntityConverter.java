package com.it.savant.ukm.invent.service.domain.converter.inventory.brief;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.entity.InventoryItemBriefEntity;
import com.it.savant.ukm.invent.service.da.entity.InventoryBriefDetailedEntity;
import com.it.savant.ukm.invent.service.domain.model.InventoryItemBrief;
import com.it.savant.ukm.invent.service.domain.model.InventoryBriefDetailed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class InventoryBriefDetailedEntityConverter implements Converter<InventoryBriefDetailed, InventoryBriefDetailedEntity> {

    private final Converter<InventoryItemBrief, InventoryItemBriefEntity> converter;

    @Autowired
    public InventoryBriefDetailedEntityConverter(final Converter<InventoryItemBrief, InventoryItemBriefEntity> converter) {
        this.converter = converter;
    }

    @Override
    public InventoryBriefDetailedEntity convert(final InventoryBriefDetailed source) {
        return new InventoryBriefDetailedEntity()
                .setDocId(source.getDocId())
                .setAssortmentName(source.getAssortmentName())
                .setItems(
                        source.getItems().stream()
                                .map(converter::convert)
                                .collect(Collectors.toList())
                );
    }
}
