package com.it.savant.ukm.invent.service.domain.service.parameter;

import com.it.savant.ukm.invent.service.domain.model.PageableSortableRequest;
import com.it.savant.ukm.invent.service.domain.model.parameter.Parameter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
public class ParameterListSorter {

    public List<Parameter> sort(final List<Parameter> parameters, final PageableSortableRequest request) {
        final String fieldName = request.getSortField() == null
                ? "title"
                : request.getSortField().toLowerCase();
        final String direction = request.getSortDirection() == null
                ? "asc"
                : request.getSortDirection().toLowerCase();
        return parameters.stream()
                .sorted(
                        (left, right) -> {
                            if ("title".equals(fieldName)) {
                                return "asc".equals(direction)
                                        ? StringUtils.compare(left.getTitle(), right.getTitle())
                                        : StringUtils.compare(right.getTitle(), left.getTitle());
                            }
                            if ("value".equals(request.getSortField().toLowerCase())) {
                                return "asc".equals(direction)
                                        ? StringUtils.compare(left.getValue(), right.getValue())
                                        : StringUtils.compare(right.getValue(), left.getValue());
                            }
                            return 0;
                        }
                )
                .collect(toList());
    }
}
