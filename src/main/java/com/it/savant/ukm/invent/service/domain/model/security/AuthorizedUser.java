package com.it.savant.ukm.invent.service.domain.model.security;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.jlead.assets.security.SecurityGroup;
import org.jlead.assets.security.SecurityUser;

@Data
@ToString
@Accessors(chain = true)
public class AuthorizedUser implements SecurityUser {

    private final AuthorizedUserSecurityGroup securityGroup;
    private User user;

    public AuthorizedUser(final User user) {
        this.securityGroup = new AuthorizedUserSecurityGroup(user);
        this.user = user;
    }

    @Override
    public String getLogin() {
        return user.getName();
    }

    @Override
    public SecurityGroup getSecurityGroup() {
        return this.securityGroup;
    }
}
