package com.it.savant.ukm.invent.service.da.client.initialize;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class FileImportLogTableCreateClient implements CreateTableClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public FileImportLogTableCreateClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public String getTableName() {
        return "file_import_log";
    }

    public void create() {
        jdbcTemplate.update(
                "CREATE TABLE IF NOT EXISTS " + DATABASE_NAME + ".file_import_log ( \n" +
                        "    record_id BIGINT AUTO_INCREMENT NOT NULL, \n" +
                        "    record_date TIMESTAMP DEFAULT now() NOT NULL, \n" +
                        "    success SMALLINT DEFAULT 1 NOT NULL, \n" +
                        "    error_message TEXT, \n" +
                        "    filename VARCHAR(4192), \n" +
                        "    content TEXT, \n" +
                        "    PRIMARY KEY (record_id) \n" +
                        ") ENGINE = InnoDB DEFAULT CHARSET = UTF8"
        );
    }
}
