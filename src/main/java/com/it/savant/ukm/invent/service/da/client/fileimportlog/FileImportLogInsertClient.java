package com.it.savant.ukm.invent.service.da.client.fileimportlog;

import com.it.savant.ukm.invent.service.da.entity.FileImportLogEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class FileImportLogInsertClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public FileImportLogInsertClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void insert(final FileImportLogEntity fileImportLog) {
        jdbcTemplate.update(
                "INSERT INTO " + DATABASE_NAME + ".file_import_log ( \n" +
                        "   filename, \n" +
                        "   success, \n" +
                        "   error_message, \n" +
                        "   content \n" +
                        ") \n" +
                        "VALUES ( \n" +
                        "   ?, ?, ?, ?" +
                        ")",
                fileImportLog.getFilename(),
                fileImportLog.isSuccess(),
                fileImportLog.getErrorMessage(),
                fileImportLog.getContent()
        );
    }
}
