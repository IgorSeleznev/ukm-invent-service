package com.it.savant.ukm.invent.service.da.entity;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class InventoryExportRecordEntity {

    private String docId;
    private String assortmentName;
    private String confirmDate;
    private Short polled;
}
