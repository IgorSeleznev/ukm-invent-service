package com.it.savant.ukm.invent.service.domain.service.parameter;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.client.parameter.ParameterInsertClient;
import com.it.savant.ukm.invent.service.da.client.parameter.ParameterMaxIdClient;
import com.it.savant.ukm.invent.service.da.entity.ParameterEntity;
import com.it.savant.ukm.invent.service.domain.model.parameter.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ParameterInsertService {

    private final ParameterMaxIdClient maxIdClient;
    private final ParameterInsertClient insertClient;
    private final Converter<Parameter, ParameterEntity> parameterEntityConverter;

    @Autowired
    public ParameterInsertService(final ParameterMaxIdClient maxIdClient,
                                  final ParameterInsertClient insertClient,
                                  final Converter<Parameter, ParameterEntity> parameterEntityConverter) {
        this.maxIdClient = maxIdClient;
        this.insertClient = insertClient;
        this.parameterEntityConverter = parameterEntityConverter;
    }

    public void insert(final Parameter parameter) {
        parameter.setId(
                maxIdClient.maxId() + 1
        );
        insertClient.insert(
                parameterEntityConverter.convert(parameter)
        );
    }
}
