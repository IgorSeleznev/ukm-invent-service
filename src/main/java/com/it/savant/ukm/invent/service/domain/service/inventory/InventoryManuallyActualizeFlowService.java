package com.it.savant.ukm.invent.service.domain.service.inventory;

import com.it.savant.ukm.invent.service.da.client.inventory.item.InventoryManualItemInsertNotExistClient;
import com.it.savant.ukm.invent.service.da.client.inventory.item.InventoryManualItemUnActualByParentDocIdClient;
import com.it.savant.ukm.invent.service.da.client.inventory.item.InventoryManualItemUpdateByParentDocIdClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InventoryManuallyActualizeFlowService {

    private final InventoryManualItemInsertNotExistClient insertNotExistClient;
    private final InventoryManualItemUnActualByParentDocIdClient unActualizeClient;
    private final InventoryManualItemUpdateByParentDocIdClient updateClient;

    @Autowired
    public InventoryManuallyActualizeFlowService(
            final InventoryManualItemInsertNotExistClient insertNotExistClient,
            final InventoryManualItemUnActualByParentDocIdClient unActualizeClient,
            final InventoryManualItemUpdateByParentDocIdClient updateClient
    ) {
        this.insertNotExistClient = insertNotExistClient;
        this.unActualizeClient = unActualizeClient;
        this.updateClient = updateClient;
    }

    public void actualizationFlow(final String parentDocId, final String docId) {
        updateClient.update(parentDocId, docId);
        insertNotExistClient.insertNotExist(docId);
        unActualizeClient.unActualize(docId);
    }
}
