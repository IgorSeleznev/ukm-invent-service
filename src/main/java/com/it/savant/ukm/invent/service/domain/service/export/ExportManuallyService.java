package com.it.savant.ukm.invent.service.domain.service.export;

import com.it.savant.ukm.invent.service.common.exception.NotFoundException;
import com.it.savant.ukm.invent.service.domain.model.Inventory;
import com.it.savant.ukm.invent.service.domain.service.integration.exports.InventoryExportEventAsyncDocIdQueue;
import com.it.savant.ukm.invent.service.domain.service.inventory.InventoryFindByDocIdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ExportManuallyService {

    private final InventoryExportEventAsyncDocIdQueue queue;
    private final InventoryFindByDocIdService findService;

    @Autowired
    public ExportManuallyService(final InventoryExportEventAsyncDocIdQueue queue,
                                 final InventoryFindByDocIdService findService) {
        this.queue = queue;
        this.findService = findService;
    }

    public void export(final String docId) {
        final Optional<Inventory> found = findService.findById(docId);
        if (found.isEmpty()) {
            throw new NotFoundException("Акт инвентаризации с кодом [" + docId + "] не найден.");
        }
        queue.pushAndPersist(docId);
    }
}
