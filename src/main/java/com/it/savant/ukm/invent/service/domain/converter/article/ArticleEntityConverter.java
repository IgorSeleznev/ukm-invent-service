package com.it.savant.ukm.invent.service.domain.converter.article;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.entity.ArticleEntity;
import com.it.savant.ukm.invent.service.domain.model.article.Article;
import org.springframework.stereotype.Component;

@Component
public class ArticleEntityConverter implements Converter<Article, ArticleEntity> {

    @Override
    public ArticleEntity convert(final Article source) {
        return new ArticleEntity()
                .setArticle(source.getArticle())
                .setAccepted(source.getAccepted())
                .setBornin(source.getBornin())
                .setCashload(source.getCashload())
                .setCountry(source.getCountry())
                .setCutpricedays(source.getCutpricedays())
                .setDatasubtype(source.getDatasubtype())
                .setDatatype(source.getDatatype())
                .setFlags(source.getFlags())
                .setGlobalarticle(source.getGlobalarticle())
                .setIdclass(source.getIdclass())
                .setIdmeasurement(source.getIdmeasurement())
                .setLosses(source.getLosses())
                .setMesabbrev(source.getMesabbrev())
                .setMesname(source.getMesname())
                .setMinprofit(source.getMinprofit())
                .setName(source.getName())
                .setQuantitydeviation(source.getQuantitydeviation())
                .setReceiptok(source.getReceiptok())
                .setScaleload(source.getScaleload())
                .setScrap(source.getScrap())
                .setShortname(source.getShortname())
                .setStorage(source.getStorage())
                .setUsetimedim(source.getUsetimedim())
                .setWaste(source.getWaste());
    }
}
