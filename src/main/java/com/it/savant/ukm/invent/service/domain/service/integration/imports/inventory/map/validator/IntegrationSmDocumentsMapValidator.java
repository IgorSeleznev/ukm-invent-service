package com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.map.validator;

import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class IntegrationSmDocumentsMapValidator {

    public boolean isValid(final Map<String, String> map) {
        return map.containsKey("ID")
                && map.containsKey("DOCTYPE")
                && map.containsKey("BORNIN")
                && map.containsKey("COMMENTARY")
                && map.containsKey("CREATEDAT")
                && map.containsKey("CURRENCYMULTORDER")
                && map.containsKey("CURRENCYRATE")
                && map.containsKey("CURRENCYTYPE")
                && map.containsKey("DOCSTATE")
                && map.containsKey("ISROUBLES")
                && map.containsKey("LOCATION")
                && map.containsKey("OPCODE")
                && map.containsKey("PRICEROUNDMODE")
                && map.containsKey("TOTALSUM")
                && map.containsKey("TOTALSUMCUR")
                && map.size() == 15;
    }
}
