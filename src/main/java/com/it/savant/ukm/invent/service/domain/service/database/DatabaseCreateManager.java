package com.it.savant.ukm.invent.service.domain.service.database;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Slf4j
@Component
public class DatabaseCreateManager {

    private final DatabaseExistsService existsService;
    private final DatabaseCreateService databaseCreateService;
    private final TablesCreateService tablesCreateService;

    @Autowired
    public DatabaseCreateManager(final DatabaseExistsService existsService,
                                 final DatabaseCreateService databaseCreateService,
                                 final TablesCreateService tablesCreateService) {
        this.existsService = existsService;
        this.databaseCreateService = databaseCreateService;
        this.tablesCreateService = tablesCreateService;
    }

    public void createIfNotExists() {
        if (!existsService.exists()) {
            log.info("ВНИМАНИЕ! Отсутствует база данных {}. База данных будет создана с нуля.", DATABASE_NAME.toUpperCase());
            databaseCreateService.create();
        }
        tablesCreateService.create();
    }
}
