package com.it.savant.ukm.invent.service.domain.service.integration.imports;

import com.it.savant.ukm.invent.service.domain.service.integration.IntegrationType;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.source.IntegrationDirectorySource;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.source.IntegrationDirectorySourceBuilder;
import com.it.savant.ukm.invent.service.domain.service.parameter.ParameterProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;
import static com.it.savant.ukm.invent.service.domain.constants.ParameterConstants.INTEGRATION_TYPE;

@Component
public class IntegrationDirectorySourceResolver {

    private final ParameterProvider parameterProvider;
    private final Map<IntegrationType, IntegrationDirectorySourceBuilder> sourceBuilders = newHashMap();

    @Autowired
    public IntegrationDirectorySourceResolver(final ParameterProvider parameterProvider,
                                              final List<IntegrationDirectorySourceBuilder> sourceBuilders) {

        this.parameterProvider = parameterProvider;
        sourceBuilders.forEach(
                sourceBuilder -> this.sourceBuilders.put(sourceBuilder.integrationType(), sourceBuilder)
        );

    }

    public IntegrationDirectorySource resolve() {
        final IntegrationType sourceType = IntegrationType.valueOf(
                parameterProvider.read(INTEGRATION_TYPE)
        );
        return sourceBuilders.get(sourceType).source();
    }
}