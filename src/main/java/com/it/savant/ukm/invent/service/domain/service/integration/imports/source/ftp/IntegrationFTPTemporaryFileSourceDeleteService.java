package com.it.savant.ukm.invent.service.domain.service.integration.imports.source.ftp;

import com.it.savant.ukm.invent.service.domain.service.integration.imports.source.IntegrationFileSourceDeleteService;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Slf4j
public class IntegrationFTPTemporaryFileSourceDeleteService implements IntegrationFileSourceDeleteService {

    private final String temporaryPath;
    private final String temporaryFilename;

    public IntegrationFTPTemporaryFileSourceDeleteService(final String temporaryPath, final String temporaryFilename) {
        this.temporaryPath = temporaryPath;
        this.temporaryFilename = temporaryFilename;
    }

    @Override
    public void delete() {
        try {
            Files.deleteIfExists(Paths.get(temporaryPath, temporaryFilename));
        } catch (final IOException exception) {
            log.error("Could not delete temporary file at path [" + temporaryPath + "] with name [" + temporaryFilename + "]", exception);
        }
    }
}
