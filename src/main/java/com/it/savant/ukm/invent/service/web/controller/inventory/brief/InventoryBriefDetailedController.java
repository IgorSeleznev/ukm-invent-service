package com.it.savant.ukm.invent.service.web.controller.inventory.brief;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.InventoryBriefDetailed;
import com.it.savant.ukm.invent.service.domain.service.inventory.brief.InventoryBriefDetailedService;
import com.it.savant.ukm.invent.service.web.json.DocIdRequestJson;
import com.it.savant.ukm.invent.service.web.json.InventoryBriefDetailedJson;
import com.it.savant.ukm.invent.service.web.json.Response;
import com.it.savant.ukm.invent.service.web.security.Authorized;
import com.it.savant.ukm.invent.service.web.security.Licensed;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import static com.it.savant.ukm.invent.service.web.constant.HttpHeaderConstant.HTTP_HEADER_CLIENT_UID;
import static com.it.savant.ukm.invent.service.web.json.Response.success;

@Slf4j
@RestController
public class InventoryBriefDetailedController {

    private final InventoryBriefDetailedService service;
    private final Converter<InventoryBriefDetailed, InventoryBriefDetailedJson> converter;

    @Autowired
    public InventoryBriefDetailedController(final InventoryBriefDetailedService service,
                                            final Converter<InventoryBriefDetailed, InventoryBriefDetailedJson> converter) {
        this.service = service;
        this.converter = converter;
    }

    @Licensed
    @Authorized
    @PostMapping("/inventory/detailed")
    public Response<InventoryBriefDetailedJson> classifierDetailed(
            @RequestBody final DocIdRequestJson request,
            final HttpServletRequest httpServletRequest
    ) {
        log.info(
                "Принят запрос на получение детализированного акта инвентаризации от клиента с идентификатором '{}'",
                httpServletRequest.getHeader(HTTP_HEADER_CLIENT_UID)
        );
        return success(
                converter.convert(
                        service.inventoryBriefDetailed(request.getDocId())
                )
        );
    }
}
