package com.it.savant.ukm.invent.service.da.client.license;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;
import static java.time.ZoneId.systemDefault;
import static java.util.Date.from;

@Component
public class LicenseActivityDateTimeClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public LicenseActivityDateTimeClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void update(final String clientId, final LocalDateTime activityDateTime) {
        jdbcTemplate.update(
                "UPDATE " + DATABASE_NAME + ".client_license \n" +
                        "SET last_activity_date_time = ? \n" +
                        "WHERE client_id = ?",
                from(
                        activityDateTime.atZone(systemDefault()).toInstant()
                ),
                clientId
        );
    }
}
