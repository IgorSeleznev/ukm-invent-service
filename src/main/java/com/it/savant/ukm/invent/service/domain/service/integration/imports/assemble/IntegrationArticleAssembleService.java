package com.it.savant.ukm.invent.service.domain.service.integration.imports.assemble;

import com.it.savant.ukm.invent.service.domain.model.article.Article;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.parser.XmlNode;
import org.springframework.stereotype.Service;

@Service
public class IntegrationArticleAssembleService implements IntegrationAssembleService<Article> {

    @Override
    public String getAssembleName() {
        return "ARTICLE";
    }

    @Override
    public Article assemble(final XmlNode xmlNode) {
        final Article article = new Article();

        xmlNode.getNodes().get(0)
                .getNodes().get(0)
                .getNodes()
                .forEach(
                        node -> {
                            if ("SMCARD".equals(node.getName())) {
                                article
                                        .setArticle(node.getValues().get("ARTICLE"))
                                        .setAccepted(node.getValues().get("ACCEPTED"))
                                        .setBornin(node.getValues().get("BORNIN"))
                                        .setCashload(node.getValues().get("CASHLOAD"))
                                        .setCountry(node.getValues().get("COUNTRY"))
                                        .setCutpricedays(node.getValues().get("CUTPRICEDAYS"))
                                        .setDatasubtype(node.getValues().get("DATASUBTYPE"))
                                        .setDatatype(node.getValues().get("DATATYPE"))
                                        .setFlags(node.getValues().get("FLAGS"))
                                        .setGlobalarticle(node.getValues().get("GLOBALARTICLE"))
                                        .setIdclass(node.getValues().get("IDCLASS"))
                                        .setIdmeasurement(node.getValues().get("IDMEASUREMENT"))
                                        .setLosses(node.getValues().get("LOSSES"))
                                        .setMesabbrev(node.getValues().get("MESABBREV"))
                                        .setMesname(node.getValues().get("MESNAME"))
                                        .setMinprofit(node.getValues().get("MINPROFIT"))
                                        .setName(node.getValues().get("NAME"))
                                        .setQuantitydeviation(node.getValues().get("QUANTITYDEVIATION"))
                                        .setReceiptok(node.getValues().get("RECEIPTOK"))
                                        .setScaleload(node.getValues().get("SCALELOAD"))
                                        .setScrap(node.getValues().get("SCRAP"))
                                        .setShortname(node.getValues().get("SHORTNAME"))
                                        .setStorage(node.getValues().get("STORAGE"))
                                        .setUsetimedim(node.getValues().get("USETIMEDIM"))
                                        .setWaste(node.getValues().get("WASTE"));
                            }
//                            if ("SMCARDTAX".equals(node.getName())) {
//                                article
//                                        .setRgnid(node.getValues().get("RGNID"))
//                                        .setDatefrom(node.getValues().get("DATEFROM"))
//                                        .setDateto(node.getValues().get("DATETO"))
//                                        .setTaxgroupid(node.getValues().get("TAXGROUPID"));
//                            }
                        }
                );

        return article;
    }
}
