package com.it.savant.ukm.invent.service.domain.service.integration.imports.store;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StoreXmlImportService {

    private final IntegrationStoreParseService parseService;
    private final IntegrationStoreSaveService saveService;

    @Autowired
    public StoreXmlImportService(final IntegrationStoreParseService parseService,
                                 final IntegrationStoreSaveService saveService) {
        this.parseService = parseService;
        this.saveService = saveService;
    }

    public void xmlImport(final String xml) {
        saveService.save(
                parseService.parse(xml)
        );
    }
}
