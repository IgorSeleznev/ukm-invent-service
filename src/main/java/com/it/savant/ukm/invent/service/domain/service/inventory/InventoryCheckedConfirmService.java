package com.it.savant.ukm.invent.service.domain.service.inventory;

import com.it.savant.ukm.invent.service.common.exception.UkmInventServiceBusinessException;
import com.it.savant.ukm.invent.service.da.client.inventory.InventoryExistsByDocIdByNotConfirmedClient;
import com.it.savant.ukm.invent.service.da.client.inventory.InventoryExistsByDocIdByNotConfirmedWithPreambleDateClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import static com.it.savant.ukm.invent.service.domain.service.inventory.InventoryConfirmAndCreateService.INVENTORY_CONFIRM_AND_CREATE_SERVICE;

@Primary
@Service
public class InventoryCheckedConfirmService implements InventoryConfirmService {

    private final InventoryExistsByDocIdByNotConfirmedClient existsClient;
    private final InventoryExistsByDocIdByNotConfirmedWithPreambleDateClient preambleDateExistsClient;
    private final InventoryConfirmService confirmService;

    @Autowired
    public InventoryCheckedConfirmService(
            final InventoryExistsByDocIdByNotConfirmedClient existsClient,
            final InventoryExistsByDocIdByNotConfirmedWithPreambleDateClient preambleDateExistsClient,
            @Qualifier(INVENTORY_CONFIRM_AND_CREATE_SERVICE) final InventoryConfirmService confirmService
    ) {
        this.existsClient = existsClient;
        this.preambleDateExistsClient = preambleDateExistsClient;
        this.confirmService = confirmService;
    }

    public void confirm(final String docId, final String clientId) {
        if (!existsClient.exists(docId)) {
            throw new UkmInventServiceBusinessException(
                    "Указанный акт инвентаризации не найден или уже был подтвержден"
            );
        }
        if (!preambleDateExistsClient.exists(docId)) {
            throw new UkmInventServiceBusinessException(
                    "Количество не было указано ни для одного товара. "
            );
        }
        confirmService.confirm(docId, clientId);
    }
}
