package com.it.savant.ukm.invent.service.domain.service.integration.exports;

import com.it.savant.ukm.invent.service.domain.service.scheduled.ScheduledService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TriggerContext;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ExportScheduledService implements ScheduledService {

    private final ExportNextExecutionTimeService nextExecutionTimeService;
    private final ExportManager exportManager;

    @Autowired
    public ExportScheduledService(final ExportNextExecutionTimeService nextExecutionTimeService,
                                  final ExportManager exportManager) {
        this.nextExecutionTimeService = nextExecutionTimeService;
        this.exportManager = exportManager;
    }

    @Override
    public void execute() {
        exportManager.doExport();
    }

    @Override
    public Date nextExecutionTime(final TriggerContext triggerContext) {
        return nextExecutionTimeService.nextExecutionTime(triggerContext);
    }
}
