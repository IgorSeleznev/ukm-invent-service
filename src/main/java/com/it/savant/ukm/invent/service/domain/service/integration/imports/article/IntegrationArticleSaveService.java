package com.it.savant.ukm.invent.service.domain.service.integration.imports.article;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.client.article.ArticleReplaceClient;
import com.it.savant.ukm.invent.service.da.entity.ArticleEntity;
import com.it.savant.ukm.invent.service.domain.model.article.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IntegrationArticleSaveService {

    private final ArticleReplaceClient replaceClient;
    private final Converter<Article, ArticleEntity> converter;

    @Autowired
    public IntegrationArticleSaveService(final ArticleReplaceClient replaceClient,
                                         final Converter<Article, ArticleEntity> converter) {
        this.replaceClient = replaceClient;
        this.converter = converter;
    }

    public void save(final Article article) {
        replaceClient.replace(
                converter.convert(article)
        );
    }
}
