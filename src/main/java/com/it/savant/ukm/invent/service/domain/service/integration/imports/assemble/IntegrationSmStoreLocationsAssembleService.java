package com.it.savant.ukm.invent.service.domain.service.integration.imports.assemble;

import com.it.savant.ukm.invent.service.domain.model.smstorelocations.SmStoreLocations;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.parser.XmlNode;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Component
public class IntegrationSmStoreLocationsAssembleService implements IntegrationAssembleService<List<SmStoreLocations>> {

    @Override
    public String getAssembleName() {
        return "SmStoreLocations";
    }

    @Override
    public List<SmStoreLocations> assemble(final XmlNode xmlNode) {
        final List<SmStoreLocations> result = newArrayList();

        xmlNode.getNodes()
                .forEach(
                        node -> {
                            if ("SmStoreLocations".equals(node.getName())) {
                                result.add(
                                        new SmStoreLocations()
                                                .setId(node.getValues().get("ID"))
                                                .setName(node.getValues().get("NAME"))
                                );
                            }
                        }
                );

        return result;
    }
}
