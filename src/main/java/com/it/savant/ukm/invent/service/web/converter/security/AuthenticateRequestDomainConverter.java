package com.it.savant.ukm.invent.service.web.converter.security;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.security.AuthenticateRequest;
import com.it.savant.ukm.invent.service.web.json.security.AuthenticateRequestJson;
import org.springframework.stereotype.Component;

@Component
public class AuthenticateRequestDomainConverter implements Converter<AuthenticateRequestJson, AuthenticateRequest> {

    @Override
    public AuthenticateRequest convert(final AuthenticateRequestJson source) {
        return new AuthenticateRequest()
                .setId(source.getId())
                .setPassword(source.getPassword());
    }
}
