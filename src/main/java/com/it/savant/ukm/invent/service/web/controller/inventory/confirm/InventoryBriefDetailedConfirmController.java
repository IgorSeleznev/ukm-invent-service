package com.it.savant.ukm.invent.service.web.controller.inventory.confirm;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.InventoryBriefDetailed;
import com.it.savant.ukm.invent.service.domain.service.inventory.brief.InventoryBriefDetailedConfirmService;
import com.it.savant.ukm.invent.service.web.json.InventoryBriefDetailedJson;
import com.it.savant.ukm.invent.service.web.json.Response;
import com.it.savant.ukm.invent.service.web.security.Authorized;
import com.it.savant.ukm.invent.service.web.security.Licensed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static com.it.savant.ukm.invent.service.web.json.Response.success;

@RestController
public class InventoryBriefDetailedConfirmController {

    private final InventoryBriefDetailedConfirmService service;
    private final Converter<InventoryBriefDetailedJson, InventoryBriefDetailed> converter;

    @Autowired
    public InventoryBriefDetailedConfirmController(final InventoryBriefDetailedConfirmService service,
                                                   final Converter<InventoryBriefDetailedJson, InventoryBriefDetailed> converter
    ) {
        this.service = service;
        this.converter = converter;
    }

    @Licensed
    @Authorized
    @PostMapping("/inventory/detailed/confirm")
    public Response<Void> confirm(@RequestBody final InventoryBriefDetailedJson classifierDetailed) {
        service.confirm(
                converter.convert(classifierDetailed)
        );
        return success();
    }
}
