package com.it.savant.ukm.invent.service.domain.service.security;

import com.it.savant.ukm.invent.service.domain.model.security.AuthenticateRequest;
import com.it.savant.ukm.invent.service.domain.model.security.AuthenticateResponse;
import com.it.savant.ukm.invent.service.domain.model.security.User;
import com.it.savant.ukm.invent.service.domain.service.user.UserFindByIdByPasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.it.savant.ukm.invent.service.domain.model.security.AuthenticateResponse.failed;

@Service
public class AuthorizedUserAuthenticateService {

    private final AuthorizationUserRegisterService registerService;
    private final UserFindByIdByPasswordService userFindService;

    @Autowired
    public AuthorizedUserAuthenticateService(final AuthorizationUserRegisterService registerService,
                                             final UserFindByIdByPasswordService userFindService) {
        this.registerService = registerService;
        this.userFindService = userFindService;
    }

    public AuthenticateResponse authorize(final AuthenticateRequest request, final String clientUid) {
        final Optional<User> user = userFindService.findByIdByPassword(request.getId(), request.getPassword());
        if (user.isEmpty()) {
            return failed();
        }
        return new AuthenticateResponse()
                .setToken(
                        registerService.register(user.get(), clientUid).getToken()
                );
    }

}
