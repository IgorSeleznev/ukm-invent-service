package com.it.savant.ukm.invent.service.domain.model;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class InventoryItemBriefQuantityUpdateRequest {

    private String docId;
    private String article;
    private String quantity;
}
