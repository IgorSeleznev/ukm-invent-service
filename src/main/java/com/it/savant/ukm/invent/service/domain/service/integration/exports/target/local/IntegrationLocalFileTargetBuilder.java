package com.it.savant.ukm.invent.service.domain.service.integration.exports.target.local;

import com.it.savant.ukm.invent.service.domain.service.integration.IntegrationType;
import com.it.savant.ukm.invent.service.domain.service.integration.exports.target.IntegrationFileTarget;
import com.it.savant.ukm.invent.service.domain.service.integration.exports.target.IntegrationFileTargetBuilder;
import com.it.savant.ukm.invent.service.domain.service.parameter.ParameterProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.domain.constants.ParameterConstants.INTEGRATION_TARGET_LOCAL_PATH;
import static com.it.savant.ukm.invent.service.domain.constants.ParameterConstants.INTEGRATION_TARGET_LOCAL_TEMPORARY_PATH;
import static com.it.savant.ukm.invent.service.domain.service.integration.IntegrationType.LOCAL;

@Component
public class IntegrationLocalFileTargetBuilder implements IntegrationFileTargetBuilder {

    private final ParameterProvider parameterProvider;

    @Autowired
    public IntegrationLocalFileTargetBuilder(final ParameterProvider parameterProvider) {
        this.parameterProvider = parameterProvider;
    }

    @Override
    public IntegrationFileTarget build() {
        return new IntegrationLocalFileTarget(
                parameterProvider.read(INTEGRATION_TARGET_LOCAL_PATH),
                parameterProvider.read(INTEGRATION_TARGET_LOCAL_TEMPORARY_PATH)
        );
    }

    @Override
    public IntegrationType getIntegrationType() {
        return LOCAL;
    }
}
