package com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.map.resolver;

import com.it.savant.ukm.invent.service.common.exception.UkmInventServiceBusinessException;
import com.it.savant.ukm.invent.service.domain.model.integration.IntegrationInventoryPackage;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.map.validator.IntegrationSmDocumentsMapValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class IntegrationSmDocumentsMapResolver {

    private final IntegrationSmDocumentsMapValidator validator;

    @Autowired
    public IntegrationSmDocumentsMapResolver(final IntegrationSmDocumentsMapValidator validator) {
        this.validator = validator;
    }

    public Map<String, String> resolve(final IntegrationInventoryPackage source) {
        for (final Map<String, String> map : source.getPostObject().getIl()) {
            if (validator.isValid(map)) {
                return map;
            }
        }
        throw new UkmInventServiceBusinessException("Element SMDOCUMENTS not found");
    }
}
