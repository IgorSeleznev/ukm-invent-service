package com.it.savant.ukm.invent.service.da.client.initialize;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class UserTableCreateClient implements CreateTableClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public UserTableCreateClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public String getTableName() {
        return "user";
    }

    public void create() {
        jdbcTemplate.update(
                "CREATE TABLE IF NOT EXISTS " + DATABASE_NAME + ".user ( \n" +
                        "   id BIGINT AUTO_INCREMENT NOT NULL, \n" +
                        "   name VARCHAR(100) NOT NULL, \n" +
                        "   password_hash VARCHAR(255), \n" +
                        "   location VARCHAR(11) NOT NULL, " +
                        "   administrator SMALLINT DEFAULT 0 NOT NULL, \n" +
                        "   administrator_login VARCHAR(100), \n" +
                        "   deleted SMALLINT(1) DEFAULT 0 NOT NULL, \n" +
                        "   PRIMARY KEY (id) \n" +
                        ") Engine = InnoDB"
        );
    }
}
