package com.it.savant.ukm.invent.service.domain.service.integration.imports.source.ftp;

import com.it.savant.ukm.invent.service.common.exception.UkmInventServiceSystemException;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.source.IntegrationFileSourceDeleteService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IntegrationFTPOriginalFileSourceDeleteService implements IntegrationFileSourceDeleteService {

    private final IntegrationFTPFileSourceConfiguration configuration;

    public IntegrationFTPOriginalFileSourceDeleteService(final IntegrationFTPFileSourceConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public void delete() {
        try {
            configuration.getClient().changeWorkingDirectory(configuration.getRemotePath());
            boolean success = configuration.getClient().deleteFile(
                    configuration.getRemoteFilename()
            );
            if (!success) {
                throw new UkmInventServiceSystemException("Delete file in FTP source has got result false as success flag");
            }
        } catch (final Exception exception) {
            log.error("Could not delete file in FTP source with name [" + configuration.getRemoteFilename() + "]", exception);
        }
    }
}
