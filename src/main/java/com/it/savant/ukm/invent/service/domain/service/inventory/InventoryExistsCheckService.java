package com.it.savant.ukm.invent.service.domain.service.inventory;

import com.it.savant.ukm.invent.service.da.client.inventory.InventoryExistsClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InventoryExistsCheckService {

    private final InventoryExistsClient client;

    @Autowired
    public InventoryExistsCheckService(final InventoryExistsClient client) {
        this.client = client;
    }

    public boolean checkExists(final String docId) {
        return client.exists(docId);
    }
}
