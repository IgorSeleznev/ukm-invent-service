package com.it.savant.ukm.invent.service.da.client.license;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class LicenseLocationLinkClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public LicenseLocationLinkClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void link(final String location, final String clientId) {
        jdbcTemplate.update(
                "UPDATE " + DATABASE_NAME + ".client_license \n" +
                        "SET location = ? \n" +
                        "WHERE client_id = ?",
                location,
                clientId
        );
    }
}
