package com.it.savant.ukm.invent.service.domain.service.integration.imports.source;

public interface IntegrationFileSourceDeleteService {

    void delete();
}
