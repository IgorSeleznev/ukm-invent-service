package com.it.savant.ukm.invent.service.web.controller.admin.parameter;

import com.google.j2objc.annotations.AutoreleasePool;
import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.parameter.Parameter;
import com.it.savant.ukm.invent.service.domain.service.parameter.ParameterFindOneByIdService;
import com.it.savant.ukm.invent.service.web.json.ParameterJson;
import com.it.savant.ukm.invent.service.web.security.Authorized;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class ParameterFindOneByIdController {

    private final ParameterFindOneByIdService service;
    private final Converter<Parameter, ParameterJson> converter;

    @Autowired
    public ParameterFindOneByIdController(final ParameterFindOneByIdService service,
                                          final Converter<Parameter, ParameterJson> converter) {
        this.service = service;
        this.converter = converter;
    }

    @Authorized(administratorOnly = true)
    @GetMapping("/admin/parameter/find-one/{id}")
    public ParameterJson findOneById(@PathVariable("id") final Long id) {
        log.info("Принят запрос на получение детализированного параметра");
        return converter.convert(
                service.findOneById(id)
        );
    }
}
