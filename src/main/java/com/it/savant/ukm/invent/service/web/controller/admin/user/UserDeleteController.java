package com.it.savant.ukm.invent.service.web.controller.admin.user;

import com.it.savant.ukm.invent.service.domain.service.user.UserDeleteService;
import com.it.savant.ukm.invent.service.web.security.Authorized;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class UserDeleteController {

    private final UserDeleteService service;

    @Autowired
    public UserDeleteController(final UserDeleteService service) {
        this.service = service;
    }

    @Authorized(administratorOnly = true)
    @PostMapping("/admin/user/delete/{id}")
    public void delete(@PathVariable("id") final Long id) {
        log.info("Принят запрос на удаление пользователя с идентификатором {}", id);
        service.delete(id);
    }
}
