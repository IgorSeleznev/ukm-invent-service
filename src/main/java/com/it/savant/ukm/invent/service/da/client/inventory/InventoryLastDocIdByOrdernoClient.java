package com.it.savant.ukm.invent.service.da.client.inventory;

import com.it.savant.ukm.invent.service.common.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryLastDocIdByOrdernoClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryLastDocIdByOrdernoClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public String lastDocId(final String orderno, final String location) {
        final List<String> result = jdbcTemplate.query(
                "SELECT i.id\n" +
                        "FROM " + DATABASE_NAME + ".inventory i,\n" +
                        "     (\n" +
                        "    SELECT max(i2.record_date) as record_date\n" +
                        "    FROM " + DATABASE_NAME + ".inventory i2\n" +
                        "    WHERE created_manually = 0 AND i2.orderno = ? AND i2.location = ?\n" +
                        ") rd\n" +
                        "WHERE i.record_date = rd.record_date\n" +
                        "  AND i.created_manually = 0 AND i.orderno = ? AND i.location = ?;\n",
                (resultSet, i) -> resultSet.getString(1),
                orderno,
                location,
                orderno,
                location
        );
        if (result.size() == 0 || result.get(0) == null) {
            throw new NotFoundException("inventory has no documents for orderno = [" + orderno + "]");
        }
        return result.get(0);
    }
}
