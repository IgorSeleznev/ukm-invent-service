package com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.parser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.it.savant.ukm.invent.service.common.exception.UkmInventServiceBusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class XmlParser {

    private final XmlMapper xmlMapper;

    @Autowired
    public XmlParser(final XmlMapper xmlMapper) {
        this.xmlMapper = xmlMapper;
    }

    public XmlNode parse(final String xml) {
        try {
            final JsonParser parser = xmlMapper.createParser(xml);
            String currentName = null;
            final XmlNode rootNode = new XmlNode();
            XmlNode currentNode = rootNode;
            while (!parser.isClosed()) {
                JsonToken jsonToken = parser.nextToken();
                if (jsonToken == null) {
                    parser.close();
                    continue;
                }
                if (JsonToken.FIELD_NAME.equals(jsonToken)) {
                    currentName = parser.getCurrentName();
                }
                if (JsonToken.START_OBJECT.equals(jsonToken)) {
                    final XmlNode childNode = new XmlNode()
                            .setName(currentName)
                            .setParent(currentNode);
                    currentNode.getNodes().add(childNode);
                    currentNode = childNode;
                    currentNode.setName(currentName);
                }
                if (JsonToken.END_OBJECT.equals(jsonToken)) {
                    currentNode = currentNode.getParent();
                    currentName = currentNode.getName();
                }
                if (JsonToken.VALUE_STRING.equals(jsonToken)
                        || JsonToken.VALUE_FALSE.equals(jsonToken)
                        || JsonToken.VALUE_TRUE.equals(jsonToken)
                        || JsonToken.VALUE_NULL.equals(jsonToken)
                        || JsonToken.VALUE_NUMBER_FLOAT.equals(jsonToken)
                        || JsonToken.VALUE_NUMBER_INT.equals(jsonToken)
                        || JsonToken.VALUE_EMBEDDED_OBJECT.equals(jsonToken)
                ) {
                    if (JsonToken.VALUE_STRING.equals(jsonToken)) {
                        currentNode.getValues().put(currentName, parser.getValueAsString());
                    }
                    if (JsonToken.VALUE_FALSE.equals(jsonToken)) {
                        currentNode.getValues().put(currentName, parser.getValueAsString());
                    }
                    if (JsonToken.VALUE_TRUE.equals(jsonToken)) {
                        currentNode.getValues().put(currentName, parser.getValueAsString());
                    }
                    if (JsonToken.VALUE_NULL.equals(jsonToken)) {
                        currentNode.getValues().put(currentName, parser.getValueAsString());
                    }
                    if (JsonToken.VALUE_NUMBER_INT.equals(jsonToken)) {
                        currentNode.getValues().put(currentName, parser.getValueAsString());
                    }
                    if (JsonToken.VALUE_NUMBER_FLOAT.equals(jsonToken)) {
                        currentNode.getValues().put(currentName, parser.getValueAsString());
                    }
                }
            }
            return rootNode.getNodes().get(0);
        } catch (final IOException exception) {
            throw new UkmInventServiceBusinessException(exception);
        }
    }
}
