package com.it.savant.ukm.invent.service.domain.service.inventory;

import com.it.savant.ukm.invent.service.domain.model.Inventory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InventorySaveService {

    private final InventoryInsertService inventoryInsertService;
    private final InventoryItemInsertService itemInsertService;

    @Autowired
    public InventorySaveService(final InventoryInsertService inventoryInsertService,
                                final InventoryItemInsertService itemInsertService) {
        this.inventoryInsertService = inventoryInsertService;
        this.itemInsertService = itemInsertService;
    }

    public void save(final Inventory inventory) {
        inventoryInsertService.insert(inventory);
        inventory.getItems().forEach(
                itemInsertService::insert
        );
    }
}
