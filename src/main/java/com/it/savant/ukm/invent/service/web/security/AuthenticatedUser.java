package com.it.savant.ukm.invent.service.web.security;

import com.it.savant.ukm.invent.service.domain.model.security.User;
import lombok.ToString;
import org.jlead.assets.security.v2.AuthenticationMember;

@ToString
public class AuthenticatedUser implements AuthenticationMember<User, Boolean> {

    private User user;

    public AuthenticatedUser(final User user) {
        this.user = user;
    }

    public User getIdentification() {
        return user;
    }

    public AuthenticationMember<User, Boolean> setIdentification(final User identification) {
        this.user = identification;
        return null;
    }

    @Override
    public boolean isEqualTo(final User user) {
        return this.user.getId().equals(user.getId());
    }

    @Override
    public boolean isGranted(final Boolean isAdministrator) {
        return user.getAdministrator().equals(isAdministrator);
    }
}
