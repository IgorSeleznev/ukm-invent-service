package com.it.savant.ukm.invent.service.domain.service.integration.exports;

import com.it.savant.ukm.invent.service.common.exception.NotFoundException;
import com.it.savant.ukm.invent.service.common.value.ValueHolder;
import com.it.savant.ukm.invent.service.domain.service.inventory.InventoryFindByDocIdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static com.it.savant.ukm.invent.service.common.value.ValueHolder.hold;

@Component
public class ExportManager {

    private final InventoryExportEventAsyncDocIdQueue queue;
    private final InventoryFindByDocIdService findService;
    private final IntegrationInventoryExportService exportService;

    @Autowired
    public ExportManager(
            final InventoryExportEventAsyncDocIdQueue queue,
            final InventoryFindByDocIdService findService,
            final IntegrationInventoryExportService exportService
    ) {
        this.queue = queue;
        this.findService = findService;
        this.exportService = exportService;
    }

    public void doExport() {
        final ValueHolder<Optional<String>> docId = hold(queue.poll());
        while (docId.getValue().isPresent()) {
            findService
                    .findById(docId.getValue().get())
                    .ifPresentOrElse(
                            exportService::doExport,
                            () -> {
                                throw new NotFoundException(
                                        "В процессе экспорта не найден акт инвентаризации с идентификатором '" + docId.getValue() + "'"
                                );
                            }
                    );
            docId.setValue(queue.poll());
        }
    }
}
