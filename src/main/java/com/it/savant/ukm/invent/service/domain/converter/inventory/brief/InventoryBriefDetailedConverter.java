package com.it.savant.ukm.invent.service.domain.converter.inventory.brief;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.entity.InventoryItemBriefEntity;
import com.it.savant.ukm.invent.service.da.entity.InventoryBriefDetailedEntity;
import com.it.savant.ukm.invent.service.domain.model.InventoryItemBrief;
import com.it.savant.ukm.invent.service.domain.model.InventoryBriefDetailed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class InventoryBriefDetailedConverter implements Converter<InventoryBriefDetailedEntity, InventoryBriefDetailed> {

    private final Converter<InventoryItemBriefEntity, InventoryItemBrief> converter;

    @Autowired
    public InventoryBriefDetailedConverter(final Converter<InventoryItemBriefEntity, InventoryItemBrief> converter) {
        this.converter = converter;
    }

    @Override
    public InventoryBriefDetailed convert(final InventoryBriefDetailedEntity source) {
        return new InventoryBriefDetailed()
                .setDocId(source.getDocId())
                .setAssortmentName(source.getAssortmentName())
                .setItems(
                        source.getItems().stream()
                                .map(converter::convert)
                                .collect(Collectors.toList())
                );
    }
}