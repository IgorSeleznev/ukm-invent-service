package com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.map.resolver;

import com.it.savant.ukm.invent.service.domain.model.integration.IntegrationInventoryPackage;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.map.validator.IntegrationSmSpecIlMapValidator;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.map.validator.IntegrationSmSpecMapValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;

@Component
public class IntegrationSmSpecMapResolver {

    private final IntegrationSmSpecMapValidator smSpecMapValidator;
    private final IntegrationSmSpecIlMapValidator smSpecIlMapValidator;

    @Autowired
    public IntegrationSmSpecMapResolver(final IntegrationSmSpecMapValidator smSpecMapValidator,
                                        final IntegrationSmSpecIlMapValidator smSpecIlMapValidator
    ) {
        this.smSpecMapValidator = smSpecMapValidator;
        this.smSpecIlMapValidator = smSpecIlMapValidator;
    }

    public List<Map<String, String>> resolve(final IntegrationInventoryPackage source) {
        final Map<String, Map<String, String>> result = newHashMap();
        for (final Map<String, String> map : source.getPostObject().getIl()) {
            if (smSpecMapValidator.isValid(map) || smSpecIlMapValidator.isValid(map)) {
                if (!result.containsKey(map.get("SPECITEM"))) {
                    result.put(map.get("SPECITEM"), newHashMap());
                }
                result.get(map.get("SPECITEM")).putAll(map);
            }
        }
        return new ArrayList<>(result.values());
    }
}
