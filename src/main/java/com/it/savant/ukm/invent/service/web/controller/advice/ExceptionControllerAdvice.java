package com.it.savant.ukm.invent.service.web.controller.advice;

import com.it.savant.ukm.invent.service.common.exception.UkmInventServiceBusinessException;
import com.it.savant.ukm.invent.service.common.exception.WrongPasswordException;
import com.it.savant.ukm.invent.service.web.json.Response;
import org.jlead.assets.common.utils.StackTracePrinter;
import org.jlead.assets.security.exception.AuthorizationException;
import org.jlead.assets.security.exception.ForbiddenException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.it.savant.ukm.invent.service.web.json.Response.failed;

@RestControllerAdvice
public class ExceptionControllerAdvice {
    private final static Logger LOGGER = LoggerFactory.getLogger(ExceptionControllerAdvice.class);

    @ResponseStatus(code = HttpStatus.OK)
    @ExceptionHandler(UkmInventServiceBusinessException.class)
    @ResponseBody
    public Response<Void> handleException(HttpServletRequest request,
                                          HttpServletResponse response,
                                          UkmInventServiceBusinessException exception) {
        LOGGER.error("EXCEPTION [BUSINESS] has occured: \nEXCEPTION STACKTRACE: {}", stackTrace(exception));
        response.setHeader("Content-Type", "text/plain;charset=UTF-8");
        return failed(exception.getMessage());
    }

    @ResponseStatus(code = HttpStatus.OK)
    @ExceptionHandler(WrongPasswordException.class)
    @ResponseBody
    public Response<Void> handleException(HttpServletRequest request,
                                          HttpServletResponse response,
                                          AuthorizationException exception) {
        LOGGER.error("EXCEPTION [UNAUTHORIZED] has occured: \nEXCEPTION STACKTRACE: {}", stackTrace(exception));
        return failed("Неверный пароль");
    }

    @ResponseStatus(code = HttpStatus.OK)
    @ExceptionHandler(ForbiddenException.class)
    @ResponseBody
    public Response<Void> handleException(HttpServletRequest request,
                                          HttpServletResponse response,
                                          ForbiddenException exception) {
        LOGGER.error("EXCEPTION [FORBIDDEN] has occured: \nEXCEPTION STACKTRACE: {}", stackTrace(exception));
        return failed("Операция запрещена");
    }

    @ResponseStatus(code = HttpStatus.OK)
    @ExceptionHandler({Exception.class})
    @ResponseBody
    public Response<Void> handleException(HttpServletRequest request,
                                          HttpServletResponse response,
                                          Exception exception) {
        response.setHeader("Content-Type", "text/plain;charset=UTF-8");
        LOGGER.error("EXCEPTION [{}] has occured: \nEXCEPTION STACKTRACE: {}",
                exception.getMessage(),
                stackTrace(exception));
        return failed("Произошла ошибка на сервере");
    }

    @ResponseStatus(code = HttpStatus.OK)
    @ExceptionHandler({RuntimeException.class})
    @ResponseBody
    public Response<Void> handleException(HttpServletRequest request,
                                          HttpServletResponse response,
                                          RuntimeException exception) {
        response.setHeader("Content-Type", "text/plain;charset=UTF-8");
        LOGGER.error("EXCEPTION [{}] has occured: \nEXCEPTION STACKTRACE: {}",
                exception.getMessage(),
                stackTrace(exception));
        return failed("Произошла ошибка на сервере");
    }

    private String stackTrace(Throwable exception) {
        return StackTracePrinter.print(exception);
    }
}
