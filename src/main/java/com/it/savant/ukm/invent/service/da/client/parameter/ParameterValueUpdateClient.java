package com.it.savant.ukm.invent.service.da.client.parameter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class ParameterValueUpdateClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ParameterValueUpdateClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void update(final String parameterName, final String value) {
        jdbcTemplate.update(
                "UPDATE " + DATABASE_NAME + ".parameter \n" +
                        "SET value = ? \n" +
                        "WHERE name = ?",
                value,
                parameterName
        );
    }

}
