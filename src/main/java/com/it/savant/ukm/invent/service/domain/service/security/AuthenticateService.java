package com.it.savant.ukm.invent.service.domain.service.security;

import com.it.savant.ukm.invent.service.domain.model.security.AuthenticateRequest;
import com.it.savant.ukm.invent.service.domain.model.security.AuthenticateResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AuthenticateService {

    private final AnonymousSessionCreateService anonymousCreateService;
    private final AuthorizedUserAuthenticateService authenticateService;

    @Autowired
    public AuthenticateService(final AnonymousSessionCreateService anonymousCreateService,
                               final AuthorizedUserAuthenticateService authenticateService) {
        this.anonymousCreateService = anonymousCreateService;
        this.authenticateService = authenticateService;
    }

    public AuthenticateResponse authenticate(final AuthenticateRequest request, final String clientUid) {
        if (request.getId() == null) {
            return anonymousCreateService.registerAnonymous(clientUid);
        }
        return authenticateService.authorize(request, clientUid);
    }
}
