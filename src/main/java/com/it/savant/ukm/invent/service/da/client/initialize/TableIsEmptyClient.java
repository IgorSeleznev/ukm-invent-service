package com.it.savant.ukm.invent.service.da.client.initialize;

import com.it.savant.ukm.invent.service.common.exception.UkmInventServiceSystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class TableIsEmptyClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public TableIsEmptyClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public boolean isEmpty(final String tablename) {
        final Long count = jdbcTemplate.queryForObject(
                "SELECT count(*) \n" +
                        "FROM " + DATABASE_NAME + "." + tablename,
                (resultSet, i) -> resultSet.getLong(1)
        );
        if (count == null) {
            throw new UkmInventServiceSystemException("Error has occurred when try to get rows count in table '" + tablename + "'");
        }
        return count == 0;
    }
}
