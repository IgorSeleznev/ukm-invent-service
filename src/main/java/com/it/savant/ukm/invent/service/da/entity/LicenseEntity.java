package com.it.savant.ukm.invent.service.da.entity;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@ToString
@Accessors(chain = true)
public class LicenseEntity {

    private String clientId;
    private String location;
    private LocalDateTime lastActivityDateTime;
}
