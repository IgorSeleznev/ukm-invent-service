package com.it.savant.ukm.invent.service.da.client.security;

import com.it.savant.ukm.invent.service.common.exception.MoreThanOneFoundException;
import com.it.savant.ukm.invent.service.da.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;
import static java.util.Optional.empty;
import static java.util.Optional.of;

@Component
public class UserFindByIdClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public UserFindByIdClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Optional<UserEntity> findById(final Long id) {
        final List<UserEntity> found = jdbcTemplate.query(
                "SELECT id, name, password_hash, location, administrator, administrator_login \n" +
                        "FROM " + DATABASE_NAME + ".user \n" +
                        "WHERE id = ?",
                (resultSet, i) -> new UserEntity()
                        .setId(resultSet.getLong(1))
                        .setName(resultSet.getString(2))
                        .setPasswordHash(resultSet.getString(3))
                        .setLocation(resultSet.getString(4))
                        .setAdministrator((int) resultSet.getShort(5))
                        .setAdministratorLogin(resultSet.getString(6)),
                id
        );
        if (found.size() == 0) {
            return empty();
        }
        if (found.size() > 1) {
            throw new MoreThanOneFoundException("Для идентификатора " + id + " найдено больше одной записи, вероятно нарушена консистентность данных");
        }
        return of(found.get(0));
    }
}
