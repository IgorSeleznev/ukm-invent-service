package com.it.savant.ukm.invent.service.da.client.initialize;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class ExportQueueTableCreateClient implements CreateTableClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ExportQueueTableCreateClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public String getTableName() {
        return "export_queue";
    }

    @Override
    public void create() {
        jdbcTemplate.update(
                "CREATE TABLE IF NOT EXISTS " + DATABASE_NAME + ".export_queue ( \n" +
                        "    docId VARCHAR(50) NOT NULL, \n" +
                        "    push_date TIMESTAMP DEFAULT now() NOT NULL, \n" +
                        "    poll_date TIMESTAMP, \n" +
                        "    polled SMALLINT DEFAULT 0 NOT NULL \n" +
                        ") ENGINE = InnoDB DEFAULT CHARSET = UTF8"
        );
        /*jdbcTemplate.update(
                "CREATE INDEX IF NOT EXISTS " + DATABASE_NAME + ".export_queue_index_docid_polled \n" +
                        "ON " + DATABASE_NAME + ".export_queue ( \n" +
                        "    docId, polled \n" +
                        ")"
        );
        jdbcTemplate.update(
                "CREATE INDEX IF NOT EXISTS " + DATABASE_NAME + ".export_queue_index_polled_list \n" +
                        "ON " + DATABASE_NAME + ".export_queue ( \n" +
                        "    polled, push_date \n" +
                        ")"
        );
        jdbcTemplate.update(
                "CREATE INDEX IF NOT EXISTS " + DATABASE_NAME + ".export_queue_index_docId \n" +
                        "ON " + DATABASE_NAME + ".export_queue ( \n" +
                        "    docId \n" +
                        ")"
        );*/
    }
}