package com.it.savant.ukm.invent.service.web.controller.security;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.security.AuthenticateResponse;
import com.it.savant.ukm.invent.service.domain.service.security.AuthenticateService;
import com.it.savant.ukm.invent.service.web.interceptor.RequestHeaderExtractor;
import com.it.savant.ukm.invent.service.web.json.Response;
import com.it.savant.ukm.invent.service.web.json.security.AuthenticateRequestJson;
import com.it.savant.ukm.invent.service.web.json.security.AuthenticateResponseJson;
import com.it.savant.ukm.invent.service.web.security.Licensed;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import static com.it.savant.ukm.invent.service.domain.model.security.AuthenticateRequest.anonymousRequest;
import static com.it.savant.ukm.invent.service.web.constant.HttpHeaderConstant.HTTP_HEADER_CLIENT_UID;

@Slf4j
@RestController
public class AuthenticateController {

    private final AuthenticateService authenticateService;
    private final RequestHeaderExtractor headerExtractor;
    private final Converter<AuthenticateResponse, AuthenticateResponseJson> responseConverter;

    @Autowired
    public AuthenticateController(final AuthenticateService authenticateService,
                                  final RequestHeaderExtractor headerExtractor,
                                  final Converter<AuthenticateResponse, AuthenticateResponseJson> responseConverter
    ) {
        this.authenticateService = authenticateService;
        this.headerExtractor = headerExtractor;
        this.responseConverter = responseConverter;
    }

    @Licensed
    @PostMapping(value = "/authorize", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Response<AuthenticateResponseJson> authorize(
            @RequestBody final AuthenticateRequestJson request,
            final HttpServletRequest httpRequest
    ) {
        log.info("Принят запрос на авторизацию для пользователя с идентификатором {}", request.getId());

        final AuthenticateResponse response = authenticateService.authenticate(
                anonymousRequest(),
                headerExtractor.extract(httpRequest, HTTP_HEADER_CLIENT_UID)
        );

        return Response.success(
                responseConverter.convert(response)
        );
    }

}
