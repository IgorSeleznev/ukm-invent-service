package com.it.savant.ukm.invent.service.domain.service.integration.imports.assemble;

import com.it.savant.ukm.invent.service.domain.model.Store;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.parser.XmlNode;
import org.springframework.stereotype.Service;

@Service
public class IntegrationStoreAssembleService implements IntegrationAssembleService<Store> {

    @Override
    public String getAssembleName() {
        return "STORE";
    }

    @Override
    public Store assemble(final XmlNode xmlNode) {
        final Store store = new Store();

        xmlNode.getNodes().get(0)
                .getNodes().get(0)
                .getNodes()
                .forEach(
                        node -> {
                            if ("SMSTORELOCATIONS".equals(node.getName())) {
                                store
                                        .setId(node.getValues().get("ID"))
                                        .setAccepted(node.getValues().get("ACCEPTED"))
                                        .setAddress(node.getValues().get("ADDRESS"))
                                        .setFlags(node.getValues().get("FLAGS"))
                                        .setGln(node.getValues().get("GLN"))
                                        .setIdClass(node.getValues().get("IDCLASS"))
                                        .setLocType(node.getValues().get("LOCTYPE"))
                                        .setName(node.getValues().get("NAME"))
                                        .setOrderAlg(node.getValues().get("ORDERALG"))
                                        .setParentLoc(node.getValues().get("PARENTLOC"))
                                        .setPricingMethod(node.getValues().get("PRICINGMETHOD"))
                                        .setPrty(node.getValues().get("PRTY"))
                                        .setRgnid(node.getValues().get("RGNID"))
                                        .setSuggestOrderAlg(node.getValues().get("SUGGESTORDERALG"));
                            }
                        }
                );

        return store;
    }
}
