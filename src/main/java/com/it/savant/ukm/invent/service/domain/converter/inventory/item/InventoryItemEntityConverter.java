package com.it.savant.ukm.invent.service.domain.converter.inventory.item;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.entity.InventoryItemEntity;
import com.it.savant.ukm.invent.service.domain.model.InventoryItem;
import org.springframework.stereotype.Component;

@Component
public class InventoryItemEntityConverter implements Converter<InventoryItem, InventoryItemEntity> {
    @Override
    public InventoryItemEntity convert(final InventoryItem source) {
        return new InventoryItemEntity()
                .setDocId(source.getDocId())
                .setDocType(source.getDocType())
                .setSpecItem(source.getSpecItem())
                .setArticle(source.getArticle())
                .setDisplayItem(source.getDisplayItem())
                .setItemPrice(source.getItemPrice())
                .setItemPriceCur(source.getItemPriceCur())
                .setQuantity(source.getQuantity())
                .setTotalPrice(source.getTotalPrice())
                .setTotalPriceCur(source.getTotalPriceCur())
                .setActualQuantity(source.getActualQuantity())
                .setAwaitQuantity(source.getAwaitQuantity())
                .setHistory(source.getHistory());
    }
}
