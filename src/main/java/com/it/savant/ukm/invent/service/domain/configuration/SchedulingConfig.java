package com.it.savant.ukm.invent.service.domain.configuration;

import com.it.savant.ukm.invent.service.domain.service.scheduled.ScheduledService;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.lang.NonNull;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.util.List;

@EnableScheduling
@Configuration
public class SchedulingConfig implements SchedulingConfigurer {

    private static final String SCHEDULER_NAME = "RequestInvoicesJobTaskScheduler";

    private final List<ScheduledService> scheduledServices;

    @Autowired
    public SchedulingConfig(final List<ScheduledService> scheduledServices) {
        this.scheduledServices = scheduledServices;
    }

    @Bean
    public TaskScheduler taskScheduler() {
        val scheduler = new ThreadPoolTaskScheduler();
        scheduler.setThreadNamePrefix(SCHEDULER_NAME);
        scheduler.initialize();
        return scheduler;
    }

    @Override
    public void configureTasks(@NonNull final ScheduledTaskRegistrar scheduledTaskRegistrar) {
        scheduledServices.forEach(
                scheduledService -> scheduledTaskRegistrar.addTriggerTask(
                        scheduledService::execute,
                        scheduledService::nextExecutionTime
                )
        );
    }
}
