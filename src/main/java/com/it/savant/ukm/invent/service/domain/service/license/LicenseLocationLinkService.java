package com.it.savant.ukm.invent.service.domain.service.license;

import com.it.savant.ukm.invent.service.da.client.license.LicenseExistsClient;
import com.it.savant.ukm.invent.service.da.client.license.LicenseInsertClient;
import com.it.savant.ukm.invent.service.da.client.license.LicenseLocationLinkClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static org.apache.commons.codec.digest.DigestUtils.sha1Hex;

@Service
public class LicenseLocationLinkService {

    private final LicenseExistsClient existsClient;
    private final LicenseInsertClient insertClient;
    private final LicenseLocationLinkClient client;

    @Autowired
    public LicenseLocationLinkService(final LicenseExistsClient existsClient,
                                      final LicenseInsertClient insertClient,
                                      final LicenseLocationLinkClient client) {
        this.existsClient = existsClient;
        this.insertClient = insertClient;
        this.client = client;
    }

    public void link(final String location, final String clientUid) {
        if (!existsClient.exists(sha1Hex(clientUid))) {
            insertClient.insert(sha1Hex(clientUid));
        }
        client.link(location, sha1Hex(clientUid));
    }
}
