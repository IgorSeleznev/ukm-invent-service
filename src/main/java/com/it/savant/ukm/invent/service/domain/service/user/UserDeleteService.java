package com.it.savant.ukm.invent.service.domain.service.user;

import com.it.savant.ukm.invent.service.da.client.security.UserUpdateDeletedClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserDeleteService {

    private final UserUpdateDeletedClient deletedClient;
    private final UserDeleteValidateService deleteValidateService;
    private final UserSignOutService signOutService;

    @Autowired
    public UserDeleteService(final UserUpdateDeletedClient deletedClient,
                             final UserDeleteValidateService deleteValidateService,
                             final UserSignOutService signOutService) {
        this.deletedClient = deletedClient;
        this.deleteValidateService = deleteValidateService;
        this.signOutService = signOutService;
    }

    public void delete(final Long userId) {
        deleteValidateService.deleteValidate(userId);
        deletedClient.updateDeleted(userId);
        signOutService.singOut(userId);
    }
}
