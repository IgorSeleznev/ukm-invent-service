package com.it.savant.ukm.invent.service.da.client.store;

import com.it.savant.ukm.invent.service.da.entity.StoreEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class StoreReplaceClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public StoreReplaceClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void replace(final StoreEntity store) {
        jdbcTemplate.update(
                "REPLACE INTO " + DATABASE_NAME + ".store (\n" +
                        "    id, \n" +
                        "    accepted, \n" +
                        "    address, \n" +
                        "    flags, \n" +
                        "    gln, \n" +

                        "    idClass, \n" +
                        "    locType, \n" +
                        "    name, \n" +
                        "    orderAlg, \n" +
                        "    parentLoc, \n" +

                        "    pricingMethod, \n" +
                        "    prty, \n" +
                        "    rgnid, \n" +
                        "    suggestOrderAlg \n" +
                        ") \n" +
                        "VALUES (\n" +
                        "    ?, ?, ?, ?, ?, \n" +
                        "    ?, ?, ?, ?, ?, \n" +
                        "    ?, ?, ?, ? \n" +
                        ")",
                store.getId(),
                store.getAccepted(),
                store.getAddress(),
                store.getFlags(),
                store.getGln(),
                store.getIdClass(),
                store.getLocType(),
                store.getName(),
                store.getOrderAlg(),
                store.getParentLoc(),
                store.getPricingMethod(),
                store.getPrty(),
                store.getRgnid(),
                store.getSuggestOrderAlg()
        );
    }
}