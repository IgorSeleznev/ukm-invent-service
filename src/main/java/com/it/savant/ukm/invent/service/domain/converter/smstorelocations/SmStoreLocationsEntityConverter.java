package com.it.savant.ukm.invent.service.domain.converter.smstorelocations;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.entity.SmStoreLocationsEntity;
import com.it.savant.ukm.invent.service.domain.model.smstorelocations.SmStoreLocations;
import org.springframework.stereotype.Component;

@Component
public class SmStoreLocationsEntityConverter implements Converter<SmStoreLocations, SmStoreLocationsEntity> {

    @Override
    public SmStoreLocationsEntity convert(final SmStoreLocations source) {
        return new SmStoreLocationsEntity()
                .setId(source.getId())
                .setName(source.getName());
    }
}
