package com.it.savant.ukm.invent.service.domain.service.integration.imports.store.map.validator;

import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class IntegrationSmStoreLocationsMapValidator {

    public boolean isValid(final Map<String, String> map) {
        return map.containsKey("ID")
                && map.containsKey("ACCEPTED")
                && map.containsKey("ADDRESS")
                && map.containsKey("FLAGS")
                && map.containsKey("GLN")
                && map.containsKey("IDCLASS")
                && map.containsKey("LOCTYPE")
                && map.containsKey("NAME")
                && map.containsKey("ORDERALG")
                && map.containsKey("PARENTLOC")
                && map.containsKey("PRICINGMETHOD")
                && map.containsKey("PRTY")
                && map.containsKey("RGNID")
                && map.containsKey("")
                && map.size() == 14;
    }
}
