package com.it.savant.ukm.invent.service.domain.service.inventory;

import com.it.savant.ukm.invent.service.da.client.inventory.InventoryManuallyCloneByDocIdClient;
import com.it.savant.ukm.invent.service.da.client.inventory.item.InventoryItemListCloneByDocIdClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class InventoryManuallyCloneByDocIdService {

    private final InventoryManuallyCloneByDocIdClient inventoryCloneClient;
    private final InventoryItemListCloneByDocIdClient inventoryItemListCloneClient;

    @Autowired
    public InventoryManuallyCloneByDocIdService(final InventoryManuallyCloneByDocIdClient inventoryCloneClient,
                                                final InventoryItemListCloneByDocIdClient inventoryItemListCloneClient) {
        this.inventoryCloneClient = inventoryCloneClient;
        this.inventoryItemListCloneClient = inventoryItemListCloneClient;
    }

    public void clone(final String assortmentDocId, final String manuallyDocId) {
        inventoryCloneClient.clone(
                assortmentDocId,
                new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date()),
                manuallyDocId
        );
        inventoryItemListCloneClient.clone(assortmentDocId, manuallyDocId);
    }
}
