package com.it.savant.ukm.invent.service.domain.service.parameter;

import com.it.savant.ukm.invent.service.da.client.parameter.ParameterValueClient;
import com.it.savant.ukm.invent.service.da.client.parameter.ParameterValueUpdateClient;
import com.it.savant.ukm.invent.service.domain.model.parameter.Parameter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ParameterProvider {

    private final ParameterValueClient valueClient;
    private final ParameterValueUpdateClient updateClient;
    private final ParameterInsertService insertService;

    @Autowired
    public ParameterProvider(final ParameterValueClient valueClient,
                             final ParameterValueUpdateClient updateClient,
                             final ParameterInsertService insertService) {
        this.valueClient = valueClient;
        this.updateClient = updateClient;
        this.insertService = insertService;
    }

    public String read(final String parameterName) {
        return valueClient.value(parameterName);
    }

    public String safetyRead(final String parameterName, final String defaultValue) {
        try {
            return valueClient.value(parameterName);
        } catch (final Exception exception) {
            log.warn("Safety parameter read was finished with exception for parameter name [" + parameterName + "]", exception);
            return defaultValue;
        }
    }

    public void write(final String parameterName, final String value) {
        updateClient.update(parameterName, value);
    }

    public void insert(final Parameter parameter) {
        insertService.insert(parameter);
    }

}
