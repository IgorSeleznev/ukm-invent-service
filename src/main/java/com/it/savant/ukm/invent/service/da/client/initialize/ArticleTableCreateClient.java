package com.it.savant.ukm.invent.service.da.client.initialize;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class ArticleTableCreateClient implements CreateTableClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ArticleTableCreateClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public String getTableName() {
        return "article";
    }

    public void create() {
        jdbcTemplate.update(
                "CREATE TABLE IF NOT EXISTS " + DATABASE_NAME + ".article (\n" +
                        "    article VARCHAR(50) NOT NULL, \n" +
                        "    accepted VARCHAR(6), \n" +
                        "    bornin VARCHAR(255), \n" +
                        "    cashload VARCHAR(1), \n" +
                        "    country VARCHAR(255), \n" +
                        "    cutpricedays VARCHAR(6), \n" +
                        "    datasubtype VARCHAR(6), \n" +
                        "    datatype VARCHAR(6), \n" +
                        "    flags VARCHAR(11), \n" +
                        "    globalarticle VARCHAR(50), \n" +
                        "    idclass VARCHAR(11), \n" +
                        "    idmeasurement VARCHAR(6), \n" +
                        "    losses VARCHAR(16), \n" +
                        "    mesabbrev VARCHAR(6), \n" +
                        "    mesname VARCHAR(20), \n" +
                        "    minprofit VARCHAR(16), \n" +
                        "    name VARCHAR(255), \n" +
                        "    quantitydeviation VARCHAR(20), \n" +
                        "    receiptok VARCHAR(1), \n" +
                        "    scaleload VARCHAR(1), \n" +
                        "    scrap VARCHAR(16), \n" +
                        "    shortname VARCHAR(255), \n" +
                        "    storage VARCHAR(6), \n" +
                        "    usetimedim VARCHAR(6), \n" +
                        "    waste VARCHAR(16), \n" +
//                        "    rgnid VARCHAR(), \n" +
//                        "    datefrom VARCHAR(), \n" +
//                        "    dateto VARCHAR(), \n" +
//                        "    taxgroupid VARCHAR(), \n" +
                        "    PRIMARY KEY (article) \n" +
                        ") ENGINE = InnoDB DEFAULT CHARSET = UTF8"
        );
    }
}

