package com.it.savant.ukm.invent.service.da.entity;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class StoreEntity {

    private String id;
    private String accepted;
    private String address;
    private String flags;
    private String gln;
    private String idClass;
    private String locType;
    private String name;
    private String orderAlg;
    private String parentLoc;
    private String pricingMethod;
    private String prty;
    private String rgnid;
    private String suggestOrderAlg;
}
