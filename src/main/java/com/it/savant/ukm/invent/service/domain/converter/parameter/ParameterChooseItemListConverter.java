package com.it.savant.ukm.invent.service.domain.converter.parameter;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.parameter.ParameterChooseItem;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@Component
public class ParameterChooseItemListConverter implements Converter<String, List<ParameterChooseItem>> {

    @Override
    public List<ParameterChooseItem> convert(final String source) {
        if (isEmpty(source)) {
            return newArrayList();
        }
        final List<ParameterChooseItem> target = newArrayList();
        final String[] items = source.split(";");
        for (int i = 0; i < items.length; i++) {
            if ("".equals(items[i])) {
                continue;
            }
            final ParameterChooseItem item = new ParameterChooseItem();
            final String[] parts = items[i].split(":");
            int foundPart = 0;
            for (int j = 0; j < parts.length; j++) {
                if ("".equals(parts[j])) {
                    continue;
                }
                if (foundPart == 0) {
                    item.setValue(parts[j]);
                }
                if (foundPart == 1) {
                    item.setTitle(parts[j]);
                }
                foundPart++;
            }
            target.add(item);
        }
        return target;
    }
}
