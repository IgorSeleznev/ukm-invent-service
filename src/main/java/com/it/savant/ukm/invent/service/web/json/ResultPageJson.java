package com.it.savant.ukm.invent.service.web.json;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.ResultPage;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@ToString
@Accessors(chain = true)
public class ResultPageJson<T> {

    public static <S, T> ResultPageJson<T> resultFrom(final ResultPage<S> source, final Converter<S, T> converter) {
        return new ResultPageJson<>(
                source.getResult().stream()
                        .map(converter::convert)
                        .collect(Collectors.toList()),
                source.getTotalCount()
        );
    }

    private List<T> result;
    private Integer totalCount;
}
