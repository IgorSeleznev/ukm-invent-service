package com.it.savant.ukm.invent.service.web.controller.inventory.item;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.InventoryItemBrief;
import com.it.savant.ukm.invent.service.domain.service.inventory.item.brief.InventoryItemBriefListService;
import com.it.savant.ukm.invent.service.web.json.DocIdRequestJson;
import com.it.savant.ukm.invent.service.web.json.InventoryItemBriefJson;
import com.it.savant.ukm.invent.service.web.json.Response;
import com.it.savant.ukm.invent.service.web.security.Authorized;
import com.it.savant.ukm.invent.service.web.security.Licensed;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

import static com.it.savant.ukm.invent.service.web.constant.HttpHeaderConstant.HTTP_HEADER_CLIENT_UID;
import static com.it.savant.ukm.invent.service.web.json.Response.success;

@Slf4j
@RestController
public class InventoryItemBriefListController {

    private final InventoryItemBriefListService service;
    private final Converter<InventoryItemBrief, InventoryItemBriefJson> converter;

    @Autowired
    public InventoryItemBriefListController(final InventoryItemBriefListService service,
                                            final Converter<InventoryItemBrief, InventoryItemBriefJson> converter) {
        this.service = service;
        this.converter = converter;
    }

    @Licensed
    @Authorized
    @PostMapping(value = "/inventory/item/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<List<InventoryItemBriefJson>> classifierCardsList(
            @RequestBody final DocIdRequestJson request,
            final HttpServletRequest httpServletRequest
    ) {
        log.info("Принят запрос на получение списка товаров от клиента с идентификатором '{}'", httpServletRequest.getHeader(HTTP_HEADER_CLIENT_UID));
        return success(
                service.list(request.getDocId()).stream()
                        .map(converter::convert)
                        .collect(Collectors.toList())
        );
    }
}
