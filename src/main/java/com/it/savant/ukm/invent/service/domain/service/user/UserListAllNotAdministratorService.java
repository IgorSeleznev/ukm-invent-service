package com.it.savant.ukm.invent.service.domain.service.user;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.entity.UserListAllNotAdministratorClient;
import com.it.savant.ukm.invent.service.da.entity.UserListItemEntity;
import com.it.savant.ukm.invent.service.domain.model.security.UserListItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class UserListAllNotAdministratorService {

    private final UserListAllNotAdministratorClient client;
    private final Converter<UserListItemEntity, UserListItem> converter;

    @Autowired
    public UserListAllNotAdministratorService(final UserListAllNotAdministratorClient client,
                                              final Converter<UserListItemEntity, UserListItem> converter) {
        this.client = client;
        this.converter = converter;
    }

    public List<UserListItem> listAllNotAdministrator() {
        return client.listAll().stream()
                .map(converter::convert)
                .collect(toList());
    }
}
