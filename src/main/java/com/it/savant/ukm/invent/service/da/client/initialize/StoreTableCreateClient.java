package com.it.savant.ukm.invent.service.da.client.initialize;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class StoreTableCreateClient implements CreateTableClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public StoreTableCreateClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public String getTableName() {
        return "store";
    }

    public void create() {
        jdbcTemplate.update(
                "CREATE TABLE IF NOT EXISTS " + DATABASE_NAME + ".store (\n" +
                        "    id VARCHAR(11) NOT NULL, \n" +
                        "    accepted CHAR(1), \n" +
                        "    address VARCHAR(255), \n" +
                        "    flags VARCHAR(10), \n" +
                        "    gln VARCHAR(13), \n" +
                        "    idClass VARCHAR(10), \n" +
                        "    locType VARCHAR(5), \n" +
                        "    name VARCHAR(255), \n" +
                        "    orderAlg VARCHAR(255), \n" +
                        "    parentLoc VARCHAR(10), \n" +
                        "    pricingMethod VARCHAR(50), \n" +
                        "    prty VARCHAR(5), \n" +
                        "    rgnid VARCHAR(10), \n" +
                        "    suggestOrderAlg VARCHAR(50)," +
                        "    PRIMARY KEY (id)\n" +
                        ") ENGINE = InnoDB DEFAULT CHARSET = UTF8"
        );
    }
}
