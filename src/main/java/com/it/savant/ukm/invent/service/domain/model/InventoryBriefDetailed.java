package com.it.savant.ukm.invent.service.domain.model;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@ToString
@Accessors(chain = true)
public class InventoryBriefDetailed {

    private String docId;
    private String assortmentName;
    private List<InventoryItemBrief> items;
}
