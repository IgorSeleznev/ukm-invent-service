package com.it.savant.ukm.invent.service.domain.service.inventory.item.brief;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.client.inventory.item.brief.InventoryItemBriefListByDocIdClient;
import com.it.savant.ukm.invent.service.da.entity.InventoryItemBriefEntity;
import com.it.savant.ukm.invent.service.domain.model.InventoryItemBrief;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class InventoryItemBriefListService {

    private final InventoryItemBriefListByDocIdClient client;
    private final Converter<InventoryItemBriefEntity, InventoryItemBrief> converter;

    @Autowired
    public InventoryItemBriefListService(final InventoryItemBriefListByDocIdClient client,
                                         final Converter<InventoryItemBriefEntity, InventoryItemBrief> converter) {
        this.client = client;
        this.converter = converter;
    }

    public List<InventoryItemBrief> list(final String docId) {
        return client.findAllByClassifier(docId).stream()
                .map(converter::convert)
                .collect(toList());
    }
}
