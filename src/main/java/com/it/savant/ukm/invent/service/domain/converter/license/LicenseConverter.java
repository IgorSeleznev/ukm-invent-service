package com.it.savant.ukm.invent.service.domain.converter.license;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.entity.LicenseEntity;
import com.it.savant.ukm.invent.service.domain.model.license.License;
import org.springframework.stereotype.Component;

@Component
public class LicenseConverter implements Converter<LicenseEntity, License> {

    @Override
    public License convert(final LicenseEntity source) {
        return new License()
                .setClientId(source.getClientId())
                .setLastActivityDateTime(source.getLastActivityDateTime())
                .setLocation(source.getLocation());
    }
}
