package com.it.savant.ukm.invent.service.web.json;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ResetResultJson {

    public static ResetResultJson success() {
        return new ResetResultJson().setSuccess(true);
    }

    public static ResetResultJson failed(final Throwable throwable) {
        return new ResetResultJson().setSuccess(false).setErrorMessage(throwable.getMessage());
    }

    private boolean success;
    private String errorMessage;
}
