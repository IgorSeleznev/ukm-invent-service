package com.it.savant.ukm.invent.service.domain.configuration;

import com.it.savant.ukm.invent.service.da.client.initialize.ExportQueueTableCreateClient;
import com.it.savant.ukm.invent.service.domain.model.security.User;
import com.it.savant.ukm.invent.service.domain.service.integration.exports.InventoryExportEventAsyncDocIdQueue;
import com.it.savant.ukm.invent.service.domain.service.integration.exports.InventoryExportQueuePersistManager;
import com.it.savant.ukm.invent.service.domain.service.license.AntalLicenserKvarService;
import org.jlead.assets.security.v2.AuthenticationHolder;
import org.jlead.assets.security.v2.AuthenticationHolderConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedList;

import static com.google.common.collect.Lists.newLinkedList;

@Configuration
public class DomainConfiguration {

    @Bean
    public AuthenticationHolder<User, Boolean> securityHolder(final AntalLicenserKvarService service) {
        return new AuthenticationHolder<>(
                new AuthenticationHolderConfiguration()
                        .setMaxActiveSessionsCount(
                                service.siffra()
                        )
        );
    }

    @Bean
    public InventoryExportEventAsyncDocIdQueue inventoryExportEventAsyncDocIdQueue(
            final InventoryExportQueuePersistManager persistManager,
            final ExportQueueTableCreateClient tableCreateClient
    ) {
        tableCreateClient.create();
        final LinkedList<String> list = newLinkedList();
        list.addAll(
                persistManager.list()
        );
        return new InventoryExportEventAsyncDocIdQueue(persistManager, list);
    }
}
