package com.it.savant.ukm.invent.service.domain.converter.export;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.entity.InventoryExportRecordEntity;
import com.it.savant.ukm.invent.service.domain.model.export.InventoryExportRecord;
import org.springframework.stereotype.Component;

@Component
public class InventoryExportRecordConverter implements Converter<InventoryExportRecordEntity, InventoryExportRecord> {

    @Override
    public InventoryExportRecord convert(final InventoryExportRecordEntity source) {
        return new InventoryExportRecord()
                .setAssortmentName(source.getAssortmentName())
                .setConfirmDate(source.getConfirmDate())
                .setDocId(source.getDocId())
                .setPolled(source.getPolled() == 0);
    }
}
