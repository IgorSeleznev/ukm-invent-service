package com.it.savant.ukm.invent.service.web.controller.inventory.brief;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.InventoryBrief;
import com.it.savant.ukm.invent.service.domain.service.inventory.brief.InventoryBriefListService;
import com.it.savant.ukm.invent.service.web.json.InventoryBriefJson;
import com.it.savant.ukm.invent.service.web.json.Response;
import com.it.savant.ukm.invent.service.web.security.Authorized;
import com.it.savant.ukm.invent.service.web.security.Licensed;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

import static com.it.savant.ukm.invent.service.web.constant.HttpHeaderConstant.HTTP_HEADER_CLIENT_UID;
import static com.it.savant.ukm.invent.service.web.json.Response.success;

@Slf4j
@RestController
public class InventoryBriefListController {

    private final InventoryBriefListService service;
    private final Converter<InventoryBrief, InventoryBriefJson> converter;

    @Autowired
    public InventoryBriefListController(final InventoryBriefListService service,
                                        final Converter<InventoryBrief, InventoryBriefJson> converter) {
        this.service = service;
        this.converter = converter;
    }

    @Licensed
    @Authorized
    @GetMapping(value = "/inventory/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<List<InventoryBriefJson>> list(final HttpServletRequest httpRequest) {
        log.info(
                "Принят запрос на получение списка актов инвентаризации от клиента с идентификатором '{}'",
                httpRequest.getHeader(HTTP_HEADER_CLIENT_UID)
        );
        final List<InventoryBrief> result = service.list(
                httpRequest.getHeader(HTTP_HEADER_CLIENT_UID)
        );
        final List<InventoryBriefJson> resultJson = result.stream()
                .map(converter::convert)
                .collect(Collectors.toList());
        log.info("Получен следующий список ассортиментов: {}", result);
        log.info("Будет отправлен следующий список ассортиментов: {}", resultJson);
        return success(resultJson);
    }
}
