package com.it.savant.ukm.invent.service.domain.converter.store;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.entity.StoreEntity;
import com.it.savant.ukm.invent.service.domain.model.Store;
import org.springframework.stereotype.Component;

@Component
public class StoreEntityConverter implements Converter<Store, StoreEntity> {

    @Override
    public StoreEntity convert(final Store source) {
        return new StoreEntity()
                .setAccepted(source.getAccepted())
                .setAddress(source.getAddress())
                .setFlags(source.getFlags())
                .setGln(source.getGln())
                .setId(source.getId())
                .setIdClass(source.getIdClass())
                .setLocType(source.getLocType())
                .setName(source.getName())
                .setOrderAlg(source.getOrderAlg())
                .setParentLoc(source.getParentLoc())
                .setPricingMethod(source.getPricingMethod())
                .setPrty(source.getPrty())
                .setRgnid(source.getRgnid())
                .setSuggestOrderAlg(source.getSuggestOrderAlg());
    }
}
