package com.it.savant.ukm.invent.service.da.client.initialize;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class LicencesTableCreateClient implements CreateTableClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public LicencesTableCreateClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public String getTableName() {
        return "client_license";
    }

    public void create() {
        jdbcTemplate.update(
                "CREATE TABLE IF NOT EXISTS " + DATABASE_NAME + ".client_license (\n" +
                        "    client_id VARCHAR(200) DEFAULT '' NOT NULL, \n" +
                        "    location VARCHAR(12)," +
                        "    last_activity_date_time timestamp DEFAULT now() NOT NULL, \n" +
                        "    PRIMARY KEY (client_id) \n" +
                        ") ENGINE = InnoDB DEFAULT CHARSET = UTF8"
        );
    }
}
