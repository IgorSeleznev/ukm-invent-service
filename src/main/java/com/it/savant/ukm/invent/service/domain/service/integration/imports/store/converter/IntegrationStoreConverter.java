package com.it.savant.ukm.invent.service.domain.service.integration.imports.store.converter;

import com.it.savant.ukm.invent.service.domain.model.Store;
import com.it.savant.ukm.invent.service.domain.model.integration.IntegrationStorePackage;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.store.map.resolver.IntegrationSmStoreLocationsMapResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class IntegrationStoreConverter {

    private final IntegrationSmStoreLocationsMapResolver smStoreLocationsMapResolver;

    @Autowired
    public IntegrationStoreConverter(final IntegrationSmStoreLocationsMapResolver smStoreLocationsMapResolver) {
        this.smStoreLocationsMapResolver = smStoreLocationsMapResolver;
    }

    public Store convert(final IntegrationStorePackage source) {
        final Map<String, String> smStoreLocations = smStoreLocationsMapResolver.resolve(source);

        return new Store()
                .setId(smStoreLocations.get("ID"))
                .setAccepted(smStoreLocations.get("ACCEPTED"))
                .setAddress(smStoreLocations.get("ADDRESS"))
                .setFlags(smStoreLocations.get("FLAGS"))
                .setGln(smStoreLocations.get("GLN"))
                .setIdClass(smStoreLocations.get("IDCLASS"))
                .setLocType(smStoreLocations.get("LOCTYPE"))
                .setName(smStoreLocations.get("NAME"))
                .setOrderAlg(smStoreLocations.get("ORDERALG"))
                .setParentLoc(smStoreLocations.get("PARENTLOC"))
                .setPricingMethod(smStoreLocations.get("PRICINGMETHOD"))
                .setPrty(smStoreLocations.get("PRTY"))
                .setRgnid(smStoreLocations.get("RGNID"));
    }
}
