package com.it.savant.ukm.invent.service.web.controller.admin.export;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.PageableSortableRequest;
import com.it.savant.ukm.invent.service.domain.model.export.InventoryExportRecord;
import com.it.savant.ukm.invent.service.domain.service.export.InventoryExportListService;
import com.it.savant.ukm.invent.service.web.json.InventoryExportRecordJson;
import com.it.savant.ukm.invent.service.web.json.PageableSortableRequestJson;
import com.it.savant.ukm.invent.service.web.json.Response;
import com.it.savant.ukm.invent.service.web.json.ResultPageJson;
import com.it.savant.ukm.invent.service.web.security.Authorized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.it.savant.ukm.invent.service.web.json.Response.success;
import static java.util.stream.Collectors.toList;

@RestController
public class InventoryExportListController {

    private final InventoryExportListService service;
    private final Converter<InventoryExportRecord, InventoryExportRecordJson> exportRecordJsonConverter;
    private final Converter<PageableSortableRequestJson, PageableSortableRequest> requestConverter;

    @Autowired
    public InventoryExportListController(
            final InventoryExportListService service,
            final Converter<InventoryExportRecord, InventoryExportRecordJson> exportRecordJsonConverter,
            final Converter<PageableSortableRequestJson, PageableSortableRequest> requestConverter
    ) {
        this.service = service;
        this.exportRecordJsonConverter = exportRecordJsonConverter;
        this.requestConverter = requestConverter;
    }

    @Authorized(administratorOnly = true)
    @PostMapping("/admin/export/list")
    public Response<ResultPageJson<InventoryExportRecordJson>> list(@RequestBody final PageableSortableRequestJson request) {
        return success(
                ResultPageJson.resultFrom(
                        service.list(
                                requestConverter.convert(request)
                        ),
                        exportRecordJsonConverter
                )
        );
    }
}
