package com.it.savant.ukm.invent.service.domain.service.inventory;

import com.it.savant.ukm.invent.service.da.client.inventory.InventoryOrdernoByDocIdClient;
import com.it.savant.ukm.invent.service.da.client.store.StoreIdFindByClientIdClient;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InventoryManuallyCreateByDocIdService {

    private final StoreIdFindByClientIdClient storeFindClient;
    private final InventoryOrdernoByDocIdClient ordernoClient;
    private final InventoryManuallyCloneService createService;

    @Autowired
    public InventoryManuallyCreateByDocIdService(final StoreIdFindByClientIdClient storeFindClient,
                                                 final InventoryOrdernoByDocIdClient ordernoClient,
                                                 final InventoryManuallyCloneService createService) {
        this.storeFindClient = storeFindClient;
        this.ordernoClient = ordernoClient;
        this.createService = createService;
    }

    public void create(final String docId, final String clientId) {
        createService.clone(
                ordernoClient.ordernoByDocId(docId),
                storeFindClient.findByClientId(DigestUtils.sha1Hex(clientId))
        );
    }
}