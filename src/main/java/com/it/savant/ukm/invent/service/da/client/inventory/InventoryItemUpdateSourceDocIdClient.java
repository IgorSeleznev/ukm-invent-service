package com.it.savant.ukm.invent.service.da.client.inventory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryItemUpdateSourceDocIdClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryItemUpdateSourceDocIdClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void updateSourceDocId(final String docId, final String sourceDocId, final String article) {
        jdbcTemplate.update(
                "UPDATE " + DATABASE_NAME + ".inventory_item \n" +
                        "SET source_docId = ? \n" +
                        "WHERE docId = ? and article = ?",
                sourceDocId,
                docId,
                article
        );
    }
}
