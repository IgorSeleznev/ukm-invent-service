package com.it.savant.ukm.invent.service.domain.service.integration.imports.source.local;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Builder
@Accessors(chain = true)
public class IntegrationLocalFileSourceConfiguration {

    private String path;
    private String filename;
    private String localTemporaryPath;
}
