package com.it.savant.ukm.invent.service.web.controller.inventory.item;

import com.it.savant.ukm.invent.service.domain.service.inventory.item.brief.InventoryItemMarkAddedService;
import com.it.savant.ukm.invent.service.web.json.InventoryItemMarkAddedRequestJson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class InventoryItemMarkAddedController {

    private final InventoryItemMarkAddedService service;

    @Autowired
    public InventoryItemMarkAddedController(final InventoryItemMarkAddedService service) {
        this.service = service;
    }

    @PostMapping("/dev/inventory/item/mark/added")
    public void throwSourceDocId(@RequestBody final InventoryItemMarkAddedRequestJson request) {
        log.warn(
                "Принят запрос для пометки товара '{}' в документе '{}' как добавленного в ассортимент",
                request.getArticle(),
                request.getDocId()
        );
        service.mark(request.getDocId(), request.getArticle());
    }
}
