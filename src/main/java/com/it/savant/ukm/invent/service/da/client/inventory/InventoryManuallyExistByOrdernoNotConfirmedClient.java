package com.it.savant.ukm.invent.service.da.client.inventory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryManuallyExistByOrdernoNotConfirmedClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryManuallyExistByOrdernoNotConfirmedClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public boolean existNotConfirmed(final String orderno, final String location) {
        final List<Long> results = jdbcTemplate.query(
                "SELECT count(*) \n" +
                        "FROM " + DATABASE_NAME + ".inventory \n" +
                        "WHERE orderno = ? AND location = ? AND created_manually = 1 AND confirmed = 0",
                (resultSet, i) -> resultSet.getLong(1),
                orderno,
                location
        );
        return results.get(0) > 0;
    }
}
