package com.it.savant.ukm.invent.service.domain.service.integration.imports.source.ftp;

import com.it.savant.ukm.invent.service.domain.service.integration.IntegrationType;
import com.it.savant.ukm.invent.service.domain.service.integration.configuration.IntegrationFTPConfiguration;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.source.IntegrationDirectorySource;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.source.IntegrationDirectorySourceBuilder;
import com.it.savant.ukm.invent.service.domain.service.parameter.ParameterProvider;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.domain.constants.ParameterConstants.*;
import static com.it.savant.ukm.invent.service.domain.service.integration.IntegrationType.FTP;

@Component
public class IntegrationFTPDirectorySourceBuilder implements IntegrationDirectorySourceBuilder {

    private final FTPClient ftpClient;
    private final ParameterProvider parameterProvider;

    @Autowired
    public IntegrationFTPDirectorySourceBuilder(final FTPClient ftpClient,
                                                final ParameterProvider parameterProvider) {
        this.ftpClient = ftpClient;
        this.parameterProvider = parameterProvider;
    }

    @Override
    public IntegrationType integrationType() {
        return FTP;
    }

    @Override
    public IntegrationDirectorySource source() {
        return new IntegrationFTPDirectorySource(
                ftpClient,
                new IntegrationFTPConfiguration(
                        parameterProvider.read(INTEGRATION_SOURCE_FTP_HOST),
                        Integer.parseInt(parameterProvider.read(INTEGRATION_SOURCE_FTP_PORT)),
                        parameterProvider.read(INTEGRATION_SOURCE_FTP_USERNAME),
                        parameterProvider.read(INTEGRATION_SOURCE_FTP_PASSWORD),
                        parameterProvider.read(INTEGRATION_SOURCE_FTP_PATH),
                        parameterProvider.read(INTEGRATION_SOURCE_FTP_TEMPORARY_PATH)
                )
        );
    }
}
