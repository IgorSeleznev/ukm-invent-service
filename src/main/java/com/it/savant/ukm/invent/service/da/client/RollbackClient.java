package com.it.savant.ukm.invent.service.da.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class RollbackClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public RollbackClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void rollback() {
        jdbcTemplate.update("ROLLBACK");
    }
}
