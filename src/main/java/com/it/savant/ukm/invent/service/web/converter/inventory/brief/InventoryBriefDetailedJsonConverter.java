package com.it.savant.ukm.invent.service.web.converter.inventory.brief;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.InventoryItemBrief;
import com.it.savant.ukm.invent.service.domain.model.InventoryBriefDetailed;
import com.it.savant.ukm.invent.service.web.json.InventoryItemBriefJson;
import com.it.savant.ukm.invent.service.web.json.InventoryBriefDetailedJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class InventoryBriefDetailedJsonConverter implements Converter<InventoryBriefDetailed, InventoryBriefDetailedJson> {

    private final Converter<InventoryItemBrief, InventoryItemBriefJson> converter;

    @Autowired
    public InventoryBriefDetailedJsonConverter(final Converter<InventoryItemBrief, InventoryItemBriefJson> converter) {
        this.converter = converter;
    }

    @Override
    public InventoryBriefDetailedJson convert(final InventoryBriefDetailed source) {
        return new InventoryBriefDetailedJson()
                .setDocId(source.getDocId())
                .setAssortmentName(source.getAssortmentName())
                .setItems(
                        source.getItems().stream()
                                .map(converter::convert)
                                .collect(Collectors.toList())
                );
    }
}
