package com.it.savant.ukm.invent.service.domain.service.inventory;

import com.it.savant.ukm.invent.service.da.client.inventory.item.InventoryItemMaxRecordDateClient;
import com.it.savant.ukm.invent.service.da.client.inventory.item.InventoryItemMinRecordDateClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

import static java.util.Optional.empty;

@Service
public class InventoryManuallyDifferenceMaxRecordDateService {

    private final InventoryItemMinRecordDateClient minDateClient;
    private final InventoryItemMaxRecordDateClient maxDateClient;

    @Autowired
    public InventoryManuallyDifferenceMaxRecordDateService(
            final InventoryItemMinRecordDateClient minDateClient,
            final InventoryItemMaxRecordDateClient maxDateClient
    ) {
        this.minDateClient = minDateClient;
        this.maxDateClient = maxDateClient;
    }

    public Optional<Long> differenceMaxRecordDate(final String docId) {
        final Long minDate = minDateClient.min(docId);
        final Long maxDate = maxDateClient.max(docId);
        if (Objects.equals(minDate, maxDate)) {
            return Optional.of(maxDate);
        }
        return empty();
    }

}
