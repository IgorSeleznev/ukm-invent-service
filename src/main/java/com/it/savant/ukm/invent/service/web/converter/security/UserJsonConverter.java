package com.it.savant.ukm.invent.service.web.converter.security;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.security.User;
import com.it.savant.ukm.invent.service.web.json.UserJson;
import org.springframework.stereotype.Component;

@Component
public class UserJsonConverter implements Converter<User, UserJson> {

    @Override
    public UserJson convert(final User source) {
        return new UserJson()
                .setId(source.getId())
                .setName(source.getName())
                .setPasswordHash(source.getPasswordHash())
                .setLocation(source.getLocation())
                .setAdministratorLogin(source.getAdministratorLogin())
                .setAdministrator(source.getAdministrator());
    }
}
