package com.it.savant.ukm.invent.service.domain.service.parameter;

import com.it.savant.ukm.invent.service.domain.model.PageableSortableRequest;
import com.it.savant.ukm.invent.service.domain.model.ResultPage;
import com.it.savant.ukm.invent.service.domain.model.parameter.Parameter;
import com.it.savant.ukm.invent.service.domain.service.PaginateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ParameterListAllPageableSortableService {

    private final ParameterListAllService listAllService;
    private final ParameterListSorter sorter;
    private final PaginateService paginateService;

    @Autowired
    public ParameterListAllPageableSortableService(final ParameterListAllService listAllService,
                                                   final ParameterListSorter sorter,
                                                   final PaginateService paginateService) {
        this.listAllService = listAllService;
        this.sorter = sorter;
        this.paginateService = paginateService;
    }

    public ResultPage<Parameter> listAll(final PageableSortableRequest request) {
        return paginateService.paginate(
                sorter.sort(
                        listAllService.listAll(),
                        request
                ),
                request
        );
    }
}
