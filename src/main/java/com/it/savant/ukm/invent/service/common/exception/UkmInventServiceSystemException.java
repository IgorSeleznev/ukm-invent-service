package com.it.savant.ukm.invent.service.common.exception;

public class UkmInventServiceSystemException extends RuntimeException {

    public UkmInventServiceSystemException(final String message) {
        super(message);
    }

    public UkmInventServiceSystemException(final Throwable throwable) {
        super(throwable);
    }
}
