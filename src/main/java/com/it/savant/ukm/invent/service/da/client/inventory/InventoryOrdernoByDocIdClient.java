package com.it.savant.ukm.invent.service.da.client.inventory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryOrdernoByDocIdClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryOrdernoByDocIdClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public String ordernoByDocId(final String docId) {
        return jdbcTemplate.queryForObject(
                "SELECT orderno \n" +
                        "FROM " + DATABASE_NAME + ".inventory \n" +
                        "WHERE id = ?",
                (resultSet, rowNum) -> resultSet.getString(1),
                docId
        );
    }
}
