package com.it.savant.ukm.invent.service.domain.service.database;

import com.it.savant.ukm.invent.service.da.client.initialize.CreateTableClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;



@Service
public class TablesCreateService {

    private final Map<String, CreateTableClient> clients = new HashMap<>();

    @Autowired
    public TablesCreateService(final List<CreateTableClient> clients) {
        clients.forEach(
                client -> this.clients.put(client.getTableName(), client)
        );
    }

    public void create() {
        clients.get("parameter").create();
        clients.get("user").create();
        clients.get("measure").create();
        clients.get("article").create();
        clients.get("client_license").create();
        clients.get("file_import_log").create();
        clients.get("store").create();
        clients.get("inventory").create();
        clients.get("inventory_item").create();
        clients.get("export_queue").create();
    }
}
