package com.it.savant.ukm.invent.service.domain.converter.inventory.brief;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.entity.InventoryBriefEntity;
import com.it.savant.ukm.invent.service.domain.model.InventoryBrief;
import org.springframework.stereotype.Component;

@Component
public class InventoryBriefConverter implements Converter<InventoryBriefEntity, InventoryBrief> {

    @Override
    public InventoryBrief convert(final InventoryBriefEntity source) {
        return new InventoryBrief()
                .setDocId(source.getDocId())
                .setAssortmentName(source.getAssortmentName());
    }
}
