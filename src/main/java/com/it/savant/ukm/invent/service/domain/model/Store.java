package com.it.savant.ukm.invent.service.domain.model;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class Store {

    private String id;
    private String accepted;
    private String address;
    private String flags;
    private String gln;
    private String idClass;
    private String locType;
    private String name;
    private String orderAlg;
    private String parentLoc;
    private String pricingMethod;
    private String prty;
    private String rgnid;
    private String suggestOrderAlg;
}
