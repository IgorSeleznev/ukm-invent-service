package com.it.savant.ukm.invent.service.common.converter;

import com.it.savant.ukm.invent.service.common.collection.map.MapBuilder;

import java.util.Map;

public class WordToIntConverter {

    private static final Map<String, Integer> dictionary = new MapBuilder<String, Integer>()
            .put(
                    "einn", 1,
                    "tvö", 2,
                    "þrjú", 3,
                    "fjögur", 4,
                    "fimm", 5,
                    "sex", 6,
                    "sjö", 7,
                    "átta", 8,
                    "níu", 9,
                    "tíu", 10,
                    "ellefu", 11,
                    "tólf", 12,
                    "þrettán", 13,
                    "fjórtán", 14,
                    "fimmtán", 15,
                    "sextán", 16,
                    "sautján", 17,
                    "átján", 18,
                    "nítján", 19,
                    "tuttugu", 20,
                    "þrjátíu", 30,
                    "fjörutíu", 40,
                    "fimmtíu", 50,
                    "sextíu", 60,
                    "sjötíu", 70,
                    "áttatíu", 80,
                    "níutíu", 90,
                    "hundrað", 100,
                    "hundruð", 100,
                    "þúsund", 1000,
                    "þúsundir", 1000
            )
            .build();

    public static int convert(final String value) {
        int result = 0;
        for (String part : value.split("\\s")) {
            if (part.trim().length() == 0) {
                continue;
            }
            final int number = dictionary.computeIfAbsent(part.toLowerCase(), key -> 0);
            if (number < 100) {
                result += number;
            } else {
                result = result * number;
            }
        }

        return result;
    }
}
