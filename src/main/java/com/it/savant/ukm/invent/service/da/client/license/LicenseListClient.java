package com.it.savant.ukm.invent.service.da.client.license;

import com.it.savant.ukm.invent.service.da.entity.LicenseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;
import static java.time.ZoneId.systemDefault;

@Component
public class LicenseListClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public LicenseListClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<LicenseEntity> list(final int pageNumber,
                                    final int pageSize,
                                    final String sortColumn,
                                    final String direction) {
        return jdbcTemplate.query(
                "SELECT client_id, location, last_activity_date_time \n" +
                        "FROM " + DATABASE_NAME + " \n" +
                        "ORDER BY " + sortColumn + " " + direction + " \n" +
                        "LIMIT " + pageNumber + ", " + pageSize,
                (resultSet, i) -> new LicenseEntity()
                        .setClientId(resultSet.getString(1))
                        .setLocation(resultSet.getString(2))
                        .setLastActivityDateTime(
                                resultSet.getDate(3).toInstant()
                                        .atZone(systemDefault())
                                        .toLocalDateTime()
                        )
        );
    }
}
