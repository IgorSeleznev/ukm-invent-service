package com.it.savant.ukm.invent.service.domain.service.integration.imports.source.ftp;

import com.it.savant.ukm.invent.service.common.exception.UkmInventServiceBusinessException;
import com.it.savant.ukm.invent.service.common.value.ValueHolder;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.source.IntegrationFileSource;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.source.IntegrationFileSourceDeleteService;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import static com.it.savant.ukm.invent.service.common.value.ValueHolder.hold;

@Slf4j
public class IntegrationFTPFileSource implements IntegrationFileSource {

    private final IntegrationFTPFileSourceConfiguration configuration;

    private String temporaryFilename;

    public IntegrationFTPFileSource(final IntegrationFTPFileSourceConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public IntegrationFileSourceDeleteService deleteTemporaryFileService() {
        return new IntegrationFTPTemporaryFileSourceDeleteService(
                configuration.getLocalTemporaryPath(),
                temporaryFilename
        );
    }

    @Override
    public IntegrationFileSourceDeleteService deleteOriginalFileService() {
        return new IntegrationFTPOriginalFileSourceDeleteService(configuration);
    }

    @Override
    public String getFilename() {
        return configuration.getRemoteFilename();
    }

    @Override
    public String read() {
        temporaryFilename = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-").format(new Date()) +
                Thread.currentThread().getId() +
                UUID.randomUUID().toString() + ".tmp";

        final File downloadFile = Paths.get(configuration.getLocalTemporaryPath(), temporaryFilename).toFile();
        final ValueHolder<OutputStream> holder = hold();
        try {
            String content = null;
            holder.setValue(
                    new BufferedOutputStream(new FileOutputStream(downloadFile))
            );
            configuration.getClient().changeWorkingDirectory(configuration.getRemotePath());
            boolean success = configuration.getClient().retrieveFile(
                    configuration.getRemoteFilename(),
                    holder.getValue()
            );
            holder.getValue().close();
            if (success) {
                content = Files.readString(Paths.get(configuration.getLocalTemporaryPath(), temporaryFilename));
            } else {
                log.error(
                        "Read file from FTP-server was failed with non-success status for remote filename {} in local filename {} to path {}",
                        configuration.getRemoteFilename(),
                        temporaryFilename,
                        configuration.getLocalTemporaryPath()
                );
            }
            return content;
        } catch (final Exception exception) {
            try {
                holder.getValue().close();
            } catch (final IOException e) {
                log.warn(
                        "Attention, error has occurred when ftp file source try to close output stream with local filename {} in path {}, and we got exception message [{}]",
                        temporaryFilename,
                        configuration.getLocalTemporaryPath(),
                        e.getMessage()
                );
            }
            try {
                Files.deleteIfExists(Paths.get(configuration.getLocalTemporaryPath(), temporaryFilename));
            } catch (IOException e) {
                log.warn("Attention, error has occurred when ftp file source try to delete local temporary file if that exists withlocal filename {} in path {}, and we got exception message [{}]",
                        temporaryFilename,
                        configuration.getLocalTemporaryPath(),
                        e.getMessage()
                );
            }
            throw new UkmInventServiceBusinessException(exception);
        }
    }
}