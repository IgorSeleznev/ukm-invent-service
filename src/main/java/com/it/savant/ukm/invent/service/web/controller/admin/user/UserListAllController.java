package com.it.savant.ukm.invent.service.web.controller.admin.user;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.PageableSortableRequest;
import com.it.savant.ukm.invent.service.domain.model.security.User;
import com.it.savant.ukm.invent.service.domain.service.user.UserListAllPageableSortableService;
import com.it.savant.ukm.invent.service.web.json.PageableSortableRequestJson;
import com.it.savant.ukm.invent.service.web.json.Response;
import com.it.savant.ukm.invent.service.web.json.ResultPageJson;
import com.it.savant.ukm.invent.service.web.json.UserJson;
import com.it.savant.ukm.invent.service.web.security.Authorized;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static com.it.savant.ukm.invent.service.web.json.Response.success;

@Slf4j
@RestController
public class UserListAllController {

    private final UserListAllPageableSortableService service;
    private final Converter<User, UserJson> userConverter;
    private final Converter<PageableSortableRequestJson, PageableSortableRequest> requestConverter;

    @Autowired
    public UserListAllController(final UserListAllPageableSortableService service,
                                 final Converter<User, UserJson> userConverter,
                                 final Converter<PageableSortableRequestJson, PageableSortableRequest> requestConverter) {
        this.service = service;
        this.userConverter = userConverter;
        this.requestConverter = requestConverter;
    }

    @Authorized(administratorOnly = true)
    @PostMapping("/admin/user/list-all")
    public Response<ResultPageJson<UserJson>> userList(@RequestBody final PageableSortableRequestJson request) {
        log.info("Принят запрос на получение списка всех пользователей");
        return success(
                ResultPageJson.resultFrom(
                        service.findAll(
                                requestConverter.convert(request)
                        ),
                        userConverter
                )
        );
    }
}
