package com.it.savant.ukm.invent.service.web.converter;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.PageableSortableRequest;
import com.it.savant.ukm.invent.service.web.json.PageableSortableRequestJson;
import org.springframework.stereotype.Component;

@Component
public class PageableSortableRequestDomainConverter
        implements Converter<PageableSortableRequestJson, PageableSortableRequest> {

    @Override
    public PageableSortableRequest convert(final PageableSortableRequestJson source) {
        return new PageableSortableRequest()
                .setPageNumber(source.getPageNumber())
                .setPageSize(source.getPageSize())
                .setSortDirection(source.getSortDirection())
                .setSortField(source.getSortField());
    }
}
