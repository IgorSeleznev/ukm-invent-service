package com.it.savant.ukm.invent.service.web.converter.inventory;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.InventoryConfirmRequest;
import com.it.savant.ukm.invent.service.web.json.InventoryConfirmRequestJson;
import org.springframework.stereotype.Component;

@Component
public class InventoryConfirmRequestDomainConverter
        implements Converter<InventoryConfirmRequestJson, InventoryConfirmRequest> {

    @Override
    public InventoryConfirmRequest convert(final InventoryConfirmRequestJson source) {
        return new InventoryConfirmRequest()
                .setDocId(source.getDocId())
                .setForce(source.getForce());
    }
}
