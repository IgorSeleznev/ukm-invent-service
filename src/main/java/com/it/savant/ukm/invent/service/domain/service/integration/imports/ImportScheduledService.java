package com.it.savant.ukm.invent.service.domain.service.integration.imports;

import com.it.savant.ukm.invent.service.domain.service.scheduled.ScheduledService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TriggerContext;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ImportScheduledService implements ScheduledService {

    private final ImportNextExecutionTimeService nextExecutionTimeService;
    private final ImportManager importManager;

    @Autowired
    public ImportScheduledService(final ImportNextExecutionTimeService nextExecutionTimeService,
                                  final ImportManager importManager) {
        this.nextExecutionTimeService = nextExecutionTimeService;
        this.importManager = importManager;
    }

    @Override
    public void execute() {
        importManager.doImport();
    }

    @Override
    public Date nextExecutionTime(final TriggerContext triggerContext) {
        return nextExecutionTimeService.nextExecutionTime(triggerContext);
    }
}
