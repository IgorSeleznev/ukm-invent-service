package com.it.savant.ukm.invent.service.domain.service.integration.imports.source.local;

import com.it.savant.ukm.invent.service.domain.service.integration.IntegrationType;
import com.it.savant.ukm.invent.service.domain.service.integration.configuration.IntegrationLocalConfiguration;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.source.IntegrationDirectorySource;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.source.IntegrationDirectorySourceBuilder;
import com.it.savant.ukm.invent.service.domain.service.parameter.ParameterProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.domain.constants.ParameterConstants.INTEGRATION_SOURCE_LOCAL_PATH;
import static com.it.savant.ukm.invent.service.domain.constants.ParameterConstants.INTEGRATION_SOURCE_LOCAL_TEMPORARY_PATH;
import static com.it.savant.ukm.invent.service.domain.service.integration.IntegrationType.LOCAL;

@Component
public class IntegrationLocalDirectorySourceBuilder implements IntegrationDirectorySourceBuilder {

    private final ParameterProvider parameterProvider;

    @Autowired
    public IntegrationLocalDirectorySourceBuilder(final ParameterProvider parameterProvider) {
        this.parameterProvider = parameterProvider;
    }

    @Override
    public IntegrationType integrationType() {
        return LOCAL;
    }

    @Override
    public IntegrationDirectorySource source() {
        return new IntegrationLocalDirectorySource(
                new IntegrationLocalConfiguration(
                        parameterProvider.read(INTEGRATION_SOURCE_LOCAL_PATH),
                        parameterProvider.read(INTEGRATION_SOURCE_LOCAL_TEMPORARY_PATH)
                )
        );
    }
}
