package com.it.savant.ukm.invent.service.da.client.inventory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryConfirmClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryConfirmClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void confirm(final String docId, final String finalDate, final String username) {
        jdbcTemplate.update(
                "UPDATE " + DATABASE_NAME + ".inventory \n" +
                        "SET confirmed = 1, finalDate = ?, commentary = ? \n" +
                        "WHERE id = ? and confirmed = 0",
                finalDate,
                username,
                docId
        );
    }

}
