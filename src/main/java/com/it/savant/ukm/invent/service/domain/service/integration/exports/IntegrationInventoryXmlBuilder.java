package com.it.savant.ukm.invent.service.domain.service.integration.exports;

import com.it.savant.ukm.invent.service.domain.model.Inventory;
import com.it.savant.ukm.invent.service.domain.service.parameter.ParameterProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class IntegrationInventoryXmlBuilder {

    private final IntegrationInventoryItemListXmlBuilder itemListXmlBuilder;
    private final IntegrationExportOrderNoGenerator orderNoGenerator;

    @Autowired
    public IntegrationInventoryXmlBuilder(final IntegrationInventoryItemListXmlBuilder itemListXmlBuilder,
                                          final IntegrationExportOrderNoGenerator orderNoGenerator) {
        this.itemListXmlBuilder = itemListXmlBuilder;
        this.orderNoGenerator = orderNoGenerator;
    }

    public String buildXml(final Inventory inventory) {
        final StringBuilder xml = new StringBuilder();
        final List<String> itemsXml = itemListXmlBuilder.xmlBuild(inventory);

        xml.append("<PACKAGE name=\"200523200114_895450_5\">\n" +
                "  <POSTOBJECT description=\"Инвентаризационная опись\" action=\"normal\">\n" +
                "    <Id>RL" + inventory.getId() + "</Id>\n" +
                "    <RL>\n" +
                "      <SMDOCUMENTS>\n");

        xml.append("        <ID>" + inventory.getId() + "</ID>\n");
        xml.append("        <DOCTYPE>RL</DOCTYPE>\n");
        xml.append("        <BORNIN>" + inventory.getBornIn() + "</BORNIN>\n");
        xml.append("        <COMMENTARY>" + inventory.getCommentary() + "</COMMENTARY>\n");
        xml.append("        <CREATEDAT>" + inventory.getCreateDat() + "</CREATEDAT>\n");
        xml.append("        <CURRENCYMULTORDER>" + inventory.getCurrencyMultOrder() + "</CURRENCYMULTORDER>\n");
        xml.append("        <CURRENCYRATE>" + inventory.getCurrencyRate() + "</CURRENCYRATE>\n");
        xml.append("        <CURRENCYTYPE>" + inventory.getCurrencyType() + "</CURRENCYTYPE>\n");
        xml.append("        <DOCSTATE>" + inventory.getDocState() + "</DOCSTATE>\n");
        xml.append("        <ISROUBLES>" + inventory.getIsRoubles() + "</ISROUBLES>\n");
        xml.append("        <LOCATION>" + inventory.getLocation() + "</LOCATION>\n");
        xml.append("        <OPCODE>" + inventory.getOpCode() + "</OPCODE>\n");
        xml.append("        <PRICEROUNDMODE>" + inventory.getPriceRoundMode() + "</PRICEROUNDMODE>\n");
        xml.append("        <TOTALSUM>" + inventory.getTotalSum() + "</TOTALSUM>\n");
        xml.append("        <TOTALSUMCUR>" + inventory.getTotalSumCur() + "</TOTALSUMCUR>\n");
        xml.append("      </SMDOCUMENTS>\n");

        xml.append(itemsXml.get(0));

        xml.append("      <SMROLLS>\n");
        xml.append("        <ID>" + inventory.getId() + "</ID>\n");
        xml.append("        <DOCTYPE>RL</DOCTYPE>\n");
        xml.append("        <FILLSPECTYPE>" + inventory.getFillSpecType() + "</FILLSPECTYPE>\n");
        xml.append("        <FINALDATE>" + inventory.getFinalDate() + "</FINALDATE>\n");
        xml.append("        <ISACTIVEONLY>" + inventory.getIsActiveOnly() + "</ISACTIVEONLY>\n");
        xml.append("        <ISFILLCOMPLETE>" + inventory.getIsFillComplete() + "</ISFILLCOMPLETE>\n");
        xml.append("        <ORDERNO>" + orderNoGenerator.exportOrderNoString(inventory) + "</ORDERNO>\n");
        xml.append("        <OURSELFCLIENT>" + inventory.getOurSelfClient() + "</OURSELFCLIENT>\n");
        xml.append("        <PREAMBLEDATE>" + inventory.getPreambleDate() + "</PREAMBLEDATE>\n");
        xml.append("        <PRICEMODE>3</PRICEMODE>\n");
        xml.append("        <ROLLMODE>1</ROLLMODE>\n");
        xml.append("        <STORELOC>" + inventory.getStoreLoc() + "</STORELOC>\n");
        xml.append("        <WITHDUE>" + inventory.getWithDue() + "</WITHDUE>\n");
        xml.append("      </SMROLLS>\n");

        xml.append(itemsXml.get(1));

        xml.append("    </RL>\n" +
                "  </POSTOBJECT>\n" +
                "</PACKAGE>");

        return xml.toString();
    }
}
