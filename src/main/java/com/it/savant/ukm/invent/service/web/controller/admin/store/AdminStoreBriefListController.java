package com.it.savant.ukm.invent.service.web.controller.admin.store;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.StoreBrief;
import com.it.savant.ukm.invent.service.domain.service.store.brief.StoreBriefListService;
import com.it.savant.ukm.invent.service.web.json.Response;
import com.it.savant.ukm.invent.service.web.json.StoreBriefJson;
import com.it.savant.ukm.invent.service.web.security.Authorized;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.it.savant.ukm.invent.service.web.json.Response.success;
import static java.util.stream.Collectors.toList;

@Slf4j
@RestController
public class AdminStoreBriefListController {

    private final StoreBriefListService service;
    private final Converter<StoreBrief, StoreBriefJson> converter;

    @Autowired
    public AdminStoreBriefListController(final StoreBriefListService service,
                                         final Converter<StoreBrief, StoreBriefJson> converter) {
        this.service = service;
        this.converter = converter;
    }

    @Authorized(administratorOnly = true)
    @GetMapping("/admin/store/list")
    public Response<List<StoreBriefJson>> storeList() {
        log.info("Принят запрос на получение списка магазинов для администратора");
        return success(
                service.listAll().stream()
                        .map(converter::convert)
                        .collect(toList())
        );
    }
}
