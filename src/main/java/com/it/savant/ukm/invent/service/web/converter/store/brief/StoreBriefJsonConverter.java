package com.it.savant.ukm.invent.service.web.converter.store.brief;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.StoreBrief;
import com.it.savant.ukm.invent.service.web.json.StoreBriefJson;
import org.springframework.stereotype.Component;

@Component
public class StoreBriefJsonConverter implements Converter<StoreBrief, StoreBriefJson> {

    @Override
    public StoreBriefJson convert(final StoreBrief source) {
        return new StoreBriefJson()
                .setId(source.getId())
                .setName(source.getName());
    }
}
