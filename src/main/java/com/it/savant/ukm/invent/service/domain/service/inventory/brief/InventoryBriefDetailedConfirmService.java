package com.it.savant.ukm.invent.service.domain.service.inventory.brief;

import com.it.savant.ukm.invent.service.da.client.inventory.item.brief.InventoryItemBriefQuantityUpdateClient;
import com.it.savant.ukm.invent.service.da.client.inventory.InventoryConfirmClient;
import com.it.savant.ukm.invent.service.domain.model.InventoryBriefDetailed;
import com.it.savant.ukm.invent.service.domain.model.security.User;
import com.it.savant.ukm.invent.service.domain.service.user.CurrentUserNameResolver;
import org.jlead.assets.security.v2.AuthenticationHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class InventoryBriefDetailedConfirmService {

    private final InventoryConfirmClient classifierConfirmClient;
    private final InventoryItemBriefQuantityUpdateClient cardUpdateClient;
    private final CurrentUserNameResolver userNameResolver;

    @Autowired
    public InventoryBriefDetailedConfirmService(final InventoryConfirmClient classifierConfirmClient,
                                                final InventoryItemBriefQuantityUpdateClient cardUpdateClient,
                                                final CurrentUserNameResolver userNameResolver) {
        this.classifierConfirmClient = classifierConfirmClient;
        this.cardUpdateClient = cardUpdateClient;
        this.userNameResolver = userNameResolver;
    }

    public void confirm(final InventoryBriefDetailed classifierDetailed) {
        classifierDetailed.getItems().forEach(
                smCard -> cardUpdateClient.update(classifierDetailed.getDocId(), smCard.getArticle(), smCard.getQuantity())
        );
        classifierConfirmClient.confirm(
                classifierDetailed.getDocId(),
                new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date()),
                userNameResolver.resolve()
        );
    }
}
