package com.it.savant.ukm.invent.service.domain.service.integration.exports;

import com.it.savant.ukm.invent.service.domain.model.Inventory;
import com.it.savant.ukm.invent.service.domain.service.integration.exports.target.IntegrationFileTarget;
import com.it.savant.ukm.invent.service.domain.service.integration.exports.target.IntegrationFileTargetBuilder;
import com.it.savant.ukm.invent.service.domain.service.integration.exports.target.IntegrationFileTargetBuilderResolver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class IntegrationInventoryExportService {

    private final IntegrationInventoryXmlBuilder xmlBuilder;
    private final IntegrationFileTargetBuilderResolver targetResolver;

    @Autowired
    public IntegrationInventoryExportService(
            final IntegrationInventoryXmlBuilder xmlBuilder,
            final IntegrationFileTargetBuilderResolver targetResolver
    ) {
        this.xmlBuilder = xmlBuilder;
        this.targetResolver = targetResolver;
    }

    public void doExport(final Inventory inventory) {
        try {
            log.info("Начат экспорт акта инвентаризации '{}'", inventory.getId());
            final IntegrationFileTargetBuilder fileTargetBuilder = targetResolver.resolve();
            final IntegrationFileTarget fileTarget = fileTargetBuilder.build();
            fileTarget.write(
                    inventory.getId(),
                    xmlBuilder.buildXml(inventory)
            );
            log.info("Экспорт акта инвентаризации '{}' успешно завершен.", inventory.getId());
        } catch (final Throwable throwable) {
            log.error("Во время экспорта акта инвентаризации с идентификатором [" + inventory.getId() +  "] произошла", throwable);
        }
    }

}
