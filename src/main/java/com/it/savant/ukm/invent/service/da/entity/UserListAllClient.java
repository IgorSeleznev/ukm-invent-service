package com.it.savant.ukm.invent.service.da.entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class UserListAllClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public UserListAllClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<UserEntity> listAll() {
        return jdbcTemplate.query(
                "SELECT id, name, password_hash, location, administrator_login, administrator \n" +
                        "FROM " + DATABASE_NAME + ".user \n" +
                        "WHERE deleted = 0 ",
                (resultSet, i) -> new UserEntity()
                        .setId(resultSet.getLong(1))
                        .setName(resultSet.getString(2))
                        .setPasswordHash(resultSet.getString(3))
                        .setLocation(resultSet.getString(4))
                        .setAdministratorLogin(resultSet.getString(5))
                        .setAdministrator((int) resultSet.getShort(6))
        );
    }
}
