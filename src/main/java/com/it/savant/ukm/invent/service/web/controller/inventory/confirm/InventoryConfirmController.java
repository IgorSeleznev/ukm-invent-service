package com.it.savant.ukm.invent.service.web.controller.inventory.confirm;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.InventoryConfirmRequest;
import com.it.savant.ukm.invent.service.domain.model.InventoryItemBrief;
import com.it.savant.ukm.invent.service.domain.service.inventory.InventoryConfirmManager;
import com.it.savant.ukm.invent.service.web.json.InventoryConfirmRequestJson;
import com.it.savant.ukm.invent.service.web.json.InventoryItemBriefJson;
import com.it.savant.ukm.invent.service.web.json.Response;
import com.it.savant.ukm.invent.service.web.security.Authorized;
import com.it.savant.ukm.invent.service.web.security.Licensed;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static com.it.savant.ukm.invent.service.web.constant.HttpHeaderConstant.HTTP_HEADER_CLIENT_UID;
import static com.it.savant.ukm.invent.service.web.json.Response.failed;
import static com.it.savant.ukm.invent.service.web.json.Response.success;
import static java.util.stream.Collectors.toList;

@Slf4j
@RestController
public class InventoryConfirmController {

    private final InventoryConfirmManager service;
    private final Converter<InventoryConfirmRequestJson, InventoryConfirmRequest> requestConverter;
    private final Converter<InventoryItemBrief, InventoryItemBriefJson> itemConverter;

    @Autowired
    public InventoryConfirmController(final InventoryConfirmManager service,
                                      final Converter<InventoryConfirmRequestJson, InventoryConfirmRequest> requestConverter,
                                      final Converter<InventoryItemBrief, InventoryItemBriefJson> itemConverter) {
        this.service = service;
        this.requestConverter = requestConverter;
        this.itemConverter = itemConverter;
    }

    @Licensed
    @Authorized
    @PostMapping("/inventory/confirm")
    public Response<List<InventoryItemBriefJson>> confirm(
            @RequestBody final InventoryConfirmRequestJson request,
            final HttpServletRequest httpRequest
    ) {
        log.info(
                "Принят запрос на подтверждение акта инвентаризации {} с признаком принудительного подтверждения {}",
                request.getDocId(),
                request.getForce()
        );
        final List<InventoryItemBrief> result = service.confirm(
                requestConverter.convert(request),
                httpRequest.getHeader(HTTP_HEADER_CLIENT_UID)
        );
        if (result.size() == 0) {
            return success(newArrayList());
        }
        log.info("Подтверждение акта инвентаризации {} отклонено, поскольку ассортимент был изменен", request.getDocId());
        return failed(
                "Ассортимент был изменен",
                result.stream()
                        .map(itemConverter::convert)
                        .collect(toList())
        );
    }
}