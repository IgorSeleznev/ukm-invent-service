package com.it.savant.ukm.invent.service.web.converter.inventory.brief;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.InventoryItemBrief;
import com.it.savant.ukm.invent.service.domain.model.InventoryBriefDetailed;
import com.it.savant.ukm.invent.service.web.json.InventoryItemBriefJson;
import com.it.savant.ukm.invent.service.web.json.InventoryBriefDetailedJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class InventoryBriefDetailedDomainConverter implements Converter<InventoryBriefDetailedJson, InventoryBriefDetailed> {

    private final Converter<InventoryItemBriefJson, InventoryItemBrief> converter;

    @Autowired
    public InventoryBriefDetailedDomainConverter(final Converter<InventoryItemBriefJson, InventoryItemBrief> converter) {
        this.converter = converter;
    }

    @Override
    public InventoryBriefDetailed convert(final InventoryBriefDetailedJson source) {
        return new InventoryBriefDetailed()
                .setDocId(source.getDocId())
                .setAssortmentName(source.getAssortmentName())
                .setItems(
                        source.getItems().stream()
                                .map(converter::convert)
                                .collect(Collectors.toList())
                );
    }
}
