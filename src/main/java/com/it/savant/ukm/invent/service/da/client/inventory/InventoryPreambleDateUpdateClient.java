package com.it.savant.ukm.invent.service.da.client.inventory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryPreambleDateUpdateClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryPreambleDateUpdateClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void update(final String docId, final String preambleDate) {
        jdbcTemplate.update(
                "UPDATE " + DATABASE_NAME + ".inventory \n" +
                        "SET preambleDate = ? \n" +
                        "WHERE id = ? and preambleDate IS NULL",
                preambleDate,
                docId
        );
    }
}