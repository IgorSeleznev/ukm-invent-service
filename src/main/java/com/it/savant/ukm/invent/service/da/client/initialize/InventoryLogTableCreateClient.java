package com.it.savant.ukm.invent.service.da.client.initialize;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryLogTableCreateClient implements CreateTableClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryLogTableCreateClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public String getTableName() {
        return "inventory_log";
    }

    public void create() {
        jdbcTemplate.update(
                "CREATE TABLE IF NOT EXISTS " + DATABASE_NAME + ".inventory_log (\n" +
                        "    record_id BIGINT AUTO_INCREMENT NOT NULL, \n" +
                        "    record_date TIMESTAMP DEFAULT now() NOT NULL, \n" +
                        "    id VARCHAR(50),\n" +
                        "    docType VARCHAR(2),\n" +
                        "    bornIn VARCHAR(255),\n" +
                        "    commentary VARCHAR(255),\n" +
                        "    createDat VARCHAR(40),\n" +
                        "    currencyMultOrder VARCHAR(6),\n" +
                        "    currencyRate VARCHAR(9),\n" +
                        "    currencyType VARCHAR(6),\n" +
                        "    docState VARCHAR(6),\n" +
                        "    isRoubles VARCHAR(1),\n" +
                        "    location VARCHAR(11),\n" +
                        "    opCode VARCHAR(6),\n" +
                        "    priceRoundMode VARCHAR(6),\n" +
                        "    totalSum VARCHAR(21),\n" +
                        "    totalSumCur VARCHAR(21),\n" +
                        "    fillSpecType VARCHAR(6),\n" +
                        "    finalDate VARCHAR(40),\n" +
                        "    isActiveOnly VARCHAR(1),\n" +
                        "    isFillComplete VARCHAR(1),\n" +
                        "    orderNo VARCHAR(50),\n" +
                        "    ourSelfClient VARCHAR(12),\n" +
                        "    preambleDate VARCHAR(40),\n" +
                        "    priceMode VARCHAR(6),\n" +
                        "    priceType VARCHAR(6),\n" +
                        "    storeLoc VARCHAR(12),\n" +
                        "    withDue VARCHAR(20),\n" +
                        "    confirmed SMALLINT DEFAULT 0 NOT NULL, \n" +
                        "    created_manually SMALLINT DEFAULT 0 NOT NULL, \n" +
                        "    PRIMARY KEY (record_id) \n" +
                        ") ENGINE = InnoDB DEFAULT CHARSET = UTF8"
        );
    }
}
