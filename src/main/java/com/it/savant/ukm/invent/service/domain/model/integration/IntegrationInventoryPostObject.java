package com.it.savant.ukm.invent.service.domain.model.integration;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;

@Data
@ToString
@Accessors(chain = true)
public class IntegrationInventoryPostObject {

    @JacksonXmlProperty(localName = "description", isAttribute = true)
    private String description;
    @JacksonXmlProperty(localName = "action", isAttribute = true)
    private String action;
    @JacksonXmlProperty(localName = "Id")
    private String id;
    @JacksonXmlProperty(localName = "IL")
    private List<Map<String, String>> il;
}
