package com.it.savant.ukm.invent.service.domain.service.inventory;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.client.inventory.InventoryFindByDocIdClient;
import com.it.savant.ukm.invent.service.da.client.inventory.item.InventoryItemListByDocIdClient;
import com.it.savant.ukm.invent.service.da.entity.InventoryEntity;
import com.it.savant.ukm.invent.service.domain.model.Inventory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.of;

@Service
public class InventoryFindByDocIdService {

    private final InventoryFindByDocIdClient inventoryClient;
    private final InventoryItemListByDocIdClient itemClient;
    private final Converter<InventoryEntity, Inventory> converter;

    @Autowired
    public InventoryFindByDocIdService(final InventoryFindByDocIdClient inventoryClient,
                                       final InventoryItemListByDocIdClient itemClient,
                                       final Converter<InventoryEntity, Inventory> converter) {
        this.inventoryClient = inventoryClient;
        this.itemClient = itemClient;
        this.converter = converter;
    }

    public Optional<Inventory> findById(final String docId) {
        final Optional<InventoryEntity> inventoryEntity = inventoryClient.findByDocId(docId);
        if (inventoryEntity.isEmpty()) {
            return empty();
        }
        return of(
                converter.convert(
                        inventoryEntity.get()
                                .setItems(
                                        itemClient.listByDocId(docId)
                                )
                )
        );
    }
}
