package com.it.savant.ukm.invent.service.domain.model.export;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class InventoryExportRecord {

    private String docId;
    private String assortmentName;
    private String confirmDate;
    private Boolean polled;
}
