package com.it.savant.ukm.invent.service.da.client.inventory.item;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryItemMaxRecordDateClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryItemMaxRecordDateClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Long max(final String docId) {
        final List<Long> results = jdbcTemplate.query(
                "SELECT unix_timestamp(max(mii.record_date)) as max_record_date \n" +
                        "FROM " + DATABASE_NAME + ".inventory_item mii \n" +
                        "WHERE mii.docId = ? \n" +
                        "  AND mii.actual = 1",
                (resultSet, i) -> resultSet.getLong(1),
                docId
        );
        if (results.get(0) == null) {
            return 0L;
        }
        return results.get(0);
    }

}
