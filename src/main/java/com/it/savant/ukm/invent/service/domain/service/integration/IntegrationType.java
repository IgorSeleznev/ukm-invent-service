package com.it.savant.ukm.invent.service.domain.service.integration;

public enum IntegrationType {

    LOCAL,
    FTP
}
