package com.it.savant.ukm.invent.service.domain.converter.fileimportlog;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.entity.FileImportLogEntity;
import com.it.savant.ukm.invent.service.domain.model.FileImportLog;
import org.springframework.stereotype.Component;

@Component
public class FileImportLogEntityConverter implements Converter<FileImportLog, FileImportLogEntity> {

    @Override
    public FileImportLogEntity convert(final FileImportLog source) {
        return new FileImportLogEntity()
                .setFilename(source.getFilename())
                .setSuccess(source.isSuccess())
                .setErrorMessage(source.getErrorMessage())
                .setContent(source.getContent());
    }
}
