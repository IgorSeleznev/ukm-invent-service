package com.it.savant.ukm.invent.service.domain.service.inventory;

import com.google.common.base.Strings;
import com.it.savant.ukm.invent.service.common.value.ValueHolder;
import com.it.savant.ukm.invent.service.domain.service.parameter.ParameterProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.common.value.ValueHolder.hold;
import static com.it.savant.ukm.invent.service.domain.constants.ParameterConstants.DOCID_PREFIX_PLACEHOLDER;
import static com.it.savant.ukm.invent.service.domain.constants.ParameterConstants.DOCID_SEQUENCE;

@Component
public class InventoryManuallyDocIdGenerator {

    private final ParameterProvider parameterProvider;

    @Autowired
    public InventoryManuallyDocIdGenerator(final ParameterProvider parameterProvider) {
        this.parameterProvider = parameterProvider;
    }

    public String generate(final String location) {
        final ValueHolder<String> docIdNumber = hold();

        synchronized (this) {
            final String value = parameterProvider.read(DOCID_SEQUENCE);
            docIdNumber.setValue(String.valueOf(Long.parseLong(value) + 1));
            parameterProvider.write(DOCID_SEQUENCE, docIdNumber.getValue());
        }

        final String prefix = String.format(parameterProvider.read(DOCID_PREFIX_PLACEHOLDER), location);

        return prefix +
                Strings.repeat("0", 10 - docIdNumber.getValue().length()) +
                docIdNumber.getValue();
    }
}
