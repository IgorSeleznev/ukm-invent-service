package com.it.savant.ukm.invent.service.common.exception;

public class MoreThanOneFoundException extends RuntimeException {

    public MoreThanOneFoundException(final String message) {
        super(message);
    }
}
