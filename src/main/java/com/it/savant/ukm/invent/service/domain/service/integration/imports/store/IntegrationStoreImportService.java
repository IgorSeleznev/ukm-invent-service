package com.it.savant.ukm.invent.service.domain.service.integration.imports.store;

import com.it.savant.ukm.invent.service.domain.service.integration.imports.ImportService;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.IntegrationDeserializeService;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.parser.XmlNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class IntegrationStoreImportService implements ImportService {

    private final IntegrationDeserializeService deserializeService;
    private final IntegrationStoreSaveService saveService;

    @Autowired
    public IntegrationStoreImportService(final IntegrationDeserializeService deserializeService,
                                         final IntegrationStoreSaveService saveService) {
        this.deserializeService = deserializeService;
        this.saveService = saveService;
    }

    @Override
    public boolean isValid(final XmlNode xml) {
        final List<XmlNode> nodes = xml.getNodes().get(0)
                .getNodes().get(0)
                .getNodes();
        for (final XmlNode node : nodes) {
            if ("SMSTORELOCATIONS".equals(node.getName())) {
                log.info("Импорт данных: обнаружен файл описания магазина");
                return true;
            }
        }

        return false;
    }

    @Override
    public void doImport(final XmlNode xml) {
        log.info("Импорт данных: импорт магазина");
        saveService.save(
                deserializeService.deserialize(xml, "STORE")
        );
    }
}
