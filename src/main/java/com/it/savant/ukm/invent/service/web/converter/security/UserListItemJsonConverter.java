package com.it.savant.ukm.invent.service.web.converter.security;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.security.UserListItem;
import com.it.savant.ukm.invent.service.web.json.security.UserListItemJson;
import org.springframework.stereotype.Component;

@Component
public class UserListItemJsonConverter implements Converter<UserListItem, UserListItemJson> {

    @Override
    public UserListItemJson convert(final UserListItem source) {
        return new UserListItemJson()
                .setId(source.getId())
                .setName(source.getName());
    }
}
