package com.it.savant.ukm.invent.service.da.client.parameter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class ParameterMaxIdClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ParameterMaxIdClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Long maxId() {
        final List<Long> results = jdbcTemplate.query(
                "SELECT ifnull(max(id), 0) as maxId \n" +
                        "FROM " + DATABASE_NAME + ".parameter",
                (resultSet, i) -> resultSet.getLong(1)
        );

        return results.get(0);
    }
}
