package com.it.savant.ukm.invent.service.web.converter.inventory.item.brief;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.InventoryItemBriefQuantityUpdateRequest;
import com.it.savant.ukm.invent.service.web.json.InventoryItemBriefQuantityUpdateRequestJson;
import org.springframework.stereotype.Component;

@Component
public class InventoryItemBriefQuantityUpdateRequestDomainConverter
        implements Converter<InventoryItemBriefQuantityUpdateRequestJson, InventoryItemBriefQuantityUpdateRequest> {

    @Override
    public InventoryItemBriefQuantityUpdateRequest convert(final InventoryItemBriefQuantityUpdateRequestJson source) {
        return new InventoryItemBriefQuantityUpdateRequest()
                .setDocId(source.getDocId())
                .setArticle(source.getArticle())
                .setQuantity(source.getQuantity());
    }
}
