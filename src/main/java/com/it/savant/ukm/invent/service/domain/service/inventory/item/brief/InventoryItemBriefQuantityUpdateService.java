package com.it.savant.ukm.invent.service.domain.service.inventory.item.brief;

import com.it.savant.ukm.invent.service.common.exception.UkmInventServiceBusinessException;
import com.it.savant.ukm.invent.service.da.client.inventory.InventoryCreateDatUpdateClient;
import com.it.savant.ukm.invent.service.da.client.inventory.InventoryExistsByDocIdByNotConfirmedClient;
import com.it.savant.ukm.invent.service.da.client.inventory.InventoryPreambleDateUpdateClient;
import com.it.savant.ukm.invent.service.da.client.inventory.item.brief.InventoryItemBriefQuantityUpdateClient;
import com.it.savant.ukm.invent.service.domain.model.InventoryItemBriefQuantityUpdateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class InventoryItemBriefQuantityUpdateService {

    private final InventoryExistsByDocIdByNotConfirmedClient existsClient;
    private final InventoryItemBriefQuantityUpdateClient updateClient;
    private final InventoryStartDatesUpdateService datesUpdateService;

    @Autowired
    public InventoryItemBriefQuantityUpdateService(
            final InventoryExistsByDocIdByNotConfirmedClient existsClient,
            final InventoryItemBriefQuantityUpdateClient updateClient,
            final InventoryStartDatesUpdateService datesUpdateService
    ) {
        this.existsClient = existsClient;
        this.updateClient = updateClient;
        this.datesUpdateService = datesUpdateService;
    }

    @Transactional
    public void updateQuantity(final InventoryItemBriefQuantityUpdateRequest request) {
        if (!existsClient.exists(request.getDocId())) {
            throw new UkmInventServiceBusinessException(
                    "Нельзя изменить количество товара для подтвержденного акта инвентаризации, либо акт не существует"
            );
        }
        updateClient.update(
                request.getDocId(),
                request.getArticle(),
                request.getQuantity()
        );
        datesUpdateService.update(request.getDocId(), new Date());
    }
}
