package com.it.savant.ukm.invent.service.domain.service.integration.imports.store;

import com.it.savant.ukm.invent.service.domain.model.Store;
import com.it.savant.ukm.invent.service.domain.service.store.StoreInsertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IntegrationStoreSaveService {

    private final StoreInsertService storeInsertService;

    @Autowired
    public IntegrationStoreSaveService(final StoreInsertService storeInsertService) {
        this.storeInsertService = storeInsertService;
    }

    public void save(final Store store) {
        storeInsertService.insert(store);
    }
}
