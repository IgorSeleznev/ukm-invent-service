package com.it.savant.ukm.invent.service.da.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class StartTransactionClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public StartTransactionClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void start() {
        jdbcTemplate.update("START TRANSACTION");
    }
}
