package com.it.savant.ukm.invent.service.domain.service.inventory;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.client.inventory.item.InventoryItemInsertClient;
import com.it.savant.ukm.invent.service.da.entity.InventoryItemEntity;
import com.it.savant.ukm.invent.service.domain.model.InventoryItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InventoryItemInsertService {

    private final InventoryItemInsertClient insertClient;
    private final Converter<InventoryItem, InventoryItemEntity> converter;

    @Autowired
    public InventoryItemInsertService(final InventoryItemInsertClient insertClient,
                                      final Converter<InventoryItem, InventoryItemEntity> converter) {
        this.insertClient = insertClient;
        this.converter = converter;
    }

    public void insert(final InventoryItem inventoryItem) {
        insertClient.insert(
                converter.convert(inventoryItem)
        );
    }
}
