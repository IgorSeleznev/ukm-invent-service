package com.it.savant.ukm.invent.service.web.converter.export;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.export.InventoryExportRecord;
import com.it.savant.ukm.invent.service.web.json.InventoryExportRecordJson;
import org.springframework.stereotype.Component;

@Component
public class InventoryExportRecordJsonConverter implements Converter<InventoryExportRecord, InventoryExportRecordJson> {

    @Override
    public InventoryExportRecordJson convert(final InventoryExportRecord source) {
        return new InventoryExportRecordJson()
                .setAssortmentName(source.getAssortmentName())
                .setConfirmDate(source.getConfirmDate())
                .setDocId(source.getDocId())
                .setId(source.getDocId())
                .setPolled(source.getPolled());
    }
}
