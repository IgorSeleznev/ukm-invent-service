package com.it.savant.ukm.invent.service.domain.service.inventory;

import com.it.savant.ukm.invent.service.da.client.inventory.InventoryLastDocIdByOrdernoClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class InventoryManuallyCloneService {

    private final InventoryLastDocIdByOrdernoClient lastDocIdClient;
    private final InventoryManuallyDocIdGenerator docIdGenerator;
    private final InventoryManuallyCloneByDocIdService insertService;

    @Autowired
    public InventoryManuallyCloneService(final InventoryLastDocIdByOrdernoClient lastDocIdClient,
                                         final InventoryManuallyDocIdGenerator docIdGenerator,
                                         final InventoryManuallyCloneByDocIdService insertService) {
        this.lastDocIdClient = lastDocIdClient;
        this.docIdGenerator = docIdGenerator;
        this.insertService = insertService;
    }

    public String clone(final String assortmentName, final String location) {
        final String docId = docIdGenerator.generate(location);
        insertService.clone(
                lastDocIdClient.lastDocId(assortmentName, location),
                docId
        );

        log.info(
                "Для ассортимента '{}' в магазин с идентификатором {} на основании ассортимента добавлен вручную новый акт инвентаризации '{}'",
                assortmentName,
                location,
                docId
        );

        return docId;
    }
}
