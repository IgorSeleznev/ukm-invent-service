package com.it.savant.ukm.invent.service.domain.service.integration.exports.target.local;

import com.it.savant.ukm.invent.service.domain.service.integration.exports.target.IntegrationFileTarget;
import lombok.extern.slf4j.Slf4j;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@Slf4j
public class IntegrationLocalFileTarget implements IntegrationFileTarget {

    private final String localPath;
    private final String temporaryPath;

    public IntegrationLocalFileTarget(final String localPath, final String temporaryPath) {
        this.localPath = localPath;
        this.temporaryPath = temporaryPath;
    }

    @Override
    public void write(final String docId, String content) {
        final String temporaryFilename = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-").format(new Date()) +
                Thread.currentThread().getId() +
                UUID.randomUUID().toString() + ".tmp";
        final String targetFilename = "inventory-" +
                new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-").format(new Date()) +
                docId + ".xml";

        try {
            Files.writeString(Paths.get(temporaryPath, temporaryFilename), content);
            Files.copy(
                    Paths.get(temporaryPath, temporaryFilename),
                    Paths.get(localPath, targetFilename)
            );
            log.info("Данные экспортируемого акта инвентаризации были записаны в файл {}", targetFilename);
        } catch (final Throwable throwable) {
            log.error(
                    "Error was occurred when try to write or copy file for export with temporary path '" +
                            temporaryFilename + "'" +
                            " with target path '" + localPath + "' with target filename '" + targetFilename + "'" +
                            " with temporary filename '" + temporaryFilename + "'",
                    throwable
            );
        } finally {
            try {
                Files.deleteIfExists(Paths.get(temporaryPath, temporaryFilename));
            } catch (final Throwable deleteThrowable) {
                log.error(
                        "Attention! Error has occurred when try to delete temporary file that was created for export with in path '" + temporaryPath + "' with filename '" + temporaryFilename + "'",
                        deleteThrowable
                );
            }
        }
    }
}
