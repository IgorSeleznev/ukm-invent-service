package com.it.savant.ukm.invent.service.domain.service.license;

import com.it.savant.ukm.invent.service.da.client.license.LicenseExistsClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static org.apache.commons.codec.digest.DigestUtils.sha1Hex;

@Service
public class LicenseExistsService {

    private final LicenseExistsClient client;

    @Autowired
    public LicenseExistsService(final LicenseExistsClient client) {
        this.client = client;
    }

    public boolean exists(final String license) {
        return client.exists(sha1Hex(license));
    }
}
