package com.it.savant.ukm.invent.service.domain.service.integration.imports.source.ftp;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.apache.commons.net.ftp.FTPClient;

@Data
@ToString
@Builder
@Accessors(chain = true)
public class IntegrationFTPFileSourceConfiguration {

    private String remotePath;
    private String remoteFilename;
    private String localTemporaryPath;
    private FTPClient client;
}
