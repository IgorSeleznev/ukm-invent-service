package com.it.savant.ukm.invent.service.da.client.measure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;
import static java.util.Optional.empty;
import static java.util.Optional.of;

@Component
public class MeasureIncreaseStepByIdClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public MeasureIncreaseStepByIdClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Optional<String> increaseStepFindById(final String id) {
        final List<String> found = jdbcTemplate.query(
                "SELECT increase_step \n" +
                        "FROM " + DATABASE_NAME + ".measure \n" +
                        "WHERE id = ?",
                (resultSet, i) -> resultSet.getString(1),
                id
        );
        if (found.size() == 0) {
            return empty();
        }
        return of(found.get(0));
    }
}
