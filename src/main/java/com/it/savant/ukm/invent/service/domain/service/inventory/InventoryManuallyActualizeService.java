package com.it.savant.ukm.invent.service.domain.service.inventory;

import com.it.savant.ukm.invent.service.da.client.inventory.InventoryLastDocIdByOrdernoClient;
import com.it.savant.ukm.invent.service.domain.model.Inventory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InventoryManuallyActualizeService {

    private final InventoryLastDocIdByOrdernoClient lastDocIdClient;
    private final InventoryManuallyActualizeFlowService flowService;

    @Autowired
    public InventoryManuallyActualizeService(
            final InventoryLastDocIdByOrdernoClient lastDocIdClient,
            final InventoryManuallyActualizeFlowService flowService
    ) {
        this.lastDocIdClient = lastDocIdClient;
        this.flowService = flowService;
    }

    public void actualize(final Inventory importedInventory) {
        flowService.actualizationFlow(
                importedInventory.getId(),
                lastDocIdClient.lastDocId(
                        importedInventory.getOrderNo(),
                        importedInventory.getLocation()
                )
        );
    }
}
