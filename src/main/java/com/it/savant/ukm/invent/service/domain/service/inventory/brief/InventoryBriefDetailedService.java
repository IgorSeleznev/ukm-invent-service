package com.it.savant.ukm.invent.service.domain.service.inventory.brief;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.common.exception.UkmInventServiceBusinessException;
import com.it.savant.ukm.invent.service.da.client.inventory.InventoryExistsByDocIdByNotConfirmedClient;
import com.it.savant.ukm.invent.service.da.client.inventory.brief.InventoryBriefDetailedEntityComposeService;
import com.it.savant.ukm.invent.service.da.entity.InventoryBriefDetailedEntity;
import com.it.savant.ukm.invent.service.domain.model.InventoryBriefDetailed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InventoryBriefDetailedService {

    private final InventoryExistsByDocIdByNotConfirmedClient existsClient;
    private final InventoryBriefDetailedEntityComposeService composeService;
    private final Converter<InventoryBriefDetailedEntity, InventoryBriefDetailed> converter;

    @Autowired
    public InventoryBriefDetailedService(
            final InventoryExistsByDocIdByNotConfirmedClient existsClient,
            final InventoryBriefDetailedEntityComposeService composeService,
            final Converter<InventoryBriefDetailedEntity, InventoryBriefDetailed> converter
    ) {
        this.existsClient = existsClient;
        this.composeService = composeService;
        this.converter = converter;
    }

    public InventoryBriefDetailed inventoryBriefDetailed(final String docId) {
        if (existsClient.exists(docId)) {
            return converter.convert(
                    composeService.findCompose(docId)
            );
        }
        throw new UkmInventServiceBusinessException("Акт инвентаризации не существует или уже подтвержден");
    }
}
