package com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory;

import com.it.savant.ukm.invent.service.domain.model.Inventory;
import com.it.savant.ukm.invent.service.domain.service.inventory.InventoryInsertService;
import com.it.savant.ukm.invent.service.domain.service.inventory.InventoryItemInsertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IntegrationInventorySaveService {

    private final InventoryInsertService inventoryInsertService;
    private final InventoryItemInsertService itemInsertService;

    @Autowired
    public IntegrationInventorySaveService(final InventoryInsertService inventoryInsertService,
                                           final InventoryItemInsertService itemInsertService) {
        this.inventoryInsertService = inventoryInsertService;
        this.itemInsertService = itemInsertService;
    }

    public void save(final Inventory inventory) {
        inventoryInsertService.insert(
                inventory.setCreatedManually(false)
        );
        inventory.getItems().forEach(itemInsertService::insert);
    }
}
