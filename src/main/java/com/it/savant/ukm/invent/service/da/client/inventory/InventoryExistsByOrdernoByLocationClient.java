package com.it.savant.ukm.invent.service.da.client.inventory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

public class InventoryExistsByOrdernoByLocationClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryExistsByOrdernoByLocationClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public boolean count(final String orderno, final String location) {
        final List<Long> result = jdbcTemplate.query(
                "SELECT count(*) \n" +
                        "FROM " + DATABASE_NAME + ".inventory \n" +
                        "WHERE orderno = ? AND created_manually = 0 AND location = ?",
                (resultSet, i) -> resultSet.getLong(1),
                orderno,
                location
        );
        return result.get(0) > 0;
    }
}
