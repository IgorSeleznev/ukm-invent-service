package com.it.savant.ukm.invent.service.domain.service.database;

import com.it.savant.ukm.invent.service.da.client.database.DatabaseExistsClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Service
public class DatabaseExistsService {

    private final DatabaseExistsClient client;

    @Autowired
    public DatabaseExistsService(final DatabaseExistsClient client) {
        this.client = client;
    }

    public boolean exists() {
        return client.exists(DATABASE_NAME);
    }
}