package com.it.savant.ukm.invent.service.web.converter.parameter;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.parameter.Parameter;
import com.it.savant.ukm.invent.service.web.json.ParameterJson;
import org.springframework.stereotype.Component;

@Component
public class ParameterJsonConverter implements Converter<Parameter, ParameterJson> {

    @Override
    public ParameterJson convert(final Parameter source) {
        return new ParameterJson()
                .setId(source.getId())
                .setName(source.getName())
                .setValue(source.getValue())
                .setType(source.getType())
                .setChooses(source.getChooses())
                .setTitle(source.getTitle())
                .setEditable(source.getEditable());
    }
}
