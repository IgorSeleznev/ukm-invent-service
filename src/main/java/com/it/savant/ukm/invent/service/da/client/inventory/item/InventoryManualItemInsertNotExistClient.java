package com.it.savant.ukm.invent.service.da.client.inventory.item;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryManualItemInsertNotExistClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryManualItemInsertNotExistClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void insertNotExist(final String docId) {
        jdbcTemplate.update(
                "INSERT INTO " + DATABASE_NAME + ".inventory_item ( \n" +
                        "    docId, \n" +
                        "    docType, \n" +
                        "    specItem, \n" +
                        "    article, \n" +
                        "    displayItem, \n" +
                        " \n" +
                        "    itemPrice, \n" +
                        "    itemPriceCur, \n" +
                        "    quantity, \n" +
                        "    totalPrice, \n" +
                        "    totalPriceCur, \n" +
                        " \n" +
                        "    actualQuantity, \n" +
                        "    awaitQuantity, \n" +
                        "    history, \n" +
                        "    actual, \n" +
                        "    source_docId \n" +
                        ") \n" +
                        "SELECT \n" +
                        "    mi.id, \n" +
                        "    aii.docType, \n" +
                        "    aii.specItem, \n" +
                        "    aii.article, \n" +
                        "    aii.displayItem, \n" +
                        " \n" +
                        "    aii.itemPrice, \n" +
                        "    aii.itemPriceCur, \n" +
                        "    0 as quantity, \n" +
                        "    aii.totalPrice, \n" +
                        "    aii.totalPriceCur, \n" +
                        " \n" +
                        "    0 as actualQuantity, \n" +
                        "    0 as awaitQuantity, \n" +
                        "    aii.history, \n" +
                        "    1 as actual, \n" +
                        "    ai.id \n" +
                        "FROM " + DATABASE_NAME + ".inventory mi \n" +
                        "  INNER JOIN " + DATABASE_NAME + ".inventory ai ON mi.orderno = ai.orderno AND mi.location = ai.location \n" +
                        "  INNER JOIN " + DATABASE_NAME + ".inventory_item aii ON ai.id = aii.docid \n" +
                        "  LEFT JOIN " + DATABASE_NAME + ".inventory_item mii ON aii.article = mii.article AND mii.docid = mi.id \n" +
                        "WHERE mii.article IS NULL \n" +
                        "  AND mi.created_manually = 1 \n" +
                        "  AND ai.id = ?",
                docId
        );
    }
}
