package com.it.savant.ukm.invent.service.domain.converter.user;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.entity.UserEntity;
import com.it.savant.ukm.invent.service.domain.model.security.User;
import org.springframework.stereotype.Component;

import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;

@Component
public class UserConverter implements Converter<UserEntity, User> {

    @Override
    public User convert(final UserEntity source) {
        return new User()
                .setId(source.getId())
                .setName(source.getName())
                .setLocation(source.getLocation())
                .setPasswordHash(source.getPasswordHash())
                .setAdministratorLogin(source.getAdministratorLogin())
                .setAdministrator(defaultIfNull(source.getAdministrator(), 0) == 1);
    }
}
