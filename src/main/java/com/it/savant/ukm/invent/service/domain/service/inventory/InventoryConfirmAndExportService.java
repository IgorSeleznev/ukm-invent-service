package com.it.savant.ukm.invent.service.domain.service.inventory;

import com.it.savant.ukm.invent.service.da.client.inventory.InventoryConfirmClient;
import com.it.savant.ukm.invent.service.domain.service.integration.exports.InventoryExportEventAsyncDocIdQueue;
import com.it.savant.ukm.invent.service.domain.service.user.CurrentUserNameResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class InventoryConfirmAndExportService {

    private final InventoryConfirmClient confirmClient;
    private final InventoryExportEventAsyncDocIdQueue queue;
    private final CurrentUserNameResolver userNameResolver;

    @Autowired
    public InventoryConfirmAndExportService(final InventoryConfirmClient confirmClient,
                                            final InventoryExportEventAsyncDocIdQueue queue,
                                            final CurrentUserNameResolver userNameResolver) {
        this.confirmClient = confirmClient;
        this.queue = queue;
        this.userNameResolver = userNameResolver;
    }

    public void confirmAndExport(final String docId) {
        confirmClient.confirm(
                docId,
                new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date()),
                userNameResolver.resolve()
        );
        queue.pushAndPersist(docId);
    }
}
