package com.it.savant.ukm.invent.service.web.json.security;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class AuthenticateRequestJson {

    private Long id;
    private String password;
}
