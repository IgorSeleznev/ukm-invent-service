package com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory;

import com.it.savant.ukm.invent.service.domain.service.integration.imports.assemble.IntegrationAssembleService;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.parser.XmlNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IntegrationDeserializeService {

    private final Map<String, IntegrationAssembleService<?>> assembleServices = new HashMap<>();

    @Autowired
    public IntegrationDeserializeService(final List<IntegrationAssembleService<?>> assembleServices) {
        assembleServices.forEach(
                service -> this.assembleServices.put(service.getAssembleName(), service)
        );
    }

    public <T> T deserialize(final XmlNode xml, final String modelName) {
        final IntegrationAssembleService<T> assembleService = (IntegrationAssembleService<T>) assembleServices.get(modelName);
        return assembleService.assemble(xml);
    }
}
