package com.it.savant.ukm.invent.service.domain.service.security;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.TreeMap;

@Component
public class MobileClientSessionTokenHolder {

    private final Map<String, String> clientTokenMap = new TreeMap<>();
    private final Map<String, String> tokenClientMap = new TreeMap<>();

    public void put(final String clientId, final String token) {
        if (containsToken(token)) {
            final String actualClient = tokenClientMap.get(token);
            clientTokenMap.remove(actualClient);
        }
        clientTokenMap.put(clientId, token);
        tokenClientMap.put(token, clientId);
    }

    public boolean containsToken(final String token) {
        return tokenClientMap.containsKey(token);
    }

    public boolean containsClientId(final String clientId) {
        return clientTokenMap.containsKey(clientId);
    }

    public String getToken(final String clientId) {
        return clientTokenMap.get(clientId);
    }

    public String getClientId(final String token) {
        return tokenClientMap.get(token);
    }

    public String removeByClientId(final String clientId) {
        final String tokenRemoved = clientTokenMap.remove(clientId);
        if (tokenRemoved != null) {
            tokenClientMap.remove(tokenRemoved);
        }
        return tokenRemoved;
    }

    public String removeByToken(final String token) {
        final String clientIdRemoved = tokenClientMap.remove(token);
        if (clientIdRemoved != null) {
            clientTokenMap.remove(clientIdRemoved);
        }
        return clientIdRemoved;
    }
}
