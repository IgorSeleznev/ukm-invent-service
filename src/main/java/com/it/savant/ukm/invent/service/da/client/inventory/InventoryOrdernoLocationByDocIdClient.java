package com.it.savant.ukm.invent.service.da.client.inventory;

import com.it.savant.ukm.invent.service.da.entity.InventoryOrdernoLocationEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryOrdernoLocationByDocIdClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryOrdernoLocationByDocIdClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public InventoryOrdernoLocationEntity ordernoLocationByDocId(final String docId) {
        return jdbcTemplate.queryForObject(
                "SELECT orderno, location \n" +
                        "FROM " + DATABASE_NAME + ".inventory \n" +
                        "WHERE id = ?",
                (resultSet, rowNum) -> new InventoryOrdernoLocationEntity()
                        .setOrderno(resultSet.getString(1))
                        .setLocation(resultSet.getString(2)),
                docId
        );
    }
}
