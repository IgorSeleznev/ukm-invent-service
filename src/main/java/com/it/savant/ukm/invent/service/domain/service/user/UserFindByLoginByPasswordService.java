package com.it.savant.ukm.invent.service.domain.service.user;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.common.exception.MoreThanOneFoundException;
import com.it.savant.ukm.invent.service.da.client.security.UserFindByLoginByPasswordClient;
import com.it.savant.ukm.invent.service.da.entity.UserEntity;
import com.it.savant.ukm.invent.service.domain.model.security.User;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.of;

@Service
public class UserFindByLoginByPasswordService {

    private final UserFindByLoginByPasswordClient client;
    private final Converter<UserEntity, User> converter;

    @Autowired
    public UserFindByLoginByPasswordService(
            final UserFindByLoginByPasswordClient client,
            final Converter<UserEntity, User> converter
    ) {
        this.client = client;
        this.converter = converter;
    }

    public Optional<User> findByIdByPassword(final String administratorLogin, final String password) {
        final List<UserEntity> found = client.findByLoginByPassword(
                administratorLogin,
                DigestUtils.sha1Hex(password)
        );
        if (found.size() > 1) {
            throw new MoreThanOneFoundException("There is more than one users with specified login [" + administratorLogin + "] and password [" + password + "]");
        }

        if (found.size() == 0) {
            return empty();
        }
        return of(
                converter.convert(found.get(0))
        );
    }
}
