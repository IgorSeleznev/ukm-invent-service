package com.it.savant.ukm.invent.service.domain.service.integration.imports.assemble;

import com.it.savant.ukm.invent.service.domain.model.measure.Measure;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.parser.XmlNode;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Service
public class IntegrationMeasureAssembleService implements IntegrationAssembleService<List<Measure>> {

    @Override
    public String getAssembleName() {
        return "MEASURE";
    }

    @Override
    public List<Measure> assemble(final XmlNode xmlNode) {
        final List<Measure> measures = newArrayList();

        xmlNode.getNodes().get(0)
                .getNodes().get(0)
                .getNodes()
                .forEach(
                        node -> {
                            if ("SAMEASUREMENT".equals(node.getName())) {
                                measures.add(
                                        new Measure()
                                                .setId(node.getValues().get("ID"))
                                                .setAbbrev(node.getValues().get("ABBREV"))
                                                .setBaseType(node.getValues().get("BASETYPE"))
                                                .setCode(node.getValues().get("CODE"))
                                                .setCodeIso(node.getValues().get("CODEISO"))
                                                .setMesType(node.getValues().get("MESTYPE"))
                                                .setName(node.getValues().get("NAME"))
                                                .setPrec(node.getValues().get("PREC"))
                                                .setUnitName(node.getValues().get("UNITNAME"))
                                );
                            }
                        }
                );

        return measures;
    }
}