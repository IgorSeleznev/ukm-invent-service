package com.it.savant.ukm.invent.service.domain.service.integration.imports.assemble;

import com.it.savant.ukm.invent.service.domain.model.Inventory;
import com.it.savant.ukm.invent.service.domain.model.InventoryItem;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.parser.XmlNode;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class IntegrationInventoryAssembleService implements IntegrationAssembleService<Inventory> {

    @Override
    public String getAssembleName() {
        return "INVENTORY";
    }

    @Override
    public Inventory assemble(final XmlNode xmlNode) {
        final Inventory inventory = new Inventory()
                .setConfirmed(false)
                .setCreatedManually(false);

        final Map<String, InventoryItem> items = new HashMap<>();

        xmlNode.getNodes().get(0)
                .getNodes().get(0)
                .getNodes()
                .forEach(
                        node -> {
                            if ("SMSPEC".equals(node.getName()) || "SMSPECIL".equals(node.getName())) {
                                if (!items.containsKey(node.getValues().get("SPECITEM"))) {
                                    items.put(node.getValues().get("SPECITEM"), new InventoryItem());
                                }
                            }
                            if ("SMSPEC".equals(node.getName())) {
                                final InventoryItem item = items.get(node.getValues().get("SPECITEM"));
                                final Map<String, String> map = node.getValues();
                                item
                                        .setDocId(map.get("DOCID"))
                                        .setDocType(map.get("DOCTYPE"))
                                        .setSpecItem(map.get("SPECITEM"))
                                        .setArticle(map.get("ARTICLE"))
                                        .setDisplayItem(map.get("DISPLAYITEM"))
                                        .setItemPrice(map.get("ITEMPRICE"))
                                        .setItemPriceCur(map.get("ITEMPRICECUR"))
                                        .setQuantity(map.get("QUANTITY"))
                                        .setTotalPrice(map.get("TOTALPRICE"))
                                        .setTotalPriceCur(map.get("TOTALPRICECUR"));
                            }
                            if ("SMSPECIL".equals(node.getName())) {
                                final InventoryItem item = items.get(node.getValues().get("SPECITEM"));
                                final Map<String, String> map = node.getValues();
                                item
                                        .setActualQuantity(map.get("ACTUALQUANTITY"))
                                        .setAwaitQuantity(map.get("AWAITQUANTITY"))
                                        .setHistory(map.get("HISTORY"));
                            }
                            if ("SMDOCUMENTS".equals(node.getName())) {
                                inventory
                                        .setId(node.getValues().get("ID"))
                                        .setDocType(node.getValues().get("DOCTYPE"))
                                        .setBornIn(node.getValues().get("BORNIN"))
                                        .setCommentary(node.getValues().get("COMMENTARY"))
                                        .setCreateDat(node.getValues().get("CREATEDAT"))
                                        .setCurrencyMultOrder(node.getValues().get("CURRENCYMULTORDER"))
                                        .setCurrencyRate(node.getValues().get("CURRENCYRATE"))
                                        .setCurrencyType(node.getValues().get("CURRENCYTYPE"))
                                        .setDocState(node.getValues().get("DOCSTATE"))
                                        .setIsRoubles(node.getValues().get("ISROUBLES"))
                                        .setLocation(node.getValues().get("LOCATION"))
                                        .setOpCode(node.getValues().get("OPCODE"))
                                        .setPriceRoundMode(node.getValues().get("PRICEROUNDMODE"))
                                        .setTotalSum(node.getValues().get("TOTALSUM"))
                                        .setTotalSumCur(node.getValues().get("TOTALSUMCUR"));
                            }
                            if ("SMINVLIST".equals(node.getName())) {
                                inventory
                                        .setFillSpecType(node.getValues().get("FILLSPECTYPE"))
                                        .setFinalDate(node.getValues().get("FINALDATE"))
                                        .setIsActiveOnly(node.getValues().get("ISACTIVEONLY"))
                                        .setIsFillComplete(node.getValues().get("ISFILLCOMPLETE"))
                                        .setOrderNo(node.getValues().get("ORDERNO"))
                                        .setOurSelfClient(node.getValues().get("OURSELFCLIENT"))
                                        .setPreambleDate(node.getValues().get("PREAMBLEDATE"))
                                        .setPriceMode(node.getValues().get("PRICEMODE"))
                                        .setPriceType(node.getValues().get("PRICETYPE"))
                                        .setStoreLoc(node.getValues().get("STORELOC"))
                                        .setWithDue(node.getValues().get("WITHDUE"));
                            }
                        }
                );

        inventory.getItems().addAll(
                items.values()
        );
        return inventory;
    }
}
