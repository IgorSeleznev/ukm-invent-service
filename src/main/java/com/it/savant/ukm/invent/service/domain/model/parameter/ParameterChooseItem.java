package com.it.savant.ukm.invent.service.domain.model.parameter;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ParameterChooseItem {

    private String title;
    private String value;
}
