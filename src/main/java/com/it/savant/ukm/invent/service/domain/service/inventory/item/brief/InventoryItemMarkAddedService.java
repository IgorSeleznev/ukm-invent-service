package com.it.savant.ukm.invent.service.domain.service.inventory.item.brief;

import com.it.savant.ukm.invent.service.da.client.inventory.InventoryItemUpdateSourceDocIdClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class InventoryItemMarkAddedService {

    private final InventoryItemUpdateSourceDocIdClient client;

    @Autowired
    public InventoryItemMarkAddedService(final InventoryItemUpdateSourceDocIdClient client) {
        this.client = client;
    }

    public void mark(final String docId, final String article) {
        client.updateSourceDocId(docId, UUID.randomUUID().toString(), article);
    }
}
