package com.it.savant.ukm.invent.service.domain.service.user;

import com.it.savant.ukm.invent.service.domain.model.PageableSortableRequest;
import com.it.savant.ukm.invent.service.domain.model.ResultPage;
import com.it.savant.ukm.invent.service.domain.model.security.User;
import com.it.savant.ukm.invent.service.domain.service.PaginateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserListAllPageableSortableService {

    private final UserListAllService listAllService;
    private final UserListSorter sorter;
    private final PaginateService paginateService;

    @Autowired
    public UserListAllPageableSortableService(final UserListAllService listAllService,
                                              final UserListSorter sorter,
                                              final PaginateService paginateService) {
        this.listAllService = listAllService;
        this.sorter = sorter;
        this.paginateService = paginateService;
    }

    public ResultPage<User> findAll(final PageableSortableRequest request) {
        return paginateService.paginate(
                sorter.sort(
                        listAllService.listAll(),
                        request
                ),
                request
        );
    }
}