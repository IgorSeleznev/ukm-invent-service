package com.it.savant.ukm.invent.service.domain.service.integration.imports.source.ftp;

import com.it.savant.ukm.invent.service.common.exception.UkmInventServiceBusinessException;
import com.it.savant.ukm.invent.service.domain.service.integration.configuration.IntegrationFTPConfiguration;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.source.IntegrationDirectorySource;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.source.IntegrationFileSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import java.io.IOException;

@Slf4j
public class IntegrationFTPDirectorySource implements IntegrationDirectorySource {

    private final FTPClient client;
    private final IntegrationFTPConfiguration configuration;

    private FTPFile[] files;
    private int currentFileIndex = 0;

    public IntegrationFTPDirectorySource(final FTPClient client,
                                         final IntegrationFTPConfiguration configuration) {
        this.client = client;
        this.configuration = configuration;
        try {
            client.connect(configuration.getHost());
            client.login(configuration.getUsername(), configuration.getPassword());
            client.changeWorkingDirectory(configuration.getPath());
            files = client.listFiles();
            currentFileIndex = 0;
        } catch (final Exception exception) {
            if (client.isConnected()) {
                try {
                    client.disconnect();
                } catch (final Exception ioException) {
                    log.error("Has got error on disconnect from FTP server", ioException);
                }
            }
            log.error("Ошибка подключения к серверу FTP и получения списка файлов из каталога " + configuration.getPath(), exception);
            throw new UkmInventServiceBusinessException(exception);
        }
    }

    @Override
    public boolean hasFiles() {
        return client.isConnected() && files != null && currentFileIndex < files.length;
    }

    @Override
    public IntegrationFileSource nextFileSource() {
        try {
            if (files == null) {
                return null;
            }
            if (!client.isConnected()) {
                client.connect(configuration.getHost());
                client.login(configuration.getUsername(), configuration.getPassword());
                client.changeWorkingDirectory(configuration.getPath());
                files = client.listFiles();
                currentFileIndex = 0;
            }
            if (currentFileIndex >= files.length) {
                client.disconnect();
                return null;
            }
            final IntegrationFTPFileSource fileSource = new IntegrationFTPFileSource(
                    IntegrationFTPFileSourceConfiguration.builder()
                            .remotePath(configuration.getPath())
                            .client(client)
                            .remoteFilename(files[currentFileIndex].getName())
                            .localTemporaryPath(configuration.getLocalTemporaryPath())
                            .build()
            );
            currentFileIndex++;
            return fileSource;

        } catch (final IOException exception) {
            if (client.isConnected()) {
                try {
                    client.disconnect();
                } catch (final IOException ioException) {
                    log.error("Has got error on disconnect from FTP server", ioException);
                }
            }
            log.error("Ошибка получения информации о файле с FTP-сервера из каталога " + configuration.getPath(), exception);
            throw new UkmInventServiceBusinessException(exception);
        }
    }
}
