package com.it.savant.ukm.invent.service.da.client.security;

import com.it.savant.ukm.invent.service.da.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class UserUpdateClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public UserUpdateClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void update(final UserEntity userEntity) {
        jdbcTemplate.update(
                "UPDATE " + DATABASE_NAME + ".user \n" +
                        "SET name = ?, password_hash = ?, location = ?, administrator = ?, administrator_login = ? \n" +
                        "WHERE id = ? and deleted = 0",
                userEntity.getName(),
                userEntity.getPasswordHash(),
                userEntity.getLocation(),
                Short.parseShort(userEntity.getAdministrator().toString()),
                userEntity.getAdministratorLogin(),
                userEntity.getId()
        );
    }
}
