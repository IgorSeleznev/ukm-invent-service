package com.it.savant.ukm.invent.service.domain.service.integration.imports.storelocations;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.client.store.StoreReplaceClient;
import com.it.savant.ukm.invent.service.da.entity.StoreEntity;
import com.it.savant.ukm.invent.service.domain.model.Store;
import com.it.savant.ukm.invent.service.domain.model.smstorelocations.SmStoreLocations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IntegrationSmStoreLocationsSaveService {

    private final StoreReplaceClient storeReplaceClient;
    private final Converter<Store, StoreEntity> converter;

    @Autowired
    public IntegrationSmStoreLocationsSaveService(
            final StoreReplaceClient storeReplaceClient,
            final Converter<Store, StoreEntity> converter
    ) {
        this.storeReplaceClient = storeReplaceClient;
        this.converter = converter;
    }

    public void save(final List<SmStoreLocations> smStoreLocationsList) {
        smStoreLocationsList.forEach(
                smStoreLocations ->
                        storeReplaceClient.replace(
                                converter.convert(
                                        new Store()
                                                .setId(smStoreLocations.getId())
                                                .setName(smStoreLocations.getName())
                                )
                        ));
    }
}
