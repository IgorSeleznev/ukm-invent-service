package com.it.savant.ukm.invent.service.domain.model;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class InventoryItemBrief {

    private String article;
    private String name;
    private String quantity;
    private String measure;
    private String increaseStep;
}
