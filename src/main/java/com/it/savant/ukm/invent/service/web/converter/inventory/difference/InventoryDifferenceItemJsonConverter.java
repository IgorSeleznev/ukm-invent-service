package com.it.savant.ukm.invent.service.web.converter.inventory.difference;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.InventoryDifferenceItem;
import com.it.savant.ukm.invent.service.web.json.InventoryDifferenceItemJson;
import org.springframework.stereotype.Component;

@Component
public class InventoryDifferenceItemJsonConverter implements Converter<InventoryDifferenceItem, InventoryDifferenceItemJson> {

    @Override
    public InventoryDifferenceItemJson convert(final InventoryDifferenceItem source) {
        return new InventoryDifferenceItemJson()
                .setArticle(source.getArticle())
                .setName(source.getName());
    }
}
