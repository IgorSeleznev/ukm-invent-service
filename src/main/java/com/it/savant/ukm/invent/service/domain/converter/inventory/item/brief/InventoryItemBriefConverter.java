package com.it.savant.ukm.invent.service.domain.converter.inventory.item.brief;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.entity.InventoryItemBriefEntity;
import com.it.savant.ukm.invent.service.domain.model.InventoryItemBrief;
import org.springframework.stereotype.Component;

@Component
public class InventoryItemBriefConverter implements Converter<InventoryItemBriefEntity, InventoryItemBrief> {

    @Override
    public InventoryItemBrief convert(final InventoryItemBriefEntity source) {
        return new InventoryItemBrief()
                .setArticle(source.getArticle())
                .setName(source.getName())
                .setQuantity(source.getQuantity())
                .setMeasure(source.getMeasure())
                .setIncreaseStep(source.getIncreaseStep());
    }
}
