package com.it.savant.ukm.invent.service.domain.service.user;

import com.it.savant.ukm.invent.service.domain.model.security.User;
import org.jlead.assets.security.v2.AuthenticationHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserSignOutService {

    private final UserFindByIdService findByIdService;
    private final AuthenticationHolder<User, Boolean> authenticationHolder;

    @Autowired
    public UserSignOutService(final UserFindByIdService findByIdService,
                              final AuthenticationHolder<User, Boolean> authenticationHolder) {
        this.findByIdService = findByIdService;
        this.authenticationHolder = authenticationHolder;
    }

    public void singOut(final Long userId) {
        authenticationHolder.signOut(
                findByIdService.findById(userId)
        );
    }
}
