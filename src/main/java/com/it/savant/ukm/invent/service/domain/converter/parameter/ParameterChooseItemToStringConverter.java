package com.it.savant.ukm.invent.service.domain.converter.parameter;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.domain.model.parameter.ParameterChooseItem;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ParameterChooseItemToStringConverter implements Converter<List<ParameterChooseItem>, String> {

    @Override
    public String convert(final List<ParameterChooseItem> source) {
        if (source == null || source.size() == 0) {
            return null;
        }
        return source.stream()
                .map(item -> item.getValue() + ":" + item.getTitle())
                .collect(Collectors.joining(";"));
    }
}
