package com.it.savant.ukm.invent.service.da.client.parameter;

import com.it.savant.ukm.invent.service.da.entity.ParameterEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class ParameterListAllClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ParameterListAllClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<ParameterEntity> listAll() {
        return jdbcTemplate.query(
                "SELECT id, name, value, type, title, `values`, editable \n" +
                        "FROM " + DATABASE_NAME + ".parameter",
                (resultSet, i) -> new ParameterEntity()
                        .setId(resultSet.getLong(1))
                        .setName(resultSet.getString(2))
                        .setValue(resultSet.getString(3))
                        .setType(resultSet.getString(4))
                        .setTitle(resultSet.getString(5))
                        .setChooses(resultSet.getString(6))
                        .setEditable(resultSet.getShort(7))
        );
    }
}
