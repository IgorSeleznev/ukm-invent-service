package com.it.savant.ukm.invent.service.domain.service.integration.exports.target.ftp;

import com.it.savant.ukm.invent.service.domain.service.integration.configuration.IntegrationFTPConfiguration;
import com.it.savant.ukm.invent.service.domain.service.integration.exports.target.IntegrationFileTarget;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.ftp.FTPClient;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@Slf4j
public class IntegrationFTPFileTarget implements IntegrationFileTarget {

    private final FTPClient ftpClient;
    private final IntegrationFTPConfiguration configuration;

    public IntegrationFTPFileTarget(final FTPClient ftpClient, final IntegrationFTPConfiguration configuration) {
        this.ftpClient = ftpClient;
        this.configuration = configuration;
    }

    @Override
    public void write(final String docId, String content) {
        final String temporaryFilename = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-").format(new Date()) +
                Thread.currentThread().getId() +
                UUID.randomUUID().toString() + ".tmp";
        final String targetFilename = "inventory-" +
                new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-").format(new Date()) +
                docId + ".xml";

        try {
            Files.writeString(Paths.get(configuration.getLocalTemporaryPath(), temporaryFilename), content);

            final File uploadFile = Paths.get(configuration.getLocalTemporaryPath(), temporaryFilename).toFile();

            ftpClient.connect(configuration.getHost());
            //ftpClient.setDataTimeout(30000);
            //ftpClient.setConnectTimeout(60000);
            //ftpClient.setSoTimeout(60000);
            //ftpClient.setControlKeepAliveTimeout(60000);
            ftpClient.login(configuration.getUsername(), configuration.getPassword());
            ftpClient.changeWorkingDirectory(configuration.getPath());
            ftpClient.storeFile(targetFilename, new BufferedInputStream(new FileInputStream(uploadFile)));
            ftpClient.disconnect();

            log.info("Данные экспортируемого акта инвентаризации были записаны в файл {}", targetFilename);
        } catch (final Throwable throwable) {
            log.error(
                    "Error was occurred when try to write or copy file for export with temporary path '" +
                            temporaryFilename + "'" +
                            " with target path '" + configuration.getPath() + "' with target filename '" + targetFilename + "'" +
                            " with temporary filename '" + temporaryFilename + "'",
                    throwable
            );
        } finally {
            try {
                Files.deleteIfExists(Paths.get(configuration.getLocalTemporaryPath(), temporaryFilename));
            } catch (final Throwable deleteThrowable) {
                log.error(
                        "Attention! Error has occurred when try to delete temporary file that was created for export with in path '" + configuration.getLocalTemporaryPath() + "' with filename '" + temporaryFilename + "'",
                        deleteThrowable
                );
            }
        }
    }
}
