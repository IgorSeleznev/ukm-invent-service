package com.it.savant.ukm.invent.service.domain.service.license;

import org.springframework.stereotype.Service;

import static com.it.savant.ukm.invent.service.common.converter.WordToIntConverter.convert;

@Service
public class AntalLicenserKvarService {

    private static final int SJÖTÍU_FIMM = convert("sjötíu fimm");

    public int siffra() {
        return SJÖTÍU_FIMM;
    }
}
