package com.it.savant.ukm.invent.service.domain.service.database;

import com.it.savant.ukm.invent.service.domain.service.parameter.ParameterTableInitializeService;
import com.it.savant.ukm.invent.service.domain.service.user.UserTableInitializeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataInitializeService {

    private final ParameterTableInitializeService parameterInitializeService;
    private final UserTableInitializeService userInitializeService;

    @Autowired
    public DataInitializeService(
            final ParameterTableInitializeService parameterInitializeService,
            final UserTableInitializeService userInitializeService
    ) {
        this.parameterInitializeService = parameterInitializeService;
        this.userInitializeService = userInitializeService;
    }

    public void initialize() {
        parameterInitializeService.initialize();
        userInitializeService.initialize();
    }
}
