package com.it.savant.ukm.invent.service.domain.service.user;

import com.it.savant.ukm.invent.service.da.client.initialize.TableIsEmptyClient;
import com.it.savant.ukm.invent.service.domain.model.security.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserTableInitializeService {

    private final TableIsEmptyClient emptyClient;
    private final UserInsertService insertService;

    @Autowired
    public UserTableInitializeService(final TableIsEmptyClient emptyClient,
                                      final UserInsertService insertService) {
        this.emptyClient = emptyClient;
        this.insertService = insertService;
    }

    public void initialize() {
        if (!emptyClient.isEmpty("user")) {
            return;
        }
        insertService.insert(
                new User()
                        .setName("Гость")
                        .setLocation("15")
                        .setPasswordHash("1234")
                        .setAdministrator(false)
        );
        insertService.insert(
                new User()
                        .setName("Администратор по-умолчанию")
                        .setLocation("15")
                        .setPasswordHash("admin")
                        .setAdministrator(true)
                        .setAdministratorLogin("admin")
        );
    }
}
