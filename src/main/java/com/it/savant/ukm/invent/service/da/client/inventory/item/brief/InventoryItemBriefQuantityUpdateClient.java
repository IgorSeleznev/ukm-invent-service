package com.it.savant.ukm.invent.service.da.client.inventory.item.brief;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryItemBriefQuantityUpdateClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryItemBriefQuantityUpdateClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void update(final String docId, final String article, final String quantity) {
        jdbcTemplate.update(
                "UPDATE " + DATABASE_NAME + ".inventory_item \n" +
                        "SET quantity = ?, source_docId = ? \n" +
                        "WHERE article = ? and docid = ?",
                quantity,
                docId,
                article,
                docId
        );
    }
}