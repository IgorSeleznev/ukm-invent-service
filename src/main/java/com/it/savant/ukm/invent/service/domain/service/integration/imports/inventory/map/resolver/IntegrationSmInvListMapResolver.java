package com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.map.resolver;

import com.it.savant.ukm.invent.service.common.exception.UkmInventServiceBusinessException;
import com.it.savant.ukm.invent.service.domain.model.integration.IntegrationInventoryPackage;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.inventory.map.validator.IntegrationSmInvListMapValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class IntegrationSmInvListMapResolver {

    private final IntegrationSmInvListMapValidator validator;

    @Autowired
    public IntegrationSmInvListMapResolver(final IntegrationSmInvListMapValidator validator) {
        this.validator = validator;
    }

    public Map<String, String> resolve(final IntegrationInventoryPackage source) {
        for (final Map<String, String> map : source.getPostObject().getIl()) {
            if (validator.isValid(map)) {
                return map;
            }
        }
        throw new UkmInventServiceBusinessException("Element SMINVLIST not found");
    }
}
