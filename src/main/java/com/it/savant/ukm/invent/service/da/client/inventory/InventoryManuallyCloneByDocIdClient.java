package com.it.savant.ukm.invent.service.da.client.inventory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryManuallyCloneByDocIdClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryManuallyCloneByDocIdClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void clone(final String assortmentDocId, final String createDate, final String manuallyDocId) {
        jdbcTemplate.update(
                "INSERT INTO " + DATABASE_NAME + ".inventory ( \n" +
                        "    id, \n" +
                        "    docType, \n" +
                        "    bornIn, \n" +
                        "    commentary, \n" +
                        "    createDat, \n" +

                        "    currencyMultOrder, \n" +
                        "    currencyRate, \n" +
                        "    currencyType, \n" +
                        "    docState, \n" +
                        "    isRoubles, \n" +

                        "    location, \n" +
                        "    opCode, \n" +
                        "    priceRoundMode, \n" +
                        "    totalSum, \n" +
                        "    totalSumCur, \n" +

                        "    fillSpecType, \n" +
                        "    finalDate, \n" +
                        "    isActiveOnly, \n" +
                        "    isFillComplete, \n" +
                        "    orderNo, \n" +

                        "    ourSelfClient, \n" +
                        "    preambleDate, \n" +
                        "    priceMode, \n" +
                        "    priceType, \n" +
                        "    storeLoc, \n" +

                        "    withDue, \n" +
                        "    confirmed, \n" +
                        "    created_manually \n" +
                        ") \n" +
                        "SELECT \n" +
                        "    ?, \n" +
                        "    docType, \n" +
                        "    bornIn, \n" +
                        "    commentary, \n" +
                        "    ? createDat, \n" +

                        "    currencyMultOrder, \n" +
                        "    currencyRate, \n" +
                        "    currencyType, \n" +
                        "    docState, \n" +
                        "    isRoubles, \n" +

                        "    location, \n" +
                        "    opCode, \n" +
                        "    priceRoundMode, \n" +
                        "    0 as totalSum, \n" +
                        "    0 as totalSumCur, \n" +

                        "    fillSpecType, \n" +
                        "    null as finalDate, \n" +
                        "    isActiveOnly, \n" +
                        "    isFillComplete, \n" +
                        "    orderNo, \n" +

                        "    ourSelfClient, \n" +
                        "    null as preambleDate, \n" +
                        "    priceMode, \n" +
                        "    priceType, \n" +
                        "    storeLoc, \n" +

                        "    withDue, \n" +
                        "    0 as confirmed, \n" +
                        "    1 as created_manually \n" +
                        "FROM " + DATABASE_NAME + ".inventory \n" +
                        "WHERE id = ?",
                manuallyDocId,
                createDate,
                assortmentDocId
        );
    }

}
