package com.it.savant.ukm.invent.service.da.client.initialize;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class ParameterTableCreateClient implements CreateTableClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ParameterTableCreateClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public String getTableName() {
        return "parameter";
    }

    public void create() {
        jdbcTemplate.update(
                "CREATE TABLE IF NOT EXISTS " + DATABASE_NAME + ".parameter ( \n" +
                        "  id BIGINT NOT NULL, \n" +
                        "  name varchar(255) NOT NULL, \n" +
                        "  value varchar(4192), \n" +
                        "  type varchar(20) DEFAULT 'TEXT' NOT NULL, \n" +
                        "  title varchar(255), \n" +
                        "  `values` varchar(4192), \n" +
                        "  editable smallint DEFAULT 1 NOT NULL, \n" +
                        "  PRIMARY KEY (name) \n" +
                        ") ENGINE = InnoDB DEFAULT CHARSET = UTF8"
        );
    }
}
