package com.it.savant.ukm.invent.service.da.client.parameter;

import com.it.savant.ukm.invent.service.da.entity.ParameterEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class ParameterInsertClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ParameterInsertClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void insert(final ParameterEntity parameterEntity) {
        jdbcTemplate.update(
                "INSERT INTO " + DATABASE_NAME + ".parameter ( \n" +
                        "    id, name, value, title, type, `values`, editable \n" +
                        ") \n" +
                        "VALUES (?, ?, ?, ?, ?, ?, ?)",
                parameterEntity.getId(),
                parameterEntity.getName(),
                parameterEntity.getValue(),
                parameterEntity.getTitle(),
                parameterEntity.getType(),
                parameterEntity.getChooses(),
                parameterEntity.getEditable()
        );
    }
}
