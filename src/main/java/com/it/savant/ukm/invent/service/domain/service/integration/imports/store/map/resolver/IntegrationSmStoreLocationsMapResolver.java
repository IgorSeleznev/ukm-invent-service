package com.it.savant.ukm.invent.service.domain.service.integration.imports.store.map.resolver;

import com.it.savant.ukm.invent.service.common.exception.UkmInventServiceBusinessException;
import com.it.savant.ukm.invent.service.domain.model.integration.IntegrationStorePackage;
import com.it.savant.ukm.invent.service.domain.service.integration.imports.store.map.validator.IntegrationSmStoreLocationsMapValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class IntegrationSmStoreLocationsMapResolver {

    private final IntegrationSmStoreLocationsMapValidator validator;

    @Autowired
    public IntegrationSmStoreLocationsMapResolver(final IntegrationSmStoreLocationsMapValidator validator) {
        this.validator = validator;
    }

    public Map<String, String> resolve(final IntegrationStorePackage source) {
        for (final Map<String, String> map : source.getPostObject().getSh()) {
            if (validator.isValid(map)) {
                return map;
            }
        }
        throw new UkmInventServiceBusinessException("Element SMSTORELOCATIONS not found");
    }
}
