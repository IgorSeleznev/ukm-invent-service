package com.it.savant.ukm.invent.service.domain.service.inventory.brief;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.client.inventory.brief.InventoryBriefListByStoreIdClient;
import com.it.savant.ukm.invent.service.da.client.store.StoreIdFindByClientIdClient;
import com.it.savant.ukm.invent.service.da.entity.InventoryBriefEntity;
import com.it.savant.ukm.invent.service.domain.model.InventoryBrief;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static org.apache.commons.codec.digest.DigestUtils.sha1Hex;

@Slf4j
@Service
public class InventoryBriefListService {

    private final StoreIdFindByClientIdClient storeIdClient;
    private final InventoryBriefListByStoreIdClient inventoryListClient;
    private final Converter<InventoryBriefEntity, InventoryBrief> converter;

    @Autowired
    public InventoryBriefListService(final StoreIdFindByClientIdClient storeIdClient,
                                     final InventoryBriefListByStoreIdClient inventoryListClient,
                                     final Converter<InventoryBriefEntity, InventoryBrief> converter) {
        this.storeIdClient = storeIdClient;
        this.inventoryListClient = inventoryListClient;
        this.converter = converter;
    }

    public List<InventoryBrief> list(final String clientId) {
        final List<InventoryBrief> result = inventoryListClient.listByStoreId(
                storeIdClient.findByClientId(sha1Hex(clientId))
        ).stream()
                .map(converter::convert)
                .collect(Collectors.toList());
        log.warn("We have got domain result for assortment list: {}", result);

        return result;
    }
}
