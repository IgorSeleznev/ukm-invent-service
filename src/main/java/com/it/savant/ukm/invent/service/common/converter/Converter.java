package com.it.savant.ukm.invent.service.common.converter;

public interface Converter<TSource, TTarget> {

    TTarget convert(final TSource source);
}
