package com.it.savant.ukm.invent.service.web.security;

import com.it.savant.ukm.invent.service.domain.model.security.User;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import org.jlead.assets.security.v2.Authentication;
import org.jlead.assets.security.v2.AuthenticationMember;

import java.util.Date;

@Data
@Accessors(chain = true)
@Builder
public class Authenticated implements Authentication<User, Boolean> {

    private String token;
    private AuthenticationMember<User, Boolean> member;
    private Date lastActivityTime;
    private String clientUid;


    @Override
    public String getToken() {
        return token;
    }

    @Override
    public Authentication<User, Boolean> setToken(final String token) {
        this.token = token;
        return this;
    }

    @Override
    public AuthenticationMember<User, Boolean> getMember() {
        return member;
    }

    @Override
    public Authentication<User, Boolean> setMember(final AuthenticationMember<User, Boolean> member) {
        this.member = member;
        return this;
    }

    @Override
    public Date getLastActivityTime() {
        return lastActivityTime;
    }

    @Override
    public Authentication<User, Boolean> setLastActivityTime(final Date lastActivityTime) {
        this.lastActivityTime = lastActivityTime;
        return this;
    }
}