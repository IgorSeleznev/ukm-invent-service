package com.it.savant.ukm.invent.service.da.client.export.record;

import com.it.savant.ukm.invent.service.common.exception.UkmInventServiceSystemException;
import org.springframework.stereotype.Component;

@Component
public class InventoryExportRecordListSqlOrderExpressionBuilder {

    public String build(final String orderName, final String direction) {
        if ("assortmentName".equals(orderName)) {
            return "orderno " + direction + ", finalDate DESC";
        }
        if ("id".equals(orderName) || "docId".equals(orderName)) {
            return "docId " + direction + ", finalDate DESC";
        }
        if ("polled".equals(orderName) || "inQueue".equals(orderName)) {
            return "polled " + ("asc".equals(direction.toLowerCase()) ? "desc" : "asc") + ", finalDate DESC";
        }
        if ("confirmDate".equals(orderName)) {
            return "finalDate " + direction + ", orderno DESC";
        }
        throw new UkmInventServiceSystemException("Было передано неизвестное поле для сортировки списка");
    }
}
