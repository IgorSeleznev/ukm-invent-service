package com.it.savant.ukm.invent.service.da.client.export.queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class ExportQueueMarkExportedClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ExportQueueMarkExportedClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void markPolled(final String docId) {
        jdbcTemplate.update(
                "UPDATE " + DATABASE_NAME + ".export_queue \n" +
                        "SET polled = 1, poll_date = now() \n" +
                        "WHERE docId = ? ",
                docId
        );
    }
}
