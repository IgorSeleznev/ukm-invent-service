package com.it.savant.ukm.invent.service.domain.service.integration.imports.source.local;

import com.it.savant.ukm.invent.service.domain.service.integration.imports.source.IntegrationFileSourceDeleteService;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Slf4j
public class IntegrationLocalFileSourceDeleteService implements IntegrationFileSourceDeleteService {

    private final String path;
    private final String filename;

    public IntegrationLocalFileSourceDeleteService(final String path, final String filename) {
        this.path = path;
        this.filename = filename;
    }

    @Override
    public void delete() {
        try {
            Files.deleteIfExists(Paths.get(path, filename));
        } catch (final IOException exception) {
            log.error("Could not delete temporary file at path [" + path + "] with name [" + filename + "]", exception);
        }
    }
}
