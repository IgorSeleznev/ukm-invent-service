package com.it.savant.ukm.invent.service.domain.converter.user;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.entity.UserListItemEntity;
import com.it.savant.ukm.invent.service.domain.model.security.UserListItem;
import org.springframework.stereotype.Component;

@Component
public class UserListItemConverter implements Converter<UserListItemEntity, UserListItem> {

    @Override
    public UserListItem convert(final UserListItemEntity source) {
        return new UserListItem()
                .setId(source.getId())
                .setName(source.getName());
    }
}
