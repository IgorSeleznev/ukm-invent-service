package com.it.savant.ukm.invent.service.domain.converter.measure;

import com.it.savant.ukm.invent.service.common.converter.Converter;
import com.it.savant.ukm.invent.service.da.entity.MeasureEntity;
import com.it.savant.ukm.invent.service.domain.model.measure.Measure;
import org.springframework.stereotype.Component;

@Component
public class MeasureConverter implements Converter<MeasureEntity, Measure> {

    @Override
    public Measure convert(final MeasureEntity source) {
        return new Measure()
                .setId(source.getId())
                .setAbbrev(source.getAbbrev())
                .setBaseType(source.getBaseType())
                .setCode(source.getCode())
                .setCodeIso(source.getCodeIso())
                .setMesType(source.getMesType())
                .setName(source.getName())
                .setPrec(source.getPrec())
                .setUnitName(source.getUnitName())
                .setIncreaseStep(source.getIncreaseStep());
    }
}
