package com.it.savant.ukm.invent.service.web.json;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class PageableSortableRequestJson {

    private Integer pageNumber;
    private Integer pageSize;
    private String sortField;
    private String sortDirection;
}
