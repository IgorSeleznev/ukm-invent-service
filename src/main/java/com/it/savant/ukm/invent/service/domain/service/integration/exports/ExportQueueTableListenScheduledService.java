package com.it.savant.ukm.invent.service.domain.service.integration.exports;

import com.it.savant.ukm.invent.service.domain.service.scheduled.ScheduledService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TriggerContext;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ExportQueueTableListenScheduledService implements ScheduledService {

    private final ExportQueueTableListenNextExecutionTimeService nextExecutionTimeService;
    private final ExportQueueTableListenService listenService;

    @Autowired
    public ExportQueueTableListenScheduledService(
            final ExportQueueTableListenNextExecutionTimeService nextExecutionTimeService,
            final ExportQueueTableListenService listenService
    ) {
        this.nextExecutionTimeService = nextExecutionTimeService;
        this.listenService = listenService;
    }

    @Override
    public void execute() {
        listenService.listen();
    }

    @Override
    public Date nextExecutionTime(final TriggerContext triggerContext) {
        return nextExecutionTimeService.nextExecutionTime(triggerContext);
    }
}
