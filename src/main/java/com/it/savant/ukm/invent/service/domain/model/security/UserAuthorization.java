package com.it.savant.ukm.invent.service.domain.model.security;

import lombok.Builder;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.jlead.assets.security.SecurityAuthorization;
import org.jlead.assets.security.SecurityUser;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@ToString
@Builder
@Accessors(chain = true)
public class UserAuthorization implements SecurityAuthorization {

    private String token;
    private AuthorizedUser user;
    private Date activityTime;
    private Map<String, Object> attributes = new HashMap<>();
    private String clientUID;

    @Override
    public String getToken() {
        return token;
    }

    public SecurityAuthorization setToken(final String token) {
        this.token = token;
        return this;
    }

    public SecurityUser getUser() {
        return user;
    }

    public SecurityAuthorization setUser(final AuthorizedUser securityUser) {
        this.user = securityUser;
        return this;
    }

    @Override
    public Date getActivityTime() {
        return activityTime;
    }

    @Override
    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public SecurityAuthorization setAttributes(final Map<String, Object> attributes) {
        this.attributes = attributes;
        return this;
    }

    @Override
    public SecurityAuthorization setActivityTime(final Date date) {
        this.activityTime = date;
        return this;
    }

    public String getClientUID() {
        return clientUID;
    }

    public SecurityAuthorization setClientUID(final String clientUID) {
        this.clientUID = clientUID;
        return this;
    }
}
