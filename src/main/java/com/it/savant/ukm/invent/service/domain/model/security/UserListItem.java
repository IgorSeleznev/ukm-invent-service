package com.it.savant.ukm.invent.service.domain.model.security;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class UserListItem {

    private Long id;
    private String name;
}
