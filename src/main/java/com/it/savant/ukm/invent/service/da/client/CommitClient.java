package com.it.savant.ukm.invent.service.da.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class CommitClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public CommitClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void commit() {
        jdbcTemplate.update("COMMIT");
    }
}
