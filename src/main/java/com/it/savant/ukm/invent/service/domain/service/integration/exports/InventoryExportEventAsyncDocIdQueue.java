package com.it.savant.ukm.invent.service.domain.service.integration.exports;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.LinkedList;
import java.util.Optional;
import java.util.Queue;

import static java.util.Optional.empty;
import static java.util.Optional.of;

public class InventoryExportEventAsyncDocIdQueue {

    private final InventoryExportQueuePersistManager persistManager;

    private final Queue<String> queue;

    @Autowired
    public InventoryExportEventAsyncDocIdQueue(final InventoryExportQueuePersistManager persistManager,
                                               final LinkedList<String> queue) {
        this.persistManager = persistManager;
        this.queue = queue;
    }

    public void pushAndPersist(final String docId) {
        synchronized (this) {
            persistManager.replace(docId);
            queue.offer(docId);
        }
    }

    public void push(final String docId) {
        synchronized (this) {
            queue.offer(docId);
        }
    }

    public Optional<String> poll() {
        synchronized (this) {
            final String docId = queue.poll();
            if (docId == null) {
                return empty();
            }
            persistManager.markPolled(docId);
            return of(docId);
        }
    }
}
