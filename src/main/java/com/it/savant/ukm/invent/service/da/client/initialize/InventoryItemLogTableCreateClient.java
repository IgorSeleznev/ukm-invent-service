package com.it.savant.ukm.invent.service.da.client.initialize;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import static com.it.savant.ukm.invent.service.da.constants.DatabaseConstants.DATABASE_NAME;

@Component
public class InventoryItemLogTableCreateClient implements CreateTableClient {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public InventoryItemLogTableCreateClient(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public String getTableName() {
        return "inventory_item_log";
    }

    public void create() {
        jdbcTemplate.update(
                "CREATE TABLE IF NOT EXISTS " + DATABASE_NAME + ".inventory_item_log (\n" +
                        "    record_id BIGINT AUTO_INCREMENT NOT NULL, \n" +
                        "    record_date TIMESTAMP DEFAULT now() NOT NULL, \n" +
                        "    docId VARCHAR(50),\n" +
                        "    docType VARCHAR(2),\n" +
                        "    specItem VARCHAR(12),\n" +
                        "    article VARCHAR(50),\n" +
                        "    displayItem VARCHAR(12),\n" +
                        "    itemPrice VARCHAR(21),\n" +
                        "    itemPriceCur VARCHAR(21),\n" +
                        "    quantity VARCHAR(16),\n" +
                        "    totalPrice VARCHAR(21),\n" +
                        "    totalPriceCur VARCHAR(21),\n" +
                        "    actualQuantity VARCHAR(20),\n" +
                        "    awaitQuantity VARCHAR(20),\n" +
                        "    history VARCHAR(4000)," +
                        "    actual SMALLINT DEFAULT 1 NOT NULL \n," +
                        "    PRIMARY KEY (record_id) \n" +
                        ") ENGINE = InnoDB DEFAULT CHARSET = UTF8"
        );
    }
}
