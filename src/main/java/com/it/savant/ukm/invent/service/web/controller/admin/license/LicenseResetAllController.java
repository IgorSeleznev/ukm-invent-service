package com.it.savant.ukm.invent.service.web.controller.admin.license;

import com.it.savant.ukm.invent.service.domain.service.license.LicenseResetAllService;
import com.it.savant.ukm.invent.service.web.json.ResetResultJson;
import com.it.savant.ukm.invent.service.web.security.Authorized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.it.savant.ukm.invent.service.web.json.ResetResultJson.failed;
import static com.it.savant.ukm.invent.service.web.json.ResetResultJson.success;

@RestController
public class LicenseResetAllController {

    private final LicenseResetAllService service;

    @Autowired
    public LicenseResetAllController(final LicenseResetAllService service) {
        this.service = service;
    }

    @Authorized(administratorOnly = true)
    @PostMapping("/admin/license/reset-all")
    public ResetResultJson resetAll() {
        try {
            service.resetAll();
            return success();
        } catch (final Throwable throwable) {
            return failed(throwable);
        }
    }
}
