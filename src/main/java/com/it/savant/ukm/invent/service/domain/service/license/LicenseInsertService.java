package com.it.savant.ukm.invent.service.domain.service.license;

import com.it.savant.ukm.invent.service.da.client.license.LicenseInsertClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static org.apache.commons.codec.digest.DigestUtils.sha1Hex;

@Service
public class LicenseInsertService {

    private final LicenseInsertClient insertClient;

    @Autowired
    public LicenseInsertService(final LicenseInsertClient insertClient) {
        this.insertClient = insertClient;
    }

    public void insert(final String license) {
        insertClient.insert(sha1Hex(license));
    }
}
